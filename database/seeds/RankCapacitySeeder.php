<?php

use Illuminate\Database\Seeder;

class RankCapacitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rank_capacity')->insert([
            'rank' => 'gold',
            'capacity' => '1'
        ]);

        DB::table('rank_capacity')->insert([
            'rank' => 'silver',
            'capacity' => '3'
        ]);

        DB::table('rank_capacity')->insert([
            'rank' => 'bronze',
            'capacity' => '5'
        ]);
    }
}
