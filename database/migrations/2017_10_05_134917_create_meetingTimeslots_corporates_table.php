<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingTimeslotsCorporatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetingtimeslots_corporates', function (Blueprint $table) {

            $table->integer('meeting_timeslot_id')->unsigned()->index();
            $table->foreign('meeting_timeslot_id')->references('id')->on('meeting_timeslots');

            $table->integer('corporate_id')->unsigned()->index();
            $table->foreign('corporate_id')->references('id')->on('meeting_corporates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meetingtimeslots_corporates', function(Blueprint $table) {
            $table->dropForeign(['meeting_timeslot_id']);
            $table->dropForeign(['corporate_id']);
        });

        Schema::dropIfExists('meetingtimeslots_corporates');
    }
}
