<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinkLabelToTabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_tabs', function (Blueprint $table) {
            $table->string('quick_link_label')->nullable();
            $table->boolean('quick_link_enabled')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_tabs', function (Blueprint $table) {
            $table->dropColumn('quick_link_label');
            $table->dropColumn('quick_link_enabled');
        });
    }
}
