<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusFieldInMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matrix_prime', function (Blueprint $table) {
            $table->integer('conferences_id')->unsigned()->index();
            $table->foreign('conferences_id')->references('id')->on('conferences');
            $table->string('status')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matrix_prime', function (Blueprint $table) {
            $table->dropForeign(['conferences_id']);
            $table->dropColumn('conferences_id');
            $table->boolean('status')->change();
        });
    }
}
