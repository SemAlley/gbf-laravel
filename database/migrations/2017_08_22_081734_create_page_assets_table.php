<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_assets', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('conference_id');
	        $table->string('page');
	        $table->string('type');
	        $table->string('description')->nullable();
	        $table->integer('asset')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_assets');
    }
}
