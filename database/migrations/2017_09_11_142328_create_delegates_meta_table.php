<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelegatesMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delegates_meta', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedInteger('delegate_id');
	        $table->string('meta_key');
	        $table->string('meta_value');

	        $table->foreign('delegate_id')->references('id')->on('delegates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delegates_meta');
    }
}
