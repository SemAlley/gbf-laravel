<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks_data', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('block_id')->unsigned();
	        $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');

	        $table->string('name');
	        $table->string('content');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks_data');
    }
}
