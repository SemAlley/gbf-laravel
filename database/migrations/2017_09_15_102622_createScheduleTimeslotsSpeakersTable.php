<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTimeslotsSpeakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduletimeslots_speakers', function (Blueprint $table) {

            $table->integer('schedule_timeslot_id')->unsigned()->index();
            $table->foreign('schedule_timeslot_id')->references('id')
                ->on('schedule_timeslots');

            $table->integer('speaker_id')->unsigned()->index();
            $table->foreign('speaker_id')->references('id')
                ->on('speakers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduletimeslots_speakers', function(Blueprint $table) {
            $table->dropForeign(['schedule_timeslot_id']);
            $table->dropForeign(['speaker_id']);
        });

        Schema::dropIfExists('scheduletimeslots_speakers');
    }
}
