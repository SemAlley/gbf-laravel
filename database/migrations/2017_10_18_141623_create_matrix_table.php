<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrix_prime', function (Blueprint $table) {

            $table->integer('timeslot_id')->unsigned()->index();
            $table->foreign('timeslot_id')->references('id')->on('meeting_timeslots');

            $table->integer('corporate_id')->unsigned()->index();
            $table->foreign('corporate_id')->references('id')->on('meeting_corporates');

            $table->integer('delegate_id')->unsigned()->index();
            $table->foreign('delegate_id')->references('id')->on('delegates');

            $table->integer('day_id')->unsigned()->index();
            $table->foreign('day_id')->references('id')->on('days');

            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meetingtimeslots_corporates', function(Blueprint $table) {
            $table->dropForeign(['timeslot_id']);
            $table->dropForeign(['corporate_id']);
            $table->dropForeign(['delegate_id']);
            $table->dropForeign(['day_id']);
        });

        Schema::dropIfExists('matrix_prime');
    }
}
