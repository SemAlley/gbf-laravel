<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationGalleryVsAssests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries_conference_assets', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('gallery_id')->unsigned()->nullable();
	        $table->foreign('gallery_id')->references('id')
	              ->on('galleries');

	        $table->integer('conference_asset_id')->unsigned()->nullable();
	        $table->foreign('conference_asset_id')->references('id')
	              ->on('conference_assets');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries_conference_assets');
    }
}
