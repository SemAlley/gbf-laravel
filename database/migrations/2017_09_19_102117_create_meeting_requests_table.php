<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_requests', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('delegate_id')->unsigned()->nullable();
	        $table->foreign('delegate_id')->references('id')->on('delegates');

	        $table->integer('target_id')->unsigned()->nullable();
	        $table->foreign('target_id')->references('id')->on('delegates');

	        $table->integer('meeting_timeslot_id')->unsigned()->nullable();
	        $table->foreign('meeting_timeslot_id')->references('id')->on('meeting_timeslots');

	        $table->enum('status', ['confirmed', 'declined', 'awaiting'])->default('awaiting');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_requests');
    }
}
