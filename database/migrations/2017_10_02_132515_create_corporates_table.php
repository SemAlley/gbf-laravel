<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorporatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_corporates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('day_id')->unsigned()->nullable();
            $table->foreign('day_id')->references('id')->on('days');

            $table->integer('delegate_id')->unsigned()->nullable();
            $table->foreign('delegate_id')->references('id')->on('delegates');

            $table->string('country')->nullable();
            $table->string('company_name');
            $table->string('industry')->nullable();
            $table->string('bio')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporates');
    }
}
