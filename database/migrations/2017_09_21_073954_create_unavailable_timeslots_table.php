<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnavailableTimeslotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unavailable_timeslots', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('delegate_id')->unsigned()->nullable();
	        $table->foreign('delegate_id')->references('id')->on('delegates');

	        $table->unsignedInteger('timeslot_id');

	        $table->string('type');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unavailable_timeslots');
    }
}
