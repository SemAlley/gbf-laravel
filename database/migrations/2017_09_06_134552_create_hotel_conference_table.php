<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelConferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_conference', function (Blueprint $table) {
            $table->increments('id');

	        $table->integer('conference_id')->unsigned()->nullable();
	        $table->foreign('conference_id')->references('id')
	              ->on('conferences');

	        $table->integer('hotel_id')->unsigned()->nullable();
	        $table->foreign('hotel_id')->references('id')
	              ->on('hotels');

	        $table->string('distance')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_conference');
    }
}
