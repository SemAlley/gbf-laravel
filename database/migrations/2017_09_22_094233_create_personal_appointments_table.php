<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_appointments', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer('day_id')->unsigned()->nullable();
	        $table->foreign('day_id')->references('id')->on('days');

	        $table->integer('delegate_id')->unsigned()->nullable();
	        $table->foreign('delegate_id')->references('id')->on('delegates');

	        $table->string('title');
	        $table->string('time_from');
	        $table->string('time_to');
	        $table->string('comment')->nullable();

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_appointments');
    }
}
