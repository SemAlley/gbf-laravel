@extends('admin.layouts.app')

@section('page_title')
    Meeting Management. Days
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <day-table
                        :conference="{{$conference}}"
                        :days="{{$days}}"
                ></day-table>

            </div>
        </div>
    </div>
@endsection
