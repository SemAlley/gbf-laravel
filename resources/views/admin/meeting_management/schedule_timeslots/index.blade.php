@extends('admin.layouts.app')

@section('page_title')
    Meeting Management. Schedule Timeslots
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <schedule-timeslot-table
                        :conference="{{$conference}}"
                        :days="{{$days}}"
                        :speakers="{{$speakers}}"
                ></schedule-timeslot-table>

            </div>
        </div>
    </div>
@endsection
