@extends('admin.layouts.app')

@section('page_title')
    Meeting Management. Ranks Capacity
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <capacity-table
                        :conference="{{$conference}}"
                        :data="{{$data}}"
                ></capacity-table>

            </div>
        </div>
    </div>
@endsection