@extends('admin.layouts.app')

@section('page_title')
    Meeting Management. Corporates
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <corporates-table :corporates="{{$corporates}}" :delegates="{{$delegates}}" :meetings="{{$meetings}}" :grouped="{{$grouped}}" ></corporates-table>

            </div>
        </div>
    </div>
@endsection