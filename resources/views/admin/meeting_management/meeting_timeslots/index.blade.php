@extends('admin.layouts.app')

@section('page_title')
    Meeting Management. Meeting Timeslots
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <meeting-timeslot-table
                        :conference="{{$conference}}"
                        :days="{{$days}}"
                ></meeting-timeslot-table>

            </div>
        </div>
    </div>
@endsection
