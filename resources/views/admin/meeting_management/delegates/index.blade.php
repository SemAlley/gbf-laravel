@extends('admin.layouts.app')

@section('page_title')
    Delegate 1-1 Meetings
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <delegates-meeting-table
                        :conference="{{$conference}}"
                        :downloadLink="'{{ route('admin.meeting-management.delegates.download') }}'"
                />

            </div>
        </div>
    </div>
@endsection
