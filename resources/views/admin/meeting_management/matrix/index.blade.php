@extends('admin.layouts.app')

@section('page_title')
    Meeting Management. Matrix
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <matrix-table
                        :conference="{{$conference}}"
                        :matrix="{{$matrix}}"
                ></matrix-table>

            </div>
        </div>
    </div>
@endsection