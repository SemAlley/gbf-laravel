@extends('admin.layouts.app')

@section('page_title')
    Delegate Dashboard Page
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <content-base-dashboard :content="{{$content}}"
                                    :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"></content-base-dashboard>

            </div>
        </div>
    </div>
@endsection
