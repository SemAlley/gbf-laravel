@extends('admin.layouts.app')

@section('page_title')
    Hotel Finder Page
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <content-base-hotel-finder :content="{{$content}}"
                                           :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"
                                           :hotels="{{$hotels}}"
                                           :hotels-ordered="{{$ordered_hotels}}"
                                           :pages="{{$customPages}}"></content-base-hotel-finder>

            </div>
        </div>
    </div>
@endsection
