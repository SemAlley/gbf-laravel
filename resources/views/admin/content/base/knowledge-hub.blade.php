@extends('admin.layouts.app')

@section('page_title')
    Conference Knowledge Hub Page
@endsection

@section('main-content')
    <div id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <content-base-knowledge-hub :content="{{$content}}"
                                                :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"
                                                :asset-files="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','file'))}}"
                                                :asset-galleries="{{json_encode($galleries)}}">

                    </content-base-knowledge-hub>

                </div>
            </div>
        </div>
    </div>
@endsection
