@extends('admin.layouts.app')

@section('page_title')
    Conference Contact Page
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <content-base-contact :content="{{$content}}"
                                    :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"></content-base-contact>

            </div>
        </div>
    </div>
@endsection
