@extends('admin.layouts.app')

@section('page_title')
    Manage Galleries
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <gallery-list :values="{{ json_encode($values) }}"
                              :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','gallery'))}}"
                ></gallery-list>
            </div>
        </div>
    </div>
@endsection