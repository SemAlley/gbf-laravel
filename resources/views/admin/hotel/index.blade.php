@extends('admin.layouts.app')

@section('page_title')
    Manage Hotels
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                            <hotel-table
                                    :values="{{ $hotels }}"
                                    :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"></hotel-table>


            </div>
        </div>
    </div>
@endsection
