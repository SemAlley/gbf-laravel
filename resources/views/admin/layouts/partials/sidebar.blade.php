<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <conference-switcher :conference-list="{{ GeneralHelper::CollectionToSelect2(App\Conference::all()) }}"
                             :current-conference="{{ Auth::user()->conference ? Auth::user()->conference->id : 0 }}"></conference-switcher>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">

            @if(Auth::user()->conference)

                <li class="header">CONFERENCE MANAGEMENT</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="{{Active::checkRoute(['admin.dashboard']) ? 'active' : ''}}"><a href="/a"><i
                                class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
                <li class="{{Active::checkRoute(['admin.speakers']) ? 'active' : ''}}"><a href="/a/speakers"><i
                                class='fa fa-podcast'></i> <span>Speakers</span></a></li>

                <li><a href="/a/conference"><i class='fa fa-calendar'></i> <span>Programme</span></a></li>

                <li class="{{Active::checkRoute(['admin.contact']) ? 'active' : ''}}"><a href="/a/contact"><i
                                class='fa fa-envelope'></i> <span>Contact Submissions</span></a></li>

                <li class="{{Active::checkRoute(['admin.media']) ? 'active' : ''}}"><a href="/a/media"><i
                                class='fa fa-file-video-o'></i> <span>Media Registrations</span></a></li>

                <li class="header">DELEGATE MANAGEMENT</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="{{Active::checkRoutePattern(['admin.delegate.get', 'admin.delegate.upload-csv-page']) ? 'active' : ''}}">
                    <a href="{{ route('admin.delegate.get') }}">
                        <i class='fa fa-users'></i>
                        <span>Delegates</span>
                    </a>
                </li>
                <li><a href="/a/push/send"><i class='fa fa-phone-square'></i> <span>Send Push Notification</span></a>
                </li>

                <li class="header">CONTENT MANAGEMENT</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="{{Active::checkRoute(['admin.media-manager']) ? 'active' : ''}}"><a
                            href="/a/content/media-manager"><i class='fa fa-image'></i> <span>Media Manager</span></a>
                </li>
                <li class="{{Active::checkRoute(['admin.galleries']) ? 'active' : ''}}"><a
                            href="/a/content/galleries"><i class='fa fa-object-group'></i> <span>Galleries</span></a>
                </li>
                <li class="{{Active::checkRoute(['admin.hotel.index']) ? 'active' : ''}}">
                    <a href="{{route('admin.hotel.index')}}">
                        <i class='fa fa-building'></i> <span>Hotels</span>
                    </a>
                </li>
                <li class="{{Active::checkRoute(['admin.content.global']) ? 'active' : ''}}"><a
                            href="/a/content/global"><i class='fa fa-cogs'></i> <span>Global Content</span></a></li>
                <li class="treeview {{Active::checkUriPattern('a/content/base/*') ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-file-text"></i>
                        <span>Base Pages</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{Active::checkRoute(['admin.content.base.home']) ? 'active' : ''}}"><a
                                    href="/a/content/base/home"><i class="fa fa-circle-o"></i> Home</a></li>
                        <li class="{{Active::checkRoute(['admin.content.base.speakers']) ? 'active' : ''}}"><a
                                    href="/a/content/base/speakers"><i class="fa fa-circle-o"></i> Speakers</a></li>
                        <li class="{{Active::checkRoute(['admin.content.base.programme']) ? 'active' : ''}}"><a
                                    href="/a/content/base/programme"><i class="fa fa-circle-o"></i> Programme</a></li>
                        <li class="{{Active::checkRoute(['admin.content.base.login']) ? 'active' : ''}}"><a
                                    href="/a/content/base/login"><i class="fa fa-circle-o"></i> Login</a></li>
                        <li class="{{Active::checkRoute(['admin.content.base.register']) ? 'active' : ''}}"><a
                                    href="/a/content/base/register"><i class="fa fa-circle-o"></i> Register</a></li>
                        <li class="{{Active::checkRoute(['admin.content.base.knowledge-hub']) ? 'active' : ''}}"><a
                                    href="/a/content/base/knowledge-hub"><i class="fa fa-circle-o"></i> Knowledge
                                Hub</a></li>
                        <li class="{{Active::checkRoute(['admin.content.base.contact']) ? 'active' : ''}}">
                            <a href="/a/content/base/contact"><i class="fa fa-circle-o"></i> Contact</a>
                        </li>
                        <li class="{{Active::checkRoute(['admin.content.base.media-registration']) ? 'active' : ''}}">
                            <a href="/a/content/base/media-registration"><i class="fa fa-circle-o"></i> Media
                                Registration</a>
                        </li>
                        <li class="{{Active::checkRoute(['admin.content.base.hotel-finder']) ? 'active' : ''}}">
                            <a href="/a/content/base/hotel-finder"><i class="fa fa-circle-o"></i> Hotel Finder</a>
                        </li>
                        <li class="{{Active::checkRoute(['admin.content.base.dashboard']) ? 'active' : ''}}">
                            <a href="/a/content/base/dashboard"><i class="fa fa-circle-o"></i> Dashboard</a>
                        </li>
                    </ul>
                </li>

                <li class="{{Active::checkUriPattern(['a/cms/*']) ? 'active' : ''}} treeview">
                    <a href="#">
                        <i class='fa fa-file-text-o'></i> <span>Custom Pages</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{Active::checkRoute(['admin.cms.create']) ? 'active' : ''}}">
                            <a href="/a/cms/create"><i class="fa fa-circle-o"></i> Create New Page</a>
                        </li>
                        <li class="{{Active::checkRoute(['admin.cms.list']) ? 'active' : ''}}">
                            <a href="/a/cms/list"><i class="fa fa-circle-o"></i> View All Pages</a>
                        </li>
                    </ul>
                </li>


                <li class="header">EMAIL</li>

                <li class="{{Active::checkRoute(['admin.email.global']) ? 'active' : ''}}">
                    <a href="/a/email/global"><i class="fa fa-circle-o"></i> Global Templates</a>
                </li>
                <li class="{{Active::checkRoute(['admin.email.edit-page']) ? 'active' : ''}}">
                    <a href="/a/email/edit"><i class="fa fa-circle-o"></i> Edit Email Content</a>
                </li>


                <li class="header">MEETING PLATFORM</li>

                <li class="{{Active::checkRoute(['admin.meeting-management.delegates.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/delegates"><i class="fa fa-users" aria-hidden="true"></i> Delegate Meetings</a>
                </li>

                <li class="{{Active::checkRoute(['admin.meeting-management.days.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/days"><i class="fa fa-calendar" aria-hidden="true"></i> Days</a>
                </li>

                <li class="{{Active::checkRoute(['admin.meeting-management.schedule-timeslots.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/schedule-timeslots"><i class="fa fa-calendar-check-o"
                                                                          aria-hidden="true"></i> Schedule Timeslots</a>
                </li>

                <li class="{{Active::checkRoute(['admin.meeting-management.meeting-timeslots.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/meeting-timeslots"><i class="fa fa-clock-o" aria-hidden="true"></i>
                        Meeting Timeslots</a>
                </li>

                <li class="{{Active::checkRoute(['admin.meeting-management.corporates.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/meeting-corporates"><i class="fa fa-id-card" aria-hidden="true"></i>
                        Meeting Corporates</a>
                </li>

                <li class="{{Active::checkRoute(['admin.meeting-management.matrix.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/meeting-matrix"><i class="fa fa-handshake-o" aria-hidden="true"></i>
                        Meeting Matrix</a>
                </li>

                <li class="{{Active::checkRoute(['admin.meeting-management.capacity.index']) ? 'active' : ''}}">
                    <a href="/a/meeting-management/meeting-matrix/capacity"><i class="fa fa-cogs"
                                                                               aria-hidden="true"></i>Ranks Capacity</a>
                </li>





            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
