@extends('admin.layouts.app')

@section('page_title')
    CMS: Update Page
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <cms-create-page
                        :asset-images="{{App\Helpers\General::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"
                        :asset-files="{{App\Helpers\General::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','file'))}}"
                        :content='{{$content}}'
                        :page_list="{{$pageList}}"
                ></cms-create-page>

            </div>
        </div>
    </div>
@endsection
