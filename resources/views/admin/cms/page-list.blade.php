@extends('admin.layouts.app')

@section('page_title')
    CMS: Page List
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <page-list
                        :content="{{$content}}"
                ></page-list>

            </div>
        </div>
    </div>
@endsection
