@extends('admin.layouts.app')

@section('page_title')
    Send Push Notification
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <send-push-notification :devices="{{$devices}}"></send-push-notification>

            </div>
        </div>
    </div>
@endsection
