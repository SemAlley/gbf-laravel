@extends('admin.layouts.app')

@section('page_title')
    Edit Emails
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <email-single-content
                        :content-welcome="{{$welcome}}"
                        :content-eticket="{{$eticket}}"
                        :content-blank="{{$blank}}"
                        :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"></email-single-content>

            </div>
        </div>
    </div>
@endsection