@extends('admin.layouts.app')

@section('page_title')
    Global Content
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <email-global-content :content="{{$content}}"
                                      :asset-images="{{GeneralHelper::ImageCollectionToSelect2(Auth::user()->conference->assets->where('type','images'))}}"></email-global-content>

            </div>
        </div>
    </div>
@endsection