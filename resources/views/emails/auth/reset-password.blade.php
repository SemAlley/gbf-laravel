@component('mail::message')


    @component('mail::panel')
        <h3>You are receiving this email because we received a password reset request for your account.</h3>
    @endcomponent

    @component('mail::button', ['url' => $url])
        Reset Password
    @endcomponent

    <p>If you did not request a password reset, no further action is required.</p>

    <br><br><br><br>
    Thanks,<br>
    {{ config('app.name') }}
@endcomponent