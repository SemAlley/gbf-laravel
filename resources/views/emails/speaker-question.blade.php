@component('mail::message')

    <h3>New speaker question submitted:</h3>


    <h4>Question data:</h4>

    <table>
        {{--<tr>--}}
            {{--<td>Speaker</td>--}}
            {{--<td>{{$content['speaker']}}</td>--}}
        {{--</tr>--}}
        <tr>
            <td>Question</td>
            <td>{{$content['question']}}</td>
        </tr>
    </table>

    <br><br><br><br>
    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
