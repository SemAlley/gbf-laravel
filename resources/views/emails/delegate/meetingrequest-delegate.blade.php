@component('mail::layout')
    <div class="container">
        {{-- Header --}}
        @slot('header')
            @component('mail::header', ['url' => config('app.url')])
                @isset($header)
                    {!! $header !!}
                @endisset
            @endcomponent
        @endslot

        {{-- Body --}}

        <h3>New Meeting Request</h3>

        <p>You have a new meeting request from {{$delegate['title']}} {{$delegate['first_name']}} {{$delegate['last_name']}}</p>

        <p><b>PERSONAL NOTE:</b> {{$note}}</p>

        <p>Click <a href="https://globalbusinessforum.com/latin-america/login">here</a> to login and respond to the request.</p>
        <br/>
        <p>Kind regards,</p>
        <p><b>GBF Latin America Team</b></p><br>


        {{-- Subcopy --}}
        @slot('subcopy')
            @component('mail::subcopy')
            <!-- subcopy here -->
            @endcomponent
        @endslot


        {{-- Footer --}}
        @slot('footer')
            @component('mail::footer')
                @isset($footer)
                    {!! $footer !!}
                @endisset
            @endcomponent
        @endslot

   
 </div>

@endcomponent
<style>

    .container {

    }

    .container img {
        max-width: 860px;
    }

    .uic {
        font-size:20px;
    }

    .main-bg {
        background-color: {{$global['primary_color_1']}};
    }

    .sub-main-bg {
        background-color: #484848;
    }

    .sub-bg {
        background-color: #dfdfdf;
        padding: 40px 0;
    }

    .main-bg p {
        text-transform: uppercase;
        color: #fff;
    }

    .sub-main-bg p {
        color: #fff;
    }

    .sub-bg p {
        color: #727272;
        text-transform: uppercase;
        font-size: 22px;
        margin: 15px 0;
    }

    .register {
        display: inline-block;
        padding: 8px 20px;
        text-transform: uppercase;
        text-decoration: none;
        color: #fff;
        background-color: {{$global['primary_color_2']}};
        font-size:18px;
        margin-top: 30px;
    }
    .pre-footer {
        text-align: center;
        font-weight:700;
    }
    .pre-footer p {
        margin-bottom: 0;
    }
</style>
