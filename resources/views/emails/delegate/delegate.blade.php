@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            @isset($header)
                {!! $header !!}
            @endisset
        @endcomponent
    @endslot

    {{-- Body --}}
    @isset($body)
        {!! $body !!}
    @endisset

    <table width="100%" cellpadding="0" cellspacing="0" class="pre-footer">
        <tr>
            <td class="main-bg" align="center">
                <p>You are registered</p>
            </td>
        </tr>
        <tr>
            <td class="sub-bg" align="center">
                {{--<a href="{{$url}}" class="login">Login</a>--}}
                <div class="barcode" style="width:300px;">
                    <img src='http://globalbusinessforum.com/barcode/get/{{$delegate->uic}}.jpg' width="256" />
                    <p>{{$delegate->uic}}</p>
                </div>
            </td>
        </tr>

    </table>

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            @isset($footer)
                {!! $footer !!}
            @endisset
        @endcomponent
    @endslot
@endcomponent
<style>
    .main-bg {
        background-color: {{$global['primary_color_1']}};
    }

    .sub-main-bg {
        background-color: {{$global['primary_color_2']}};
    }

    .login {
        display: inline-block;
        padding: 5px 15px;
        text-transform: uppercase;
        text-decoration: none;
        color: #fff;
        background-color: {{$global['primary_text_2']}};
    }
    .register {
        display: inline-block;
        padding: 5px 15px;
        text-transform: uppercase;
        text-decoration: none;
        color: #fff;
        background-color: {{$global['primary_text_2']}};
        margin-top: 30px;
    }
</style>
