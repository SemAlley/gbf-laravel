@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            @isset($header)
                {!! $header !!}
            @endisset
        @endcomponent
    @endslot

    {{-- Body --}}
    @isset($body)
        {!! $body !!}
    @endisset

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            @isset($footer)
                {!! $footer !!}
            @endisset
        @endcomponent
    @endslot
@endcomponent
<style>
    .main-bg {
        background-color: {{$global['primary_color_1']}};
    }

    .sub-main-bg {
        background-color: {{$global['primary_color_2']}};
    }

    .login {
        display: inline-block;
        padding: 5px 15px;
        text-transform: uppercase;
        text-decoration: none;
        color: #fff;
        background-color: {{$global['primary_text_2']}};
    }
    .register {
        display: inline-block;
        padding: 5px 15px;
        text-transform: uppercase;
        text-decoration: none;
        color: #fff;
        background-color: {{$global['primary_text_2']}};
        margin-top: 30px;
    }
</style>
