@component('mail::message')


    @component('mail::panel')
        <h3>New delegate was registered</h3>
    @endcomponent

    <table>
        <tr>
            <th>id</th>
            <td>{{$delegate->id}}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{$delegate->email}}</td>
        </tr>
        <tr>
            <th>Title</th>
            <td>{{$delegate->title}}</td>
        </tr>
        <tr>
            <th>First Name</th>
            <td>{{$delegate->first_name}}</td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td>{{$delegate->last_name}}</td>
        </tr>
        <tr>
            <th>Country</th>
            <td>{{$delegate->country}}</td>
        </tr>
    </table>

    <br><br><br><br>
    Thanks,<br>
    {{ config('app.name') }}
@endcomponent