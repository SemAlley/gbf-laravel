@component('mail::layout')
    <div class="container">
        {{-- Header --}}
        @slot('header')
            @component('mail::header', ['url' => config('app.url')])
                @isset($header)
                    {!! $header !!}
                @endisset
            @endcomponent
        @endslot

        {{-- Body --}}
        @isset($body)
            {!! $body !!}
        @endisset


        @foreach($days as $day)
            <table class="planner">
                <tr>
                    <td colspan="2" class="day-name">{{$day['name']}}</td>
                </tr>

                @foreach($day['schedule_timeslots'] as $timeslot)

                    @if($timeslot['type'] == 'meeting_timeslots')

                        @foreach($timeslot['meeting_requests'] as $request)
                            @if($request['status'] == 'confirmed')
                                <tr class="meeting-timeslot">
                                    <td class="timeslot-time">{{$timeslot['time_from']}} - {{$timeslot['time_to']}}</td>
                                    <td class="timeslot-title">
                                        @if($request['delegate_id'] == $delegate->id)
                                            {{$request['target']['first_name']}} {{$request['target']['last_name']}}
                                        @else
                                            {{$request['delegate']['first_name']}} {{$request['delegate']['last_name']}}
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                    @elseif($timeslot['type'] == 'personal_appointments')
                        <tr class="personal-appointment">
                            <td class="timeslot-time">{{$timeslot['time_from']}} - {{$timeslot['time_to']}}</td>
                            <td class="timeslot-title">{{$timeslot['title']}}</td>
                        </tr>
                    @else
                        <tr class="program-timeslot">
                            <td class="timeslot-time">{{$timeslot['time_from']}} - {{$timeslot['time_to']}}</td>
                            <td class="timeslot-title">{{$timeslot['title']}}</td>
                        </tr>
                    @endif

                @endforeach

            </table>
        @endforeach


        <table width="100%" cellpadding="0" cellspacing="0" class="pre-footer">
            <tr>
                <td class="sub-bg" align="center">
                    <a href="{{$dashboard_url}}" class="link-button">My Planner</a>
                </td>
            </tr>

        </table>

        {{-- Subcopy --}}
        @slot('subcopy')
            @component('mail::subcopy')
            <!-- subcopy here -->
            @endcomponent
        @endslot


        {{-- Footer --}}
        @slot('footer')
            @component('mail::footer')
                @isset($footer)
                    {!! $footer !!}
                @endisset
            @endcomponent
        @endslot

    </div>

@endcomponent
<style>
    .planner {
        margin-top: 15px;
        border: 1px solid {{$global['primary_color_2']}};
    }

    .day-name {
        background-color: {{$global['primary_color_2']}};
        color: #fff;
        text-align: center;
    }

    .program-timeslot td, .personal-appointment td {
        color: #fff;
        background-color: {{$global['primary_color_1']}};
    }

    .meeting-timeslot td {
        background-color: #fff;
        color: {{$global['primary_color_1']}};
    }

    .timeslot-time {
        width: 20%;
    }

    .main-bg {
        background-color: {{$global['primary_color_1']}};
    }

    .sub-main-bg {
        background-color: {{$global['primary_color_2']}};
    }

    .pre-footer {
        margin-top: 35px;
    }

    .link-button {
        display: inline-block;
        padding: 5px 15px;
        text-transform: uppercase;
        text-decoration: none;
        color: #fff;
        background-color: {{$global['primary_text_2']}};
    }
</style>
