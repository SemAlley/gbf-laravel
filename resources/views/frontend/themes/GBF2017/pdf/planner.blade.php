<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <style>
        body {
            font-family: "DIN Regular", Arial, sans-serif;
        }

        .page-heading {
            padding: 30px;
        }

        .page-heading h1 {
            text-align: center;
            text-transform: uppercase;
            font-family: "DIN Light", Arial, sans-serif;
            font-size: 60px;
            line-height: 60px;
            margin: 0px 0px 10px 0px;
            color: #444;
        }

        .tab-content {
            background: #f3f3f3;
            padding: 40px;
        }

        .day-title {
            text-align: center;
            background-color: {{$global['primary_color_2']}};
            color: #fff;
            padding: 15px;
            margin: 0;
        }

        .panel-group {
            background-color: #fff;
        }

        .planner-inner {
            padding: 15px;
        }

        .left-time, .right-details {
            display: inline-block;
            width: 20%;
            vertical-align: top;
            text-transform: uppercase;
        }

        .right-details {
            width: 75%;
        }

        .meeting-requests, .information-item {
            border: 1px solid #d5d5d5;
            padding: 10px;
        }

        .meeting-requests .right-details,
        .information-item .right-details {
            color: {{$global['primary_color_1']}};
        }

        .personal-appointments,
        .programme-item {
            padding: 10px;
            background-color: {{$global['primary_color_1']}};
            color: #fff;
        }

        .programme-description {
            padding: 20px 40px;
            border: 1px solid #d5d5d5;
            border-top: none;
            page-break-before: avoid;
        }

        a.link {
            color: {{$global['primary_color_1']}};
            text-decoration: none;
        }

        .description-content a {
            color: inherit;
            text-decoration: none;
        }

        .meeting-requests, .information-item, .personal-appointments, .programme-description {
            margin-bottom: 15px;
        }

        p {
            margin: 5px 0;
        }

        .no-requests {
            color: #ccc;
        }
        .panel-body h4 {
            margin-top: 25px;
            margin-bottom: 5px;
        }

    </style>

</head>
<body class="tabs form">

<div class="page-heading container-padding">
    <h1>My Planner</h1>
</div>

<div class="tabs-copy custom-tabs accordion dashboard">
    <div id="dashboard-tab">

        <div class="tab-content container-padding extra-top">
            <div class="tab-pane active" id="myplanner">
                <div class="my-planner-wrapper">

                    @foreach($days as $day)
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <h3 class="day-title">
                                    <a role="button">{{$day['name']}}</a>
                                </h3>
                                <div>
                                    <div class="planner-inner">
                                        <div>

                                            @foreach($day['schedule_timeslots'] as $timeslot)

                                                @if($timeslot['type'] == 'meeting_timeslots')
                                                    @if(!empty($timeslot['meeting_requests']))
                                                        @foreach($timeslot['meeting_requests'] as $request)
                                                            @if($request['status'] == 'confirmed')
                                                                <div class="meeting-requests">
                                                                    <div class="left-time">
                                                                        <p>{{$timeslot['time_from']}}
                                                                            - {{$timeslot['time_to']}}</p>
                                                                    </div>
                                                                    <div class="right-details">
                                                                        @if($request['delegate_id'] == $delegate->id)
                                                                            <p>
                                                                                <span>{{$request['target']['first_name']}} {{$request['target']['last_name']}}</span>
                                                                            </p>
                                                                            <p>{{$request['target']['job_title']}}</p>
                                                                        @else
                                                                            <p>
                                                                                <span>{{$request['delegate']['first_name']}} {{$request['delegate']['first_name']}}</span>
                                                                            </p>
                                                                            <p>{{$request['delegate']['job_title']}}</p>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <div class="meeting-requests">
                                                            <div class="left-time">
                                                                <p>{{$timeslot['time_from']}}
                                                                    - {{$timeslot['time_to']}}</p>
                                                            </div>
                                                            <div class="right-details">
                                                                <p class="no-requests">No Requests</p>
                                                            </div>
                                                        </div>
                                                    @endif

                                                @elseif($timeslot['type'] == 'personal_appointments')
                                                    <div class="accordion programme">
                                                        <div>
                                                            <div class="panel-group">
                                                                <div class="programme-item">
                                                                    <div class="left-time">
                                                                        <p>{{$timeslot['time_from']}}
                                                                            - {{$timeslot['time_to']}}</p>
                                                                    </div>
                                                                    <div class="right-details">
                                                                        <p><span>{{$timeslot['title']}}</span></p>
                                                                    </div>
                                                                </div>
                                                                <div class="programme-description">
                                                                    <div class="panel-body">
                                                                        <div class="description-content">
                                                                            {!! $timeslot['comment'] !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                @elseif($timeslot['type'] == 'information_item')
                                                    <div class="information-item">

                                                        <div class="left-time">
                                                            <p>{{$timeslot['time_from']}}
                                                                - {{$timeslot['time_to']}}</p>
                                                        </div>
                                                        <div class="right-details">
                                                            <p><span>{{$timeslot['title']}}</span></p>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="accordion programme">
                                                        <div>
                                                            <div class="panel-group">
                                                                <div class="programme-item">
                                                                    <div class="left-time">
                                                                        <p>{{$timeslot['time_from']}}
                                                                            - {{$timeslot['time_to']}}</p>
                                                                    </div>
                                                                    <div class="right-details">
                                                                        <p><span>{{$timeslot['title']}}</span></p>
                                                                    </div>
                                                                </div>
                                                                <div class="programme-description">
                                                                    <div class="panel-body">
                                                                        <div class="description-content">
                                                                            {!! $timeslot['description'] !!}
                                                                        </div>
                                                                        @if($timeslot['speakers'])
                                                                            <h4>Confirmed speaker:</h4>
                                                                            @foreach($timeslot['speakers'] as $speaker)
                                                                                <p>
                                                                                    <a class="link">{{$speaker['name']}}</a>,
                                                                                    {{$speaker['company_title']}},
                                                                                    {{$speaker['company']}}
                                                                                </p>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                @endif

                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                            </div>
                            @endforeach

                        </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
