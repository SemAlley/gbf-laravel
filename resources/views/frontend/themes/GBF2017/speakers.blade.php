@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => $speakers['header_image_override'],
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title'){{$speakers['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($speakers['meta_description']) ? $speakers['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div class="page-heading container-padding page-heading-less">
        <ol class="breadcrumb">
            <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
            <li><a href="{{General::conferenceRoute('frontend.speakers')}}">speakers</a></li>
            <li class="active"><a class="active-breadcrumb" href="#">@if(!$speakers['featured_heading']) {{$speakers['featured_heading']}} @else {{$speakers['confirmed_heading']}} @endif</a></li>
        </ol>
        <h1>{{$speakers['page_heading']}}</h1>
        @if(isset($speakers['page_subheading']) && $speakers['page_subheading'] != "")
            <hr/>
            {!! $speakers['page_subheading'] !!}
        @endif
    </div>

    <div class="tabs-copy">
        <div id="speakers-tab">
            <ul class="nav nav-tabs container">
                @if($speakers['confirmed_heading'])
                    <li class="active">
                        <a href="##confirmed-speakers" class="tab-link"
                           data-toggle="tab">{{$speakers['confirmed_heading']}}</a>
                    </li>
                @endif
                    @if($speakers['featured_heading'])
                        <li class="@if(!$speakers['featured_heading']) active @endif">
                            <a href="##featured-speakers" class="tab-link"
                               data-toggle="tab">{{$speakers['featured_heading']}}</a>
                        </li>
                    @endif
                @if($speakers['previous_heading'])
                    <li @if($speakers['confirmed_heading'] == "") class="active" @endif>
                        <a href="##previous-speakers" class="tab-link"
                           data-toggle="tab">{{$speakers['previous_heading']}}</a>
                    </li>
                @endif
            </ul>
            <div class="tab-content container-padding">
                @if($speakers['featured_heading'])
                    <div class="tab-pane" id="featured-speakers">
                        {!! $speakers['featured_text'] !!}
                        <div class="speaker-profile-container">
                            @foreach($featured as $s)
                                <div class="col-md-3 col-sm-6 speaker-profile center-block">
                                    <a href="{{General::conferenceRoute('frontend.speakers.profile', $s['id'])}}"
                                       class="speaker-profile-data">
                                        <img src="{{App\Content::AssetPath($s['photo'])}}" alt=""
                                             class="img-responsive"/>
                                        <div class="profile-block">
                                            <h3>{{$s['name']}}</h3>
                                            <p>{{$s['company_title']}}</p>
                                            <p>{{$s['company']}}</p>
                                            <p>{{$s['country']}}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                @if($speakers['confirmed_heading'])
                    <div class="tab-pane active " id="confirmed-speakers">
                        {!! $speakers['confirmed_text'] !!}
                        <div class="speaker-profile-container">
                            @foreach($confirmed as $s)
                                <div class="col-md-3 col-sm-6 speaker-profile center-block">
                                    <a href="{{General::conferenceRoute('frontend.speakers.profile', $s['id'])}}"
                                       class="speaker-profile-data">
                                        <img src="{{App\Content::AssetPath($s['photo'])}}" alt=""
                                             class="img-responsive"/>
                                        <div class="profile-block">
                                            <h3>{{$s['name']}}</h3>
                                            <p>{{$s['company_title']}}</p>
                                            <p>{{$s['company']}}</p>
                                            <p>{{$s['country']}}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                @if($speakers['previous_heading'])
                    <div class="tab-pane @if($speakers['confirmed_heading'] == "") active @endif"
                         id="previous-speakers">
                        {!! $speakers['previous_text'] !!}
                        <div class="speaker-profile-container">
                            @foreach($previous as $s)
                                <div class="col-md-3 col-sm-6 speaker-profile center-block">
                                    <a href="{{General::conferenceRoute('frontend.speakers.profile', $s['id'])}}"
                                       class="speaker-profile-data">
                                        <img src="{{App\Content::AssetPath($s['photo'])}}" alt=""
                                             class="img-responsive"/>
                                        <div class="profile-block">
                                            <h3>{{$s['name']}}</h3>
                                            <p>{{$s['company_title']}}</p>
                                            <p>{{$s['company']}}</p>
                                            <p>{{$s['country']}}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
