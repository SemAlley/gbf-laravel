@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'form',
        'header_image_override' => isset($media['header_image_override'])?$media['header_image_override']:false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title'){{$media['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($media['meta_description']) ? $media['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">media registration</a></li>
            </ol>
            <h1>{{$media['page_heading'] or 'media registration'}}</h1>
            <hr/>
            <p>
                @if(isset($media['page_subheading']))
                    {!! $media['page_subheading'] !!}
                @else
                    Media attendance at the Forum is by accreditation only. Please complete the application form if you
                    wish to attend as media. All fields marked with <sup>*</sup> are mandatory.
                @endif
            </p>
        </div>

        <media-register-form :countries="{{json_encode($countries)}}"
                             :home-url="'{{General::conferenceRoute('frontend.index')}}'"></media-register-form>

    </div>


@endsection
