@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($content['header_image_override'])?$content['header_image_override']:false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title'){{$content['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($content['meta_description']) ? $content['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding page-heading-less">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li><a href="{{General::conferenceRoute('frontend.knowledge-hub')}}">KNOWLEDGE HUB</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">reports</a></li>
            </ol>
            <h1>{{$content['page_heading']}}</h1>
            @if(isset($content['page_subheading']) && $content['page_subheading'] != "")
                <hr/>
                {!! $content['page_subheading'] !!}
            @endif
        </div>


        <div class="tabs-copy knowledge-hub">
            <div id="knowlede-hub-tab">
                <ul class="nav nav-tabs container">
                    <li class="active">
                        <a href="#reports" class="tab-link" data-toggle="tab">REPORTS</a>
                    </li>
                    <li>
                        <a href="#gallery" class="tab-link" data-toggle="tab">GALLERY</a>
                    </li>
                    <li>
                        <a href="#pressreleases" class="tab-link" data-toggle="tab">PRESS RELEASES</a>
                    </li>
                </ul>
                <div class="tab-content container-padding extra-top">
                    <div class="tab-pane active" id="reports">
                        <div class="panel-section">
                            <div class="row text-center">
                                @foreach($reports as $report)
                                    <div class="col-md-4 media-block">
                                        <a href="{{$report['asset']['path']}}"
                                           onclick="window.open(this.href, '_blank'); return false;"
                                           onkeypress="if (event.keyCode==13) {window.open(this.href, '_blank'); return false;}"
                                           download="">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    @if($report['preview'])
                                                        <img src="{{$report['preview']['path']}}" alt="">
                                                    @else
                                                        <img src="{{$report['asset']['path']}}_THUMB.png" alt="">
                                                    @endif
                                                </div>
                                                <div class="panel-body" style="height: 175px;">
                                                    <h3>{{$report['description']}}</h3>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="col-md-6">
                                                        <p>download</p>
                                                    </div>
                                                    <div class="col-md-6">
                                            <span>
                                                <img src="{{asset('/frontend/img/GBF2017/panel_download.png')}}" alt="">
                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="gallery">
                        <div class="panel-section">
                            <div class="row text-center">
                                @foreach($galleries as $gallery)
                                    <div class="col-md-4 media-block gallery-block">
                                        <a href="{{General::conferenceRoute('frontend.gallery-single', $gallery['gallery']['id'])}}">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    @if($gallery['gallery']['preview'])
                                                        <img src="{{$gallery['gallery']['preview']['path']}}" alt="">
                                                    @endif
                                                </div>
                                                <div class="panel-body" style="height: 0px;">
                                                    <h3>{{$gallery['gallery']['title']}}</h3>
                                                    <p>{{$gallery['gallery']['description']}}</p>
                                                </div>
                                                <div class="panel-footer">
                                                    <p>view gallery</p>
                                                    <span>
                                                <img src="{{asset('/frontend/img/GBF2017/panel_arrow.png')}}" alt="">
                                        </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pressreleases">
                        <div class="panel-section">
                            <div class="row text-center">
                                @foreach($press_releases as $release)
                                    <div class="col-md-4 media-block">
                                        <a href="{{$release['asset']['path']}}"
                                           onclick="window.open(this.href, '_blank'); return false;"
                                           onkeypress="if (event.keyCode==13) {window.open(this.href, '_blank'); return false;}"
                                           download="">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    @if($release['preview'])
                                                        <img src="{{$release['preview']['path']}}" alt="">
                                                    @else
                                                        <img src="{{$release['asset']['path']}}_THUMB.png" alt="">
                                                    @endif
                                                </div>
                                                <div class="panel-body" style="height: 175px;">
                                                    <h3>{{$release['description']}}</h3>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="col-md-6">
                                                        <p>download</p>
                                                    </div>
                                                    <div class="col-md-6">
                                            <span>
                                                <img src="{{asset('/frontend/img/GBF2017/panel_download.png')}}" alt="">
                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
