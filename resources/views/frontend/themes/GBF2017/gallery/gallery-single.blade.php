@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'gallery',
        'header_image_override' => isset($media['header_image_override'])?$media['header_image_override']:false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title'){{$media['name'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($media['meta_description']) ? $media['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding page-heading-less">
            <ol class="breadcrumb">

                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li><a href="{{General::conferenceRoute('frontend.knowledge-hub')}}">KNOWLEDGE HUB</a></li>
                <li><a href="{{General::conferenceRoute('frontend.knowledge-hub')}}##gallery">gallery</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">{{$gallery['title']}}</a></li>
            </ol>
            <h1>{{$gallery['title']}}</h1>
        </div>

        <div class="main-copy container-padding extra-top">
            <div class="gallery-section">
                @foreach($gallery->assets as $asset)
                    <div class="slick-slide">
                        <img src="{{$asset->path}}">
                    </div>
                @endforeach
            </div>
            <div class="slider-nav slick-slider">
                @foreach($gallery->assets as $asset)
                    <div class="slick-slide">
                        <img src="{{$asset->path}}" class="img-responsive">
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@endsection
