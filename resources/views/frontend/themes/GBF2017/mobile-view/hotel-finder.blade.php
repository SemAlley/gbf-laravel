@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($content['header_image_override'])?$content['header_image_override']:false,
        'header_register_button' => true
    ];
$centerMap = [
'lat' => $content['lat'] ?? 0,
'lon' => $content['lon'] ?? 0,
'zoom' => $content['zoom'] ?? 16,
];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$content['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($content['meta_description']) ? $content['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding page-heading-less">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">hotel finder</a></li>
            </ol>
            <h1>{{$content['page_heading']}}</h1>
            @if(isset($content['page_subheading']) && $content['page_subheading'] != "")
                <hr/>
                {!! $content['page_subheading'] !!}
            @endif
        </div>

        <div class="main-copy custom-tabs hotel-finder container-padding extra-top">

            <hotel-finder :hotels="{{$hotels}}" :map-center="{{json_encode($centerMap)}}"></hotel-finder>

        </div>

    </div>

@endsection
