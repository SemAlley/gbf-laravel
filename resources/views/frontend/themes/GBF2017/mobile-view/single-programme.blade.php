@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($programme['header_image_override']) ? $programme['header_image_override'] : false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$programme['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($programme['meta_description']) ? $programme['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')


    <style>
        body.tabs .tabs-copy .tab-content .tab-pane p {
            text-align: left;
            padding-bottom:10px;
        }
        body.tabs .custom-tabs .panel .panel-body.travel {
            padding:20px 0 0 15px !important
        }
        html body .container-padding {
            padding: 0 15px !important;
        }
        body.tabs .custom-tabs .panel .panel-body h3 {
            display: none;
        }
        .tab-content {
            background: #f3f3f3;
        }
        body.tabs .custom-tabs h2 {
            margin:0 0 10px 0;
            font-size: 28px;
            clear:both;
        }
        body .programme .panel-group .col-md-10 h2 {
            font-size: 18px !important;
            font-weight: bold;
        }
        .expandAll1 {
            margin: 10px 0 0 0!important;
        }
        .date {
            margin-top: 20px!important;
            padding: 0!important;
            margin-bottom: -20px!important;
        }

        .programme .col-md-10 {
            padding:0 !important;
        }
        body.tabs .accordion .panel-group .panel.panel-default .panel-heading h4.panel-title a {
            font-size:16px !important;
        }
        body.tabs .custom-tabs .panel .panel-body p {
            font-size:14px !important;
        }
        body.tabs .accordion .panel-group .panel.panel-default .accordion-body .panel-body {
            padding: 20px 20px !important;
        }
        .tab-pane .tab-content .background-layer section.africa-live-section .confirmed-speaker .confirmed-block .overlay-background .overlay-outer .overlay-inner p {
            bottom: 5px !important;
        }
        body.tabs .custom-tabs .panel .panel-body .content-block h3 {
            display: block !important;
            font-size: 24px !important;
        }
        .col-md-12 {
            padding-left:0;
        }
    </style>



    <div id="app">

        <div class="tab-content mobile-container-padding">

            @if(isset($tab['quick_link_label']) && $tab['quick_link_label'])
                <div id="{{ strtolower(str_slug($tab['quick_link_label'], '-')) }}"
                     class="tab-pane active">
                    <div class="custom-tabs">
                        <cms-page-tab :data="{{json_encode($tab)}}"></cms-page-tab>
                    </div>
                </div>
            @else
                <div role="tabpanel"
                     class="tab-pane active">
                    <div class="custom-tabs">
                        <cms-page-tab :data="{{json_encode($tab)}}"></cms-page-tab>
                    </div>
                </div>
            @endif
        </div>

    </div>

@endsection
