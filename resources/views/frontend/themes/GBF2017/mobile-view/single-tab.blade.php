@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($programme['header_image_override']) ? $programme['header_image_override'] : false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$programme['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($programme['meta_description']) ? $programme['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')


    <style>
        body.tabs .tabs-copy .tab-content .tab-pane p {
            text-align: left;
        }
        body.tabs .custom-tabs .panel .panel-body.travel {
            padding:20px 0 0 0 !important;
        }
        html body .container-padding {
            padding: 0 15px !important;
        }
        body.tabs .custom-tabs .panel .panel-body h3 {
            display: none;
        }
        body.tabs .custom-tabs .panel .panel-body .content-block h3 {
            display: block !important;
        }
        body.tabs .custom-tabs .panel .panel-body.travel h3 {
            display: block !important;
        }
        .tab-content {
            background: #f3f3f3;
            padding: 15px 15px !important;
        }
        body.tabs .custom-tabs h2 {
            margin:0 0 10px 0;
            font-size: 28px;
        }
        body.tabs .custom-tabs .panel .panel-body .countries-list, body.tabs .custom-tabs .panel .panel-body .countries-list ul {
            margin:0 !important;
        }
    </style>



    <div id="app">

        <div class="tab-content mobile-container-padding">

                @if(isset($tab['quick_link_label']) && $tab['quick_link_label'])
                    <div id="{{ strtolower(str_slug($tab['quick_link_label'], '-')) }}"
                         class="tab-pane active">
                        <div class="custom-tabs">
                            <cms-page-tab :data="{{json_encode($tab)}}"></cms-page-tab>
                        </div>
                    </div>
                @else
                    <div role="tabpanel"
                         class="tab-pane active">
                        <div class="custom-tabs">
                            <cms-page-tab :data="{{json_encode($tab)}}"></cms-page-tab>
                        </div>
                    </div>
                @endif
        </div>

    </div>

@endsection
