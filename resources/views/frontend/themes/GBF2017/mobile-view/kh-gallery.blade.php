@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($programme['header_image_override']) ? $programme['header_image_override'] : false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$programme['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($programme['meta_description']) ? $programme['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')


    <style>
        body.tabs .tabs-copy .tab-content .tab-pane p {
            text-align: left;
        }
        body.tabs .custom-tabs .panel .panel-body.travel {
            padding:20px 0 0 0 !important;
        }
        html body .container-padding {
            padding: 0 15px !important;
        }
        body.tabs .custom-tabs .panel .panel-body h3 {
            display: none;
        }
        .tab-content {
            background: #f3f3f3;
            padding: 15px 15px !important;
        }
        body.tabs .custom-tabs h2 {
            margin:0 0 10px 0;
            font-size: 28px;
        }
    </style>



    <div id="app">

        <div class="tab-content mobile-container-padding">

            <div class="tab-pane active" id="reports">
                <div class="panel-section">
                    <div class="row text-center">
                        @foreach($galleries as $gallery)
                            <div class="col-md-4 media-block gallery-block">
                                <a href="{{General::conferenceRoute('frontend.gallery-single', $gallery['gallery']['id'])}}">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            @if($gallery['gallery']['preview'])
                                                <img src="{{$gallery['gallery']['preview']['path']}}" alt="">
                                            @endif
                                        </div>
                                        <div class="panel-body" style="height: 0px;">
                                            <h3>{{$gallery['gallery']['title']}}</h3>
                                            <p>{{$gallery['gallery']['description']}}</p>
                                        </div>
                                        <div class="panel-footer">
                                            <p>view gallery</p>
                                            <span>
                                                <img src="{{asset('/frontend/img/GBF2017/panel_arrow.png')}}" alt="">
                                        </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
