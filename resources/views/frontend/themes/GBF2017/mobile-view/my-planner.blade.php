@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($programme['header_image_override']) ? $programme['header_image_override'] : false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$programme['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($programme['meta_description']) ? $programme['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')


    <style>
        body.tabs .tabs-copy .tab-content .tab-pane p {
            text-align: left;
        }
        body.tabs .custom-tabs .panel .panel-body.travel {
            padding:20px 0 0 0 !important;
        }
        html body .container-padding {
            padding: 0 15px !important;
        }
        body.tabs .custom-tabs .panel .panel-body h3 {
            display: none;
        }
    </style>



    <div id="app">

        <div class="tabs-copy custom-tabs accordion dashboard">
            <div id="dashboard-tab">
                <div class="tab-content container-padding">

                    <div class="tab-pane active" id="myplanner">
                        <profile-planner
                                :delegate="{{json_encode($delegate)}}"
                                :speaker-path="'{{General::conferenceRoute('frontend.speakers')}}'"
                        >
                        </profile-planner>
                    </div>

                    <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logoutLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="logoutLabel">Logout</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="text-center">Are you sure you want to log out?</p>
                                    <div class="request-action text-center">
                                        <a class="btn btn-default" data-dismiss="modal" aria-label="Close">cancel</a>
                                        <a href="{{General::conferenceRoute('frontend.logout')}}" class="btn btn-default">ok</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
