@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'form',
        'header_image_override' => isset($contact['header_image_override'])?$contact['header_image_override']:false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$contact['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($contact['meta_description']) ? $contact['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">contact</a></li>
            </ol>
            <h1>{{$contact['page_heading'] or 'Contact Us'}}</h1>
            <hr/>
            <p>{!! isset($contact['page_subheading']) ? $contact['page_subheading'] : 'All fields marked with * are mandatory.' !!}</p>
        </div>

        <contact-form :counties="{{json_encode($countries)}}" :nature-of-enquiry="{{$contact['nature_of_enquiry'] or json_encode([])}}"></contact-form>

    </div>

@endsection
