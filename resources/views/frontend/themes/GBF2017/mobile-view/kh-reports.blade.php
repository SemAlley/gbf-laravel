@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($programme['header_image_override']) ? $programme['header_image_override'] : false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.mobile-view.layouts.app')

@section('meta_title'){{$programme['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($programme['meta_description']) ? $programme['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')


    <style>
        body.tabs .tabs-copy .tab-content .tab-pane p {
            text-align: left;
        }
        body.tabs .custom-tabs .panel .panel-body.travel {
            padding:20px 0 0 0 !important;
        }
        html body .container-padding {
            padding: 0 15px !important;
        }
        body.tabs .custom-tabs .panel .panel-body h3 {
            display: none;
        }
        .tab-content {
            background: #f3f3f3;
            padding: 15px 15px !important;
        }
        body.tabs .custom-tabs h2 {
            margin:0 0 10px 0;
            font-size: 28px;
        }
    </style>



    <div id="app">

        <div class="tab-content mobile-container-padding">

            <div class="tab-pane active" id="reports">
                <div class="panel-section">
                    <div class="row text-center">
                        @foreach($reports as $report)
                            <div class="col-md-4 media-block">
                                <a href="{{$report['asset']['path']}}"
                                   onclick="window.open(this.href, '_blank'); return false;"
                                   onkeypress="if (event.keyCode==13) {window.open(this.href, '_blank'); return false;}"
                                   download="">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            @if($report['preview'])
                                                <img src="{{$report['preview']['path']}}" alt="">
                                            @else
                                                <img src="{{$report['asset']['path']}}_THUMB.png" alt="">
                                            @endif
                                        </div>
                                        <div class="panel-body" style="height: 175px;">
                                            <h3>{{$report['description']}}</h3>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="col-md-6">
                                                <p>download</p>
                                            </div>
                                            <div class="col-md-6">
                                            <span>
                                                <img src="{{asset('/frontend/img/GBF2017/panel_download.png')}}" alt="">
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
