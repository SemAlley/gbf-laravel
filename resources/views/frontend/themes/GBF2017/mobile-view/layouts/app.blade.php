<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>@yield('meta_title')</title>

    <meta name="description" content="@yield('meta_description')">

    @include('frontend.themes.GBF2017.mobile-view.layouts.partials.head')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.js"></script>

</head>
<body class="{{$page_settings['body_class'] or ''}}">
<script>
    if ('addEventListener' in document) {
        document.addEventListener('DOMContentLoaded', function() {
            FastClick.attach(document.body);
        }, false);
    }
</script>
<style>
    .hero-image,
    header,
    footer,
    .copyright,
    .burgundy,
    .burgundy-home,
    .page-heading,
    .nav-tabs,
    .dashboard-left h2,
    .request-action,
    .top-right-navigation
    {
        display: none !important;
    }
    #myplanner .request-action {
        display: inline-block !important;
    }
    .mobile-container-padding,
    body.form form{
        padding: 0 !important;
    }
    .dashboard-left h3 {
        margin-top:30px !important;
    }
    html body .main-copy {
        height: initial;
        min-height: 100vh;
    }
    body.tabs .tabs-copy.dashboard #myplanner .my-planner .panel-body .meeting-request-accordion .request-action {
        width:100%;
    }
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .appointment-popup {
        margin-right:25px;
    }
    body.form form.gbf-form .form-group {
         width:100% !important;
    }
    .speaker-profile-info a {
        display:none !important;
    }
    .individual-speaker .speaker-profile-info {
        padding-left: 15px;
    }
    .individual-speaker .speaker-profile-info p {
        font-size:16px;
    }
</style>

<div>
    @yield('content')
</div>

@include('frontend.themes.GBF2017.mobile-view.layouts.partials.footer')

@if(env('APP_ENV') !== 'local')
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-38878560-12', 'auto');
        ga('send', 'pageview');

    </script>
@endif

</body>
</html>
