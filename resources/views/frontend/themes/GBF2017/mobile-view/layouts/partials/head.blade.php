<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" rel="stylesheet">
<link href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css" rel="stylesheet">

<link href="/frontend/css/base.css" rel="stylesheet">

<script
        src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>

<script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<link rel="shortcut icon" href="/storage/cms/{{General::conferenceSlug('frontend.index')}}/images/favicon.ico?{{rand(1,10)}}" type="image/x-icon">
<link rel="icon" href="/storage/cms/{{General::conferenceSlug('frontend.index')}}/images/favicon.ico?{{rand(1,10)}}" type="image/x-icon">


<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<style>

    body.home .countdown-section .timer-section #countdown .cd-box .numbers {
        color: {{General::adjustBrightness($global['primary_color_1'], 10)}} !important;
    }

    body.home .gbf-africa-section .chart-circle .chart .percent {
        color: {{General::adjustBrightness($global['primary_color_1'], 90)}} !important;
    }

    body.home .gbf-africa-section .chart-circle .chart
    {
        background-color: {{General::adjustBrightness($global['primary_color_1'], -30)}} !important;
    }

    {{--PRIMARY 1 TEXT--}}
    html body .other-intro h3,
    html body .home-intro h3,
    html body a.btn.btn-default:hover, html body a.btn.btn-default:focus,
    body.home .programme-knowledgeHub-section a .panel.panel-default .panel-body + p span,
    body.tabs .tabs-copy ul.nav.nav-tabs li.active a,
    #speakers-tab .speaker-profile a.speaker-profile-data .profile-block h3,
    #delegates-tab .speaker-profile a.speaker-profile-data .profile-block h3,
    .individual-speaker .speaker-profile-info h2,
    body.tabs .media-block a .panel.panel-default .panel-footer p,
    html body .other-intro a.btn.btn-default,
    html body .home-intro a.btn.btn-default,
    .mobile-first .menu .mainMenu.navbar.navbar-default ul li a:hover,
    .mobile-first .menu .mainMenu.navbar.navbar-default ul li a:focus,
    .page-heading p sup,
    body.form form .form-group button[type="submit"]:hover,
    body.form form .form-group button[type="submit"]:focus,
    body.form form .form-group .submit:hover,
    body.form form .form-group .submit:focus,
    body.form form .form-group label sup,
    .url-link,
    .url-link:visited,
    .url-link:hover,
    body.tabs .custom-tabs .panel .panel-body p span,
    body.tabs .accordion .panel-group .panel.panel-default .accordion-body .panel-body a,
    span.bold,
    .mobile-first .menu .mainMenu.navbar.navbar-default ul li.dropdown.open ul.dropdown-menu li a:hover,
    .page-heading p a,
    body.tabs .accordion .panel-group .panel.panel-default .panel-heading.unclickable-link,
    .page-heading h3.programme,
    body.form form .form-group .fileinput.input-group span.btn.btn-default:hover,
    body.form form .form-group .fileinput.input-group span.btn.btn-default:focus,
    html body #feedback-modal .modal-dialog .modal-body a,
    html body #error-modal-register .modal-dialog .modal-body a,
    html body #error-modal .modal-dialog .modal-body a,
    html body #film-photography-modal .modal-dialog .modal-body a,

    html body #feedback-modal .modal-dialog .modal-body a.btn:hover,
    html body #error-modal-register .modal-dialog .modal-body a.btn:hover,
    html body #error-modal .modal-dialog .modal-body a.btn:hover,
    html body #film-photography-modal .modal-dialog .modal-body a.btn:hover,
    html body a.btn.btn-default:hover,
    body.tabs .custom-tabs.hotel-finder .panel.panel-default .content-block .expanded-tab a.check-availability
    {
        color: {{$global['primary_color_1']}};
    }

    .intro-left .btn:hover {
        color: white !important;
    }

    /*IMPORTANT FIXES FOR ABOVE*/
    .mobile-first .menu.active .mainMenu ul li a:hover,
    .mobile-first .menu .mainMenu.navbar.navbar-default ul li.dropdown.open > a,
    .mobile-first .menu .mainMenu.navbar.navbar-default ul li.dropdown.open ul.dropdown-menu li a:hover,
    html body a.btn.btn-default:focus,
    html body #feedback-modal .modal-dialog .modal-body a.btn:hover,
    html body #error-modal-register .modal-dialog .modal-body a.btn:hover,
    html body #error-modal .modal-dialog .modal-body a.btn:hover,
    html body #film-photography-modal .modal-dialog .modal-body a.btn:hover,
    html body h3.speakers,
    .slider-section button.slick-next::before,
    .slider-section button.slick-prev::before,
    body.form form .form-group a.form-link,
    h2,
    html body.tabs.form div div#app div.tabs-copy.custom-tabs.accordion.dashboard div#dashboard-tab div.tab-content.container-padding.extra-top div#dashboard.tab-pane.active div div.dashboard-left div.col-md-4 h2,
    body.tabs .tabs-copy.dashboard .dashboard-left .buttons .update-photo:hover,
    body.tabs .dashboard-load .panel-group .panel.panel-default .no-requests.accordion-body .panel-body .request-action a:hover,
    body.tabs .tabs-copy.dashboard #myplanner .filter-action-btns .filtering a.active,
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .accordion.programme .panel-group .panel.panel-default .panel-heading.unclickable-link .panel-title a p.title,
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .no-requests-block .meeting-request-accordion .request-action .btn.btn-default:hover, body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .no-requests-block .meeting-request-accordion .request-action .btn.btn-default:focus,
    .mobile-first .menu .mainMenu.navbar.navbar-default ul li.dropdown.open > a,
    .navbar-default ul li.dropdown.open > a,
    .navbar-default ul li.dropdown.open > a:hover,
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .accordion.programme .panel-group .panel.panel-default .panel-heading.unclickable-link .panel-title a,
    #speakers-tab .filtering-container .filtering .bootstrap-select button[type="button"]:not(.bs-placeholder), #delegates-tab .filtering-container .filtering .bootstrap-select button[type="button"]:not(.bs-placeholder),
    body.tabs .tabs-copy.dashboard .modal.request-meeting .modal-dialog .modal-body h3,
    body.tabs .tabs-copy.dashboard a.btn-default.active,
    body.tabs .tabs-copy.dashboard .modal#update-photo .modal-dialog .modal-body .fileinput.input-group .btn-file span

    {
        color: {{$global['primary_color_1']}} !important;
    }

    /*Primary 1 border*/
    html body a.btn.btn-default,
    body.form form .form-group button[type="submit"],
    body.form form .form-group .submit,
    body.form form .form-group button[type="submit"],
    body.form form .form-group .submit,
    body.home .countdown-section .timer-section .heading-block,
    body.tabs .accordion .panel-group .panel.panel-default .panel-heading.unclickable-link,
    body.form form .form-group .fileinput.input-group span.btn.btn-default:hover,
    body.form form .form-group .fileinput.input-group span.btn.btn-default:focus,
    html body #feedback-modal .modal-dialog .modal-body a.btn:hover,
    html body #error-modal-register .modal-dialog .modal-body a.btn:hover,
    html body #error-modal .modal-dialog .modal-body a.btn:hover,
    html body #film-photography-modal .modal-dialog .modal-body a.btn:hover,
    html body #feedback-modal .modal-dialog .modal-body a.btn,
    html body #error-modal-register .modal-dialog .modal-body a.btn,
    html body #error-modal .modal-dialog .modal-body a.btn,
    html body #film-photography-modal .modal-dialog .modal-body a.btn,
    body.tabs .custom-tabs.hotel-finder .panel.panel-default .content-block .expanded-tab a.check-availability,
    body.tabs .custom-tabs.hotel-finder .panel.panel-default .content-block .expanded-tab a.check-availability:hover,
    #speakers-tab .filtering-container .filtering .bootstrap-select button[type="button"]:not(.bs-placeholder), #delegates-tab .filtering-container .filtering .bootstrap-select button[type="button"]:not(.bs-placeholder),
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .selected-appointment-block .meeting-request-accordion,
    body.tabs .tabs-copy.dashboard a.btn-default.active,
    body.tabs .tabs-copy.dashboard .modal#update-photo .modal-dialog .modal-body .fileinput.input-group .btn-file
    {
        border-color: {{$global['primary_color_1']}}
    }

    {{--PRIMARY 1 BACKGROUND--}}
    body.home .programme-section .programme-block,
    html body a.btn.btn-default,
    body.home .programme-knowledgeHub-section a .panel.panel-default .panel-footer span,
    #speakers-tab .speaker-profile a.speaker-profile-data:hover .profile-block,
    #delegates-tab .speaker-profile a.speaker-profile-data:hover .profile-block,
    #delegates-tab .speaker-profile a.speaker-profile-data:hover,
    .individual-speaker .speaker-profile-info a i,
    body.tabs .media-block a .panel.panel-default .panel-footer span,
    body.tabs .media-block .panel.panel-default .panel-footer span,
    html body .other-intro a.btn.btn-default:hover,
    html body .home-intro a.btn.btn-default:hover,
    body.form form .form-group button[type="submit"],
    body.form form .form-group .submit,
    body.tabs .accordion .panel-group .panel.panel-default .panel-heading,
    .mobile-first .menu .navbar-section .navbar-header .menuBtn,
    .mobile-first .menu .navbar-section .navbar-header .menuBtn.active:not(.act),
    body.home .gbf-africa-section,
    body.form form .form-group .fileinput.input-group span.btn.btn-default,
    html body #feedback-modal .modal-dialog .modal-body a.btn,
    html body #error-modal-register .modal-dialog .modal-body a.btn,
    html body #error-modal .modal-dialog .modal-body a.btn,
    html body #film-photography-modal .modal-dialog .modal-body a.btn,
    body.tabs .custom-tabs.hotel-finder .panel.panel-default .content-block .expanded-tab a.check-availability:hover,
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .accordion.programme .panel-group .panel.panel-default .panel-heading,
    #speakers-tab .speaker-profile a.speaker-profile-data .request-meeting, #delegates-tab .speaker-profile a.speaker-profile-data .request-meeting,
    body.tabs .tabs-copy.dashboard .modal .modal-content .modal-header,
    body.tabs .tabs-copy.dashboard .modal#myavailability .modal-dialog .modal-body .day-block h4,
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .selected-appointment-block .meeting-request-accordion
    {
        background-color: {{$global['primary_color_1']}};
    }

    {{--PRIMARY 1 MOBILE ONLY--}}
    @media only screen and (max-width : 768px) {
        .mobile-first .menu .mainMenu.navbar.navbar-default
        {
            background-color: {{$global['primary_color_1']}};
        }
        .mobile-first .menu.active .mainMenu ul li a:hover,
        .mainMenu ul li a:hover
        {
            color: white !important;
        }
        .mainMenu ul li a {
            padding: 10px 0 !important;
        }
    }

    .url-link-sponsor,
    html body #feedback-modal .modal-dialog .modal-header h4,
    html body #error-modal-register .modal-dialog .modal-header h4,
    html body #error-modal .modal-dialog .modal-header h4,
    html body #film-photography-modal .modal-dialog .modal-header h4
    {
        color: {{$global['primary_color_2']}};
    }

    {{--PRIMARY 2 BACKGROUND--}}
    body.home .burgundy-home,
    .copyright,
    .mobile-first .menu.active .burgundy,
    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-heading
    {
        background-color: {{$global['primary_color_2']}};
    }

    body.tabs .tabs-copy.dashboard #myplanner .panel-group .panel.panel-default .panel-collapse.my-planner-accordion .panel-body .accordion.programme .panel-group .panel-collapse .panel-body {
        padding: 40px 10px !important;
    }

</style>
