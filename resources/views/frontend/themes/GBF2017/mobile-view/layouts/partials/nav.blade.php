<div class="mobile-first">
    <div class="mainContainer">
        <header class="menu">
            <div class="navbar-section">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{General::conferenceRoute('frontend.index')}}">
                        <img src="{{App\Content::AssetPath($global['event_logo'])}}" alt="" class="img-responsive">
                    </a>
                    <a href="#" class="menuBtn">
                        <span class="lines"></span>
                    </a>
                </div>
            </div>
            <nav class="mainMenu navbar navbar-default">
                <ul class="nav navbar-nav nav-sub pull-right">
                    <hr class="visible-xs"/>
                    <li><a href="{{General::conferenceRoute('frontend.contact')}}">contact</a></li>
                    @if(Auth::guard( 'delegate' )->check())
                        <li>
                            <a href="{{General::conferenceRoute('frontend.dashboard')}}">
                                MY DASHBOARD
                            </a>
                        </li>
                    @else
                        @if($registration_enabled)
                            <li><a href="{{General::conferenceRoute('frontend.register')}}">register</a></li>
                        @endif
                        @if($login_enabled)
                            <li><a href="{{General::conferenceRoute('frontend.login')}}">login</a></li>
                        @endif
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{General::conferenceRoute('frontend.index')}}" class="visible-xs current">home</a>
                    </li>
                    @foreach($pages as $page)
                        @if($page['child'])
                            <li class="dropdown">
                                <a href="{{$page['url']}}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">{{$page['title']}}</a>
                                <ul class="dropdown-menu">
                                    @foreach($page['child'] as $subPage)
                                        <li><a href="{{$subPage['url']}}">{{$subPage['title']}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        @elseif (count($page['anchor_tags']))
                            <li class="dropdown">
                                <a href="{{$page['url']}}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">{{$page['title']}}</a>
                                <ul class="dropdown-menu">
                                    @foreach($page['anchor_tags'] as $tag)
                                        <li><a href="{{$page['url'].$tag['linkSuffix']}}">{{$tag['linkName']}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        @elseif ($page['has_custom_label'])
                            <li><a href="{{$page['url']}}">{{$page['title']}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </nav>

            <div class="burgundy burgundy-hidden">
                <div class="empty-space"></div>
                <div class="text-center">
                    {!! $global['header_banner'] !!}
                </div>
            </div>

        </header>
    </div>
<!-- 
    @if($page_settings['header_register_button'])
        <div class="container-fluid other-intro">
            <div class="row">
                <div class="intro-left hidden-xs">
                    <a href="{{General::conferenceRoute('frontend.register')}}" class="btn btn-default">register now</a>
                </div>
                <div class="intro-right">
                    <h3>{{$global['forum_date']}} <span>{{$global['forum_venue']}}</span></h3>
                </div>
                <div class="intro-left visible-xs">
                    <a href="{{General::conferenceRoute('frontend.register')}}" class="btn btn-default">register now</a>
                </div>
            </div>
        </div>
    @else -->
        <div class="container-fluid home-intro">
            <div class="row">
                <div class="intro-left intro-home">
                    <h3>{{$global['forum_date']}} <span>{{$global['forum_venue']}}</span></h3>
                </div>
                <div class="intro-right intro-home">
                    <h1>{!! $global['forum_title'] !!}</h1>
                </div>
            </div>
        </div>
    <!-- @endif -->

</div>
