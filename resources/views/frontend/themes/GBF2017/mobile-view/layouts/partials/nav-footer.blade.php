<ul class="nav navbar-nav">

    <li>

        <a href="{{General::conferenceRoute('frontend.index')}}" class="visible-xs current">home</a>

        @foreach($pages as $page)
            @if ($page['has_custom_label'])
                <a href="{{$page['url']}}">{{$page['title']}}</a>
            @endif
        @endforeach

        <a href="{{General::conferenceRoute('frontend.contact')}}">contact</a>

        @if(!Auth::guard( 'delegate' )->check())

            @if($registration_enabled)
                <a href="{{General::conferenceRoute('frontend.register')}}">register</a>
            @endif

            {{--<a href="{{General::conferenceRoute('frontend.login')}}">login</a>--}} {{-- TODO: this link commented out temp --}}
        @endif

    </li>

</ul>


