@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs form',
        'header_image_override' => $content['header_image_override'] ?? $global['header_image'],
        'header_register_button' => false
    ];
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title')Dashboard | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($content['meta_description']) ? $content['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">dashboard</a></li>
            </ol>
            <h1>{{$content['page_heading'] or 'Dashboard'}}</h1>
            @if(isset($content['page_subheading']) && $content['page_subheading'] != "")
                <hr/>
                {!! $content['page_subheading'] !!}
            @endif
        </div>

        <div class="tabs-copy custom-tabs accordion dashboard">
            <div id="dashboard-tab">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#dashboard" class="tab-link" data-toggle="tab">{{strtoupper('profile')}}</a>
                    </li>
                    <li>
                        <a href="#myplanner" class="tab-link" data-toggle="tab">{{strtoupper('my planner')}}</a>
                    </li>
                    <li>
                        <a href="#delegates" class="tab-link" data-toggle="tab">{{strtoupper('delegates')}}</a>
                    </li>
                    <li>
                        <a href="" class="tab-link logout-popup">{{strtoupper('logout')}}</a>
                    </li>
                </ul>
                <div class="tab-content container-padding extra-top">
                    <div class="tab-pane active" id="dashboard">

                        <profile-dashboard :photo="'{{$profile_photo}}'"
                                           :delegate="{{json_encode($delegate)}}"
                                           :countries="{{$countries}}"
                        >
                            <div slot="mobile">
                                {!! isset($content['profile_content']) ? $content['profile_content'] : '' !!}
                            </div>
                            <div slot="body">
                                {!! isset($content['profile_content']) ? $content['profile_content'] : '' !!}
                            </div>
                        </profile-dashboard>

                    </div>
                    <div class="tab-pane" id="myplanner">
                        <profile-planner
                                :delegate="{{json_encode($delegate)}}"
                                :speaker-path="'{{General::conferenceRoute('frontend.speakers')}}'"
                        >
                            <div slot="body">
                                {!! $content['planner_content'] ?? '' !!}
                            </div>
                        </profile-planner>
                    </div>
                    <div class="tab-pane" id="delegates">
                        <profile-delegates
                                :countries="{{$countries}}"></profile-delegates>
                    </div>
                    <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logoutLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="logoutLabel">Logout</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="text-center">Are you sure you want to log out?</p>
                                    <div class="request-action text-center">
                                        <a class="btn btn-default" data-dismiss="modal" aria-label="Close">cancel</a>
                                        <a href="{{General::conferenceRoute('frontend.logout')}}" class="btn btn-default">ok</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
