<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <title>@yield('meta_title')</title>

    <meta name="description" content="@yield('meta_description')">

    @include('frontend.themes.GBF2017.layouts.partials.head')

</head>
<body class="{{$page_settings['body_class'] or ''}}">

@include('frontend.themes.GBF2017.layouts.partials.header')

@if(\Request::hasHeader('X-App-Type') && \Request::header('X-App-Type') == 'gbf-mobile-react-native')
    @include('frontend.themes.GBF2017.mobile-config');
@endif

<div>
    @yield('content')
</div>

@include('frontend.themes.GBF2017.layouts.partials.footer')

@if(env('APP_ENV') !== 'local')
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-38878560-12', 'auto');
        ga('send', 'pageview');

    </script>
@endif

</body>
</html>
