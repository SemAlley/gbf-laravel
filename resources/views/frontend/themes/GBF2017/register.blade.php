@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'form',
        'header_image_override' => $register['header_image_override'],
        'header_register_button' => true
    ];
$passwordEnable = isset($login_enabled) ? $login_enabled : 1;
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title'){{$register['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($register['meta_description']) ? $register['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="page-heading container-padding">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">registration</a></li>
            </ol>
            <h1>{{$register['page_heading']}}</h1>
            @if(isset($register['page_subheading']) && $register['page_subheading'] != "")
                <hr/>
                {!! $register['page_subheading'] !!}
            @endif
        </div>
        <register-form
                :register-url="'{{General::conferenceRoute('frontend.register.post')}}'"
                :uic-check-url="'{{General::conferenceRoute('frontend.register.uic-check')}}'"
                :home-url="'{{General::conferenceRoute('frontend.index')}}'"
                :extra-fields="{{json_encode($extra_fields)}}"
                :countries="{{$countries}}"
                :password-enable="{{$passwordEnable}}"
        >
                <div slot="popup_register_success">{!! $register['popup_register_success'] or '' !!}</div>
                <div slot="popup_uic_error">{!! $register['popup_uic_error'] or '' !!}</div>
        </register-form>

    </div>

@endsection
