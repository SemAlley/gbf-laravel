@php
    $page_settings = [
        'display_banner' => false,
        'body_class' => 'tabs',
        'header_image_override' => isset($page_data['header_image_override'])?$page_data['header_image_override']:false,
        'header_register_button' => true
    ];
@endphp

@extends('frontend.themes.GBF2017.layouts.app')

@section('meta_title'){{$page_data['meta_title'] or ''}} | {{$global['meta_title'] or ''}}@endsection

@section('meta_description'){{isset($page_data['meta_description']) ? $page_data['meta_description'] : isset($global['meta_description']) ? $global['meta_description'] : ''}}@endsection

@section('content')

    <div id="app">

        <div class="@if($content['type'] === 'tabs_page')main-copy white-force @endif page-heading container-padding">
            <ol class="breadcrumb">
                <li><a href="{{General::conferenceRoute('frontend.index')}}">home</a></li>
                <li class="active"><a class="active-breadcrumb" href="#">{{$content['title']}}</a></li>
            </ol>
            @isset($content['title'])
                <h1>{{$content['title']}}</h1>
            @endisset
            @isset($page_data['page_subheading'])
                <hr/>
                <p class="more">{!! $page_data['page_subheading']  !!}</p>
            @endisset
        </div>


        <div class="tabs-copy">

            @if($content['type'] === 'content_page')
                <div class="main-copy custom-tabs container-padding extra-top">
                    <cms-page-tab :data="{{json_encode(array_shift($content['tabs']))}}"></cms-page-tab>
                </div>
            @elseif($content['type'] === 'tabs_page')
                <ul role="tablist" class="nav nav-tabs container tabs-page">
                    @foreach(General::orderDataBy($content['tabs'], 'index') as $index => $tab)

                        @if(isset($tab['quick_link_label']) && $tab['quick_link_label'])
                            <li role="presentation" class="{{$index == 0 ? 'active' : ''}}">
                                <a
                                        href="{{ strtolower('#'.str_slug($tab['quick_link_label'], '-')) }}"
                                        data-toggle="tab"
                                        class="tab-link"
                                >
                                    {{$tab['title']}}
                                </a>
                            </li>
                        @else
                            <li role="presentation" class="{{$index == 0 ? 'active' : ''}}">
                                <a
                                        href="#{{$index}}_{{str_slug($tab['title'], '_')}}"
                                        data-toggle="tab"
                                        class="tab-link"
                                >
                                    {{$tab['title']}}
                                </a>
                            </li>
                        @endif


                    @endforeach
                </ul>

                <div class="tab-content container-padding extra-top">
                    @foreach(General::orderDataBy($content['tabs'], 'index') as $index => $tab)

                        @if(isset($tab['quick_link_label']) && $tab['quick_link_label'])
                            <div id="{{ strtolower(str_slug($tab['quick_link_label'], '-')) }}"
                                 class="tab-pane {{$index == 0 ? 'active' : ''}}">
                                <div class="custom-tabs">
                                    <cms-page-tab :data="{{json_encode($tab)}}"></cms-page-tab>
                                </div>
                            </div>
                        @else
                            <div role="tabpanel" id="{{$index}}_{{str_slug($tab['title'], '_')}}"
                                 class="tab-pane {{$index == 0 ? 'active' : ''}}">
                                <div class="custom-tabs">
                                    <cms-page-tab :data="{{json_encode($tab)}}"></cms-page-tab>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif

        </div>

    </div>
@endsection
