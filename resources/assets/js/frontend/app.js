/* global _ Vue */

window._ = require('lodash')

import 'babel-polyfill'

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    // window.$ = window.jQuery = require('jquery');

    require('bootstrap-less');
} catch (e) {
    console.warn(e);
}

require('admin-lte')
window.toastr = require('toastr')
require('icheck')
require('select2/dist/js/select2.full.js')
require('vuejs-datepicker');
require('vue2-timepicker');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios')

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue')

// Use trans function in Vue (equivalent to trans() Laravel Translations helper). See htmlheader.balde.php partial.
Vue.prototype.trans = (key) => {
    return _.get(window.trans, key, key)
}


import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBvWE_sIwKbWkiuJQOf8gSk9qzpO96fhfY',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
})

// VUE!
Vue.component('login-form', require('../components/frontend/auth/LoginForm.vue'))
Vue.component('register-form', require('../components/frontend/auth/RegisterForm.vue'))
Vue.component('profile-form', require('../components/frontend/auth/ProfileForm.vue'))
Vue.component('contact-form', require('../components/frontend/contact/ContactForm.vue'))
Vue.component('media-register-form', require('../components/frontend/media/RegisterForm.vue'))
Vue.component('email-reset-password-form', require('../components/frontend/auth/EmailResetPasswordForm.vue'))
Vue.component('reset-password-form', require('../components/frontend/auth/ResetPasswordForm.vue'))

Vue.component('cms-page-tab', require('../components/frontend/cms/PageTab.vue'))

Vue.component('hotel-finder', require('../components/frontend/hotel/HotelFinder.vue'))

// Profile elements

Vue.component('profile-dashboard', require('../components/frontend/profile/Dashboard.vue'))
Vue.component('new-profile-photo', require('../components/frontend/profile/NewProfilePhoto.vue'))
Vue.component('profile-planner', require('../components/frontend/profile/Planner.vue'))
Vue.component('profile-delegates', require('../components/frontend/profile/Delegates.vue'))
Vue.component('logout-tab', require('../components/frontend/profile/Logout.vue'))

// Register all page builder elements

Vue.component('section-left-image-right-html-block', require('../components/frontend/cms/sections/BlockLIRH.vue'))
Vue.component('section-left-html-right-image-block', require('../components/frontend/cms/sections/BlockLHRI.vue'))
Vue.component('section-sponsor-block', require('../components/frontend/cms/sections/BlockSponsor.vue'))
Vue.component('section-map-block', require('../components/frontend/cms/sections/BlockMap.vue'))
Vue.component('section-full-html-block', require('../components/frontend/cms/sections/BlockFullHtml.vue'))
Vue.component('section-faq-heading-block', require('../components/frontend/cms/sections/BlockFAQ.vue'))
Vue.component('section-faq-block', require('../components/frontend/cms/sections/BlockFAQ.vue'))
Vue.component('section-link-block', require('../components/frontend/cms/sections/BlockLink.vue'))
Vue.component('section-download-block', require('../components/frontend/cms/sections/BlockDownload.vue'))

const app = new Vue({
    el: '#app'
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

require('axios')