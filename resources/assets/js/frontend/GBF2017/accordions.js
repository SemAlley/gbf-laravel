// FAQ Expand all accordions button
$(function () {

    var $body = $('body');
    var active = true;

    $body.on('click', '.expandAll', function () {
        if (active) {
            active = false;
            $('.panel-collapse').collapse('show');
            $('.panel-collapse').css('height', '');
            $('.panel-collapse').addClass('in');
            $('.panel-title').attr('data-toggle', '');
            $('.panel-title span').removeClass('glyphicon-menu-down');
            $('.panel-title span').addClass('glyphicon-menu-up');
            $(this).text('Collapse All');
        } else {
            active = true;
            $('.panel-collapse').collapse('hide');
            $('.panel-title').attr('data-toggle', 'collapse');
            $(this).text('Expand All');
        }
    });

    // Read more/Read less button
    $body.on('click', '.read-btn', function () {
        var more = 'Read more <span class="glyphicon glyphicon-menu-down">';
        var less = 'Read Less <span class="glyphicon glyphicon-menu-up">';

        if ($(this).hasClass("more")) {
            $(this).html(less)
            $(this).removeClass("more")
            $(this).addClass('less')
        } else if ($(this).hasClass("less")) {
            $(this).html(more)
            $(this).removeClass("less")
            $(this).addClass('more')
        }
    });

    $body.on('click', '.bootstrap-select', function () {
        $(this).toggleClass('open');
    });

    //Breadcrumbs active class on the tab currently active
    var activeBreadcrumb = $('.active-breadcrumb');

    $('.nav.nav-tabs').on('click', '.tab-link', function () {
        var self = $(this);
        activeBreadcrumb.attr('href', self.data('url'));
        activeBreadcrumb.text(self.text());
    });


    $body.on('click', '#myplanner [data-toggle="collapse"]', function (e) {
        var parent = $(this).data('parent');
        var currentCollapse = $(this).attr('href');
        var related = $('[data-parent="' + parent + '"]');
        related.each(function (index, el) {
            var targetId = $(el).attr('href');
            if(targetId !== currentCollapse) {
                $(targetId).collapse('hide');
            }
        });
    })
});

// Locate to different tabs on one page
$(document).ready(function () {
    $('.nav-tabs a').on('shown.bs.tab', function (e) {

        let hash = e.target.hash.replace("#", "#").replace(/^#/, '');
        let node = $('#' + hash);
        if (node.length) {
            node.attr('id', '');
        }
        document.location.hash = hash;
        if (node.length) {
            node.attr('id', hash);
        }
    });

    $('ul.dropdown-menu li a').on('click', function (e) {

        if (!$('.nav.nav-tabs.container.tabs-page').length) {
            setTimeout(function () {

                let targetId = document.location.hash.replace('#', '');

                let heightOffset = $('.mainContainer header').height();

                $('html, body').animate({
                    scrollTop: $("#" + targetId).offset().top - heightOffset
                }, 0);
            }, 10)
        }

    });

    $('.logout-popup').on('click', function (e) {
        e.preventDefault();
        $("#logout").modal()
    });

    var hash = location.hash;
    if(hash) {
        hash = hash.replace('##', '#');
        $('a[href="'+hash+'"]').trigger('click');
    }
});

// $(window).on('hashchange',function() {
//     var hash = document.location.hash;
//     if (hash) {
//         $('.nav-tabs a[href="'+hash+'"]').tab('show');
//     }
// });

// Collapse bootstrap panels when clicking on anchor tag
// $(document).ready(function () {
//    location.hash && $(location.hash + '.collapse').collapse('show');
// });
