equalheight = function (container) {

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).matchHeight();
    return;
    $(container).each(function () {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(window).load(function () {
    equalheight('.media-block .panel .panel-body');
    // equalheight('.media-block .panel .panel-heading');
    equalheight('.programme-knowledgeHub-section .panel .panel-body');
    equalheight('.home-intro .intro-home');
    equalheight('.confirmed-speaker .africa-live-block');
});


window.resizeBlocks = function () {
    equalheight('.media-block .panel .panel-body');
    // equalheight('.media-block .panel .panel-heading');
    equalheight('.programme-knowledgeHub-section .panel .panel-body');
    equalheight('.home-intro .intro-home');
    equalheight('.confirmed-speaker .africa-live-block');
};

var tmResizing = null;

$(window).resize(function () {

    clearTimeout(tmResizing);

    tmResizing = setTimeout(function () {

        equalheight('.media-block .panel .panel-body');
        // equalheight('.media-block .panel .panel-heading');
        equalheight('.programme-knowledgeHub-section .panel .panel-body');
        equalheight('.home-intro .intro-home');
        equalheight('.confirmed-speaker .africa-live-block');

    }, 300)

});
