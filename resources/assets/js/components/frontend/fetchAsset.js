export default {
    methods: {
        fetchAsset (id) {

            var confPath = document.location.pathname.split("/")[1];

            return axios.get('/'+confPath+'/api/get-asset/' + id)
                    .then(response => {
                        if (response.data.success) {
                            return response.data.asset;
                        } else {
                            return false
                        }
                    })
                    .catch(error => {
                        console.log('error at fetchAsset',error )
                    });

        }
    }
}
