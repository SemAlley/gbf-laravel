export default {
    methods: {
        transform (data) {
            let formated = {};
            data.forEach(item => {
                formated[item.name] = item.content;
            });
            return formated;
        }
    }
}
