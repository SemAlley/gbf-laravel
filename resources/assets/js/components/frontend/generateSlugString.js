export default {
    methods: {
        generateSlug (str, delimer) {
            delimer = delimer || '_';
            return str.toLowerCase().replace(/\s+/g, delimer)           // Replace spaces with delimer
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/^-+/, '')             // Trim - from start of text
                .replace(/-+$/, '');            // Trim - from end of text
        }
    }
}