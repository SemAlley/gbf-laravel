export default {
    methods: {
        moveBlockDownComponent: function()
        {
            this.blockIndex = this.getIndBlock();

            if(this.blockIndex === $('.tab-pane.active .blocks-in-loop').length-1)
                return;

            this.moveBlockDown(this.blockIndex);

            if(this.hasOwnProperty('isImageSelect2WithDropzoneActive'))
            {
                this.reloadImageSelect2WithDropzoneActive();
            }

            this.emitUpdateBlockIndexes();
        },
        moveBlockUpComponent: function ()
        {
            this.blockIndex = this.getIndBlock();

            if(this.blockIndex === 0)
                return;

            this.moveBlockUp(this.blockIndex);

            if(this.hasOwnProperty('isImageSelect2WithDropzoneActive'))
            {
                this.reloadImageSelect2WithDropzoneActive();
            }

            this.emitUpdateBlockIndexes();
        },

        moveBlockUp: function(index)
        {
            let movedBlock = $('.tab-pane.active .block-in-loop-id-'+index);
            let prevBlock = $('.tab-pane.active .block-in-loop-id-'+(index-1));

            $(movedBlock).remove();
            $(prevBlock).before($(movedBlock));

            $(movedBlock).removeClass('block-in-loop-id-'+index);
            $(movedBlock).addClass('block-in-loop-id-'+(index-1));
            $(movedBlock).attr('block-position', index-1);

            $(prevBlock).removeClass('block-in-loop-id-'+(index-1));
            $(prevBlock).addClass('block-in-loop-id-'+(index));
            $(prevBlock).attr('block-position', index);
        },

        moveBlockDown: function(index)
        {
            let movedBlock = $('.tab-pane.active .block-in-loop-id-'+index);
            let nextBlock = $('.tab-pane.active .block-in-loop-id-'+(index+1));

            $(movedBlock).remove();
            $(nextBlock).after($(movedBlock));

            $(movedBlock).removeClass('block-in-loop-id-'+index);
            $(movedBlock).addClass('block-in-loop-id-'+(index+1));
            $(movedBlock).attr('block-position', index+1);

            $(nextBlock).removeClass('block-in-loop-id-'+(index+1));
            $(nextBlock).addClass('block-in-loop-id-'+(index));
            $(nextBlock).attr('block-position', index);
        },

        getIndBlock: function()
        {
            return +$(this.$el).parent().attr('block-position');
        },

        emitUpdateBlockIndexes: function()
        {
            this.$emit('updateBlockIndexes', true);
        },

        reloadImageSelect2WithDropzoneActive: function()
        {
            this.isImageSelect2WithDropzoneActive = false;
            setTimeout(()=>{
                this.isImageSelect2WithDropzoneActive = true;
            },0)
        },
    }
}