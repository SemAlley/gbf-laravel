export default {
    methods: {
        validateImagePresence: function(page_data, pageType)
        {
            let blocksWithImages = ['left-image-right-html-block', 'left-html-right-image-block', 'sponsor-block', 'link-block', 'download-block'];
            let errorImages = [];
            let blocksData;

            if(pageType == 'content_page')
            {
                blocksData = page_data;
                for(let block in blocksData) {
                    if (blocksWithImages.indexOf(blocksData[block].block_type) !== -1)
                    {
                        if(!blocksData[block].block_data.asset_image_id)
                        {
                            let errImg = {
                                blockType: blocksData[block].block_type,
                                blockTitle: blocksData[block].block_data.title
                            };

                            errorImages.push(errImg);
                        }
                    }
                }
            }
            else if(pageType == 'tabs_page')
            {
                for(let tab in page_data)
                {
                    for(let block in page_data[tab].data)
                    {
                        if (blocksWithImages.indexOf(page_data[tab].data[block].block_type) !== -1)
                        {
                            if(!page_data[tab].data[block].block_data.asset_image_id)
                            {
                                let errImg = {
                                    blockType: page_data[tab].data[block].block_type,
                                    blockTitle: page_data[tab].data[block].block_data.title,
                                    tabTitle: page_data[tab].title
                                };

                                errorImages.push(errImg);
                            }
                        }
                    }
                }
            }

            return errorImages;
        },

        notifyImageError: function(errors)
        {
            let text = '';

            errors.forEach(err => {

                text += '<p>The image is required ';

                if(err.tabTitle)
                    text += ' in the tab "' + err.tabTitle +'"';

                text += ' at block ' + err.blockType;

                if(err.blockTitle && err.blockTitle.length > 0)
                    text += ' with title "' + err.blockTitle +'"';

                text += '</p>';

            });

            this.imageErrorText = text;
            $("html, body").animate({ scrollTop: 0 }, "slow");
        },
    }
}