<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{
    protected $table = 'meeting_corporates';
    protected $fillable = ['country', 'company_name', 'industry', 'bio', 'delegate_id', 'title', 'first_name', 'last_name', 'email'];

    public function day()
    {
        return $this->belongsTo(Day::class);
    }

    public function delegate()
    {
        return $this->belongsTo(Delegate::class);
    }

    public function meetingTimeslots()
    {
        return $this->belongsToMany(MeetingTimeslot::class, 'meetingtimeslots_corporates');
    }
}