<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
class Speaker extends Model {

    public function conference() {
        return $this->belongsTo('App\Conference');
    }

	public function photo() {
		return $this->belongsTo('App\ConferenceAsset', 'photo', 'id');
	}

    public function getRouteKeyName() {
        return 'id';
    }


    public function scheduleTimeslots()
    {
        return $this->belongsToMany(ScheduleTimeslot::class, 'scheduletimeslots_speakers');
    }
}
