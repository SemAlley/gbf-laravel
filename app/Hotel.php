<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

class Hotel extends Model
{
	protected $fillable = [
		'title',
		'photo_id',
		'address',
		'contact_person',
		'phone',
		'website',
		'website_title',
		'email',
		'promo_code',
		'promo_description',
		'description',
		'lat',
		'lon',
		'zoom_lvl',
	];

	/**
	 * Relation for photo
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photo()
	{
		return $this->belongsTo('App\ConferenceAsset', 'photo_id', 'id');
	}

	/**
	 * Relation for conferences
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function conferences()
	{
		return $this->belongsToMany('App\ConferenceAsset', 'hotel_conference')->withPivot('distance');
	}

	/**
	 * Create or update existing hotel from request data
	 *
	 * @param FormRequest $request
	 * @param null $hotel
	 *
	 * @return null|static
	 */
	public static function createUpdate(FormRequest $request, $hotel = null) {
		$data = $request->only( [
			'title',
			'photo_id',
			'address',
			'contact_person',
			'phone',
			'website',
			'website_title',
			'email',
			'promo_code',
			'promo_description',
			'description',
			'lat',
			'lon',
			'zoom_lvl',
		] );
		if(!$data['photo_id']) {
			$data['photo_id'] = 0;
		}

		if(!$hotel) {
			$hotel = new static();
		}

		$hotel->fill($data);
		$hotel->save();
		return $hotel;
	}
}
