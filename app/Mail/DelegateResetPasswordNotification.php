<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 8/29/17
 * Time: 6:29 PM
 */

namespace App\Mail;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Request;
use Illuminate\Auth\Notifications\ResetPassword;

class DelegateResetPasswordNotification extends ResetPassword {

	public $token;
	protected $conference;


	/**
	 * ContactNotification constructor.
	 *
	 * @param $token
	 */
	public function __construct( $token ) {
		parent::__construct( $token );
		$this->token = $token;
		$this->conference = Request::segment( 1 );
	}


	/**
	 * Get the notification's channels.
	 *
	 * @param  mixed $notifiable
	 *
	 * @return array|string
	 */
	public function via( $notifiable ) {
		return [ 'mail' ];
	}

	/**
	 * Build the mail representation of the notification.
	 *
	 * @param  mixed $notifiable
	 *
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail( $notifiable ) {
		$url = url( config( 'app.url' ) . route( 'frontend.password.reset', [
				$this->conference,
				$this->token
			], false ) );

		return ( new MailMessage )
			->subject( 'Password Reset' )
			->markdown( 'emails.auth.reset-password', [ 'url' => $url ] );
	}

}