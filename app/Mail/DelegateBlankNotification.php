<?php

namespace App\Mail;

use App\Content;
use App\Delegate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DelegateBlankNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $delegate;
    protected $notifyTarget;
    protected $header;
    protected $footer;
    protected $content;
    protected $global;
    public $subject = 'Blank Notification';

	/**
	 * NewDelegateNotification constructor.
	 *
	 * @param Delegate $delegate
	 * @param $notifyTarget
	 */
    public function __construct(Delegate $delegate, $notifyTarget = 'admin')
    {
        $this->delegate = $delegate;
        $this->notifyTarget = $notifyTarget;

        $this->getContent();
        $this->filterContent();
    }

    protected function getContent() {
    	$globalContent = Content::GetContentVue( 'email-global', $this->delegate->conference );
    	$content = Content::GetContentVue( 'email-blank', $this->delegate->conference );
	    $this->global = Content::GetContent( 'global', $this->delegate->conference );
    	$this->header = isset($globalContent['header']) ? $globalContent['header']['value'] : '';
    	$this->footer = isset($globalContent['footer']) ? $globalContent['footer']['value'] : '';
    	$this->content = isset($content['content']) ? $content['content']['value'] : '';
    	if(isset($content['subject']) && $content['subject']['value']) {
    		$this->subject = $content['subject']['value'];
	    }
    }

    protected function filterContent() {
	    $this->content = str_replace('[DELEGATE_FIRST_NAME]', $this->delegate->first_name, $this->content);
	    $this->content = str_replace('[DELEGATE_LAST_NAME]', $this->delegate->last_name, $this->content);
	    $this->content = str_replace('[DELEGATE_EMAIL]', $this->delegate->email, $this->content);
	    $this->content = str_replace('[DELEGATE_UIC]', $this->delegate->uic, $this->content);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
	        ->markdown('emails.delegate.blank')
	        ->with([
	        	'delegate' => $this->delegate,
		        'url' => route('frontend.login', $this->delegate->conference->url_slug),
		        'header' => $this->header,
		        'footer' => $this->footer,
		        'global' => $this->global,
		        'body' => $this->content,
		        'conference' => $this->delegate->conference,
	        ]);
    }
}
