<?php

namespace App\Mail;

use App\Conference;
use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactNotification extends Mailable
{

    use Queueable, SerializesModels;

    protected $conference;
    protected $contact;


    /**
     * ContactNotification constructor.
     *
     * @param Contact $contact
     * @param Conference $conference
     */
    public function __construct(Contact $contact, Conference $conference)
    {
        $this->contact = $contact;
        $this->conference = $conference;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contact')->with(
            [
                'sender' => $this->contact,
            ]
        )->subject('Contact Form Submission for ' . $this->conference->name);
    }
}
