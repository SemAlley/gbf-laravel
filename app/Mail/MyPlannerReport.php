<?php
namespace App\Mail;

use App\Content;
use App\Delegate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MyPlannerReport extends Mailable
{
	use Queueable, SerializesModels;

	protected $delegate;
	protected $days;
	protected $header;
	protected $footer;
	protected $content;
	protected $global;
	public $subject = 'My Planner';

	/**
	 * NewDelegateNotification constructor.
	 *
	 * @param $delegate
	 * @param $days
	 */
	public function __construct($delegate, $days)
	{
		$this->delegate = $delegate;
		$this->days = $days;

		$this->getContent();
		$this->reorderDays();
	}

	protected function getContent() {
		$globalContent = Content::GetContentVue( 'email-global', $this->delegate->conference );
		$content = Content::GetContentVue( 'email-planner', $this->delegate->conference );
		$this->global = Content::GetContent( 'global', $this->delegate->conference );
		$this->header = isset($globalContent['header']) ? $globalContent['header']['value'] : '';
		$this->footer = isset($globalContent['footer']) ? $globalContent['footer']['value'] : '';
		$this->content = isset($content['content']) ? $content['content']['value'] : '';
		if(isset($content['subject']) && $content['subject']['value']) {
			$this->subject = $content['subject']['value'];
		}
	}

	protected function reorderDays() {
		foreach ($this->days as $i => $day) {
			foreach ($day['meeting_timeslots'] as $meeting_timeslot) {
				$meeting_timeslot['type'] = 'meeting_timeslots';
				$this->days[$i]['schedule_timeslots'][] = $meeting_timeslot;
			}
			foreach ($day['personal_appointments'] as $personal_appointment) {
				$personal_appointment['type'] = 'personal_appointments';
				$this->days[$i]['schedule_timeslots'][] = $personal_appointment;
			}
			usort($this->days[$i]['schedule_timeslots'], function($a, $b) {
				$valA = floatval(str_replace(':', '.', $a['time_from']));
				$valB = floatval(str_replace(':', '.', $b['time_from']));
				if ($valA == $valB) {
					return 0;
				}
				return ($valA < $valB) ? -1 : 1;
			});
		}

	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this
			->markdown('emails.delegate.planner')
			->with([
				'delegate' => $this->delegate,
				'days' => $this->days,
				'dashboard_url' => route('frontend.dashboard', $this->delegate->conference->url_slug),
				'header' => $this->header,
				'footer' => $this->footer,
				'global' => $this->global,
				'body' => $this->content,
			]);
	}
}
