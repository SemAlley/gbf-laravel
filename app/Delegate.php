<?php
namespace App;

use App\Mail\DelegateResetPasswordNotification;
use Schema;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
/**
 * @mixin \Eloquent
 */
class Delegate extends Authenticatable {

	use Notifiable;

    public $table = "delegates";
//    public $fillable = ['*'];
    public $guarded = ['id', 'conference_id', 'email', 'password', 'uic'];
    public $hidden = ['password'];

    public function conference() {
        return $this->belongsTo('App\Conference');
    }

	public function photo() {
		return $this->hasOne(ConferenceAsset::class, 'id', 'photo');
	}

    public function getFillableColumns() {
        $delegate = new Delegate();
        $delegateTable = $delegate->getTable();

        $forbiddenValues = ['id', 'conference_id', 'remember_token', 'created_at', 'updated_at', 'photo'];
        $columns = Schema::getColumnListing($delegateTable);

        foreach($columns as $k => $v) {
            if(in_array($v, $forbiddenValues)) {
                unset($columns[$k]);
            }
        }

        return array_values($columns);
    }


	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token)
	{
		$this->notify(new DelegateResetPasswordNotification($token));
	}

	/**
	 * Get the meta for the delegate.
	 */
	public function meta()
	{
		return $this->hasMany('App\DelegateMeta');
	}

	/**
	 * Get the meta for the delegate.
	 */
	public function meetingRequests()
	{
		return $this->hasMany(MeetingRequest::class);
	}

	public function unavailableTimslots() {
		return $this->hasMany(UnavailableTimestot::class);
	}

	public function personalAppointments()
	{
		return $this->hasMany(PersonalAppointment::class);
	}

	public static function boot()
	{
		parent::boot();

		// cause a delete of a product to cascade to children so they are also deleted
		static::deleting(function($delegate)
		{
			$delegate->personalAppointments()->delete();
			$delegate->unavailableTimslots()->delete();
			$delegate->meetingRequests()->delete();
		});
	}
}
