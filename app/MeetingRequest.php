<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRequest extends Model
{
    public function delegate() {
    	return $this->belongsTo(Delegate::class);
    }

	public function target() {
		return $this->belongsTo(Delegate::class, 'target_id');
	}

	public function meetingTimeslot() {
		return $this->belongsTo(MeetingTimeslot::class, 'meeting_timeslot_id');
	}
}
