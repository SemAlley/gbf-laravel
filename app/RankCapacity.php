<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankCapacity extends Model
{
    protected $table = 'rank_capacity';

    protected $fillable = [
        'id',
        'rank',
        'capacity'
    ];
}
