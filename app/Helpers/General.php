<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class General
{

    public static function conferenceRoute($routeName, $param = false)
    {
        if ($param) {
            return route($routeName, [Request::segment(1), $param]);
        }

        return route($routeName, Request::segment(1));
    }

    public static function conferenceSlug($routeName)
    {
        $route = self::conferenceRoute($routeName);
        $routeChunks = explode('/',$route);

        return $routeChunks[count($routeChunks)-1];
    }

    public static function countdownDateFromString($string)
    {
        $d = date_parse($string);

        return $d['day'].' '.\DateTime::createFromFormat('!m', $d['month'])->format('F').' '.$d['year'];
    }

    public static function CollectionToSelect2($collection)
    {
        $select = [];
        foreach ($collection as $c) {
            $select[] = ['text' => $c->name, 'id' => $c->id];
        };

        return json_encode($select);
    }

    public static function ImageCollectionToSelect2($collection)
    {
        $select = [];
        foreach ($collection as $c) {
            $select[] = ['text' => $c->filename, 'id' => $c->id, 'path' => $c->path, 'size' => $c->size, 'width' => $c->width, 'height' => $c->height];
        };

        return json_encode($select);
    }

    public static function CollectionToTable($collection)
    {
        if (!$collection) {
            return "[]";
        }
        $table = [];
        foreach ($collection as $c) {
            $table[] = $c;
        }

        return json_encode($table);
    }

    public static function AvailableThemes()
    {
        $themes = File::directories(resource_path('views').'/frontend/themes');

        $select = [];
        foreach ($themes as $c) {
            $t = array_last(explode('/', $c));
            $select[] = ['text' => $t, 'id' => $t];
        };

        return json_encode($select);
    }

    public static function adjustBrightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }

	/**
	 * @param $data
	 * @param $by
	 *
	 * @return array
	 */
    public static function orderDataBy($data, $by) {
    	usort($data, function($a, $b) use ($by) {
		    if ( $a[$by] == $b[$by] ) {
			    return 0;
		    }
		    return $a[$by] < $b[$by] ? -1 : 1;
	    });
	    return $data;
    }

}
