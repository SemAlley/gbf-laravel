<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\PageAsset;
use App\Speaker;
use App\Content;
use App\Http\Controllers\Controller;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class KnowledgeHubController extends Controller {

	public function index( Conference $conference ) {
		$data = [
			'global'         => Content::GetContent( 'global', $conference ),
			'content'        => Content::GetContent( 'knowledge-hub', $conference ),
			'reports'        => PageAsset::with(['asset', 'preview'])
			                             ->where( 'conference_id', $conference->id )
			                             ->where( 'page', 'knowledge-hub' )
			                             ->where( 'type', 'report' )->get()->toArray(),
			'galleries'      => PageAsset::with(['gallery', 'gallery.preview'])
			                             ->where( 'conference_id', $conference->id )
			                             ->where( 'page', 'knowledge-hub' )
			                             ->where( 'type', 'gallery' )->get()->toArray(),
			'press_releases' => PageAsset::with(['asset', 'preview'])
			                             ->where( 'conference_id', $conference->id )
			                             ->where( 'page', 'knowledge-hub' )
			                             ->where( 'type', 'press_release' )->get()->toArray(),
		];

		return view(
			'frontend.themes.' . $conference->frontend_theme . '.knowledge-hub',
			$data
		);
	}

}
