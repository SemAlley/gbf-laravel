<?php

namespace App\Http\Controllers\Frontend;


use App\Conference;
use App\Http\Controllers\Controller;
use App\UnavailableTimestot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TimeslotController extends Controller {

	private $conference;

	/**
	 * ProfileController constructor.
	 *
	 * @param  Request $request
	 */
	public function __construct( Request $request ) {
		$this->middleware( 'delegate.auth' );

		$currentConference = $request->segment( 1 );
		$this->conference  = Conference::where( 'url_slug', $currentConference )->first();
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getUnavailable() {
		$delegate = Auth::guard( 'delegate' )->user();

		$unavailableList = $delegate->unavailableTimslots()->get()->groupBy('type');

		return response()->json( $unavailableList );
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateUnavailable( Request $request ) {
		$delegate = Auth::guard( 'delegate' )->user();

		$unavailableList = $delegate->unavailableTimslots;

		$meetings = array_unique($request->get( 'meetings' ));
		$programs = array_unique($request->get( 'programs' ));
		$appointments = array_unique($request->get( 'appointments' ));

		$deleteItems = [];

		foreach ( $unavailableList as $item ) {
			if ( $item->type === 'meetings' && ( $key = array_search( $item->timeslot_id, $meetings ) ) !== false ) {
				unset( $meetings[ $key ] );
			} else if ( $item->type === 'appointments' && ( $key = array_search( $item->timeslot_id, $appointments ) ) !== false ) {
				unset( $appointments[ $key ] );
			} else if ( $item->type === 'programs' && ( $key = array_search( $item->timeslot_id, $programs ) ) !== false ) {
				unset( $programs[ $key ] );
			} else {
				$deleteItems[] = $item->id;
			}
		}

		if ( ! empty( $deleteItems ) ) {
			UnavailableTimestot::destroy( $deleteItems );
		}

		foreach ( $meetings as $meeting ) {
			$delegate->unavailableTimslots()->create( [
				'timeslot_id' => $meeting,
				'type'        => 'meetings'
			] );
		}

		foreach ( $appointments as $appointment ) {
			$delegate->unavailableTimslots()->create( [
				'timeslot_id' => $appointment,
				'type'        => 'appointments'
			] );
		}

		foreach ( $programs as $program ) {
			$delegate->unavailableTimslots()->create( [
				'timeslot_id' => $program,
				'type'        => 'programs'
			] );
		}

		return response()->json( [ 'success' => true ] );
	}

}