<?php

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class BaseController extends Controller {


	/**
	 * Get View based on user device
	 *
	 * @param $view
	 * @param array $args
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	protected function getView( $view, $args = [], Conference $conference ) {
		$viewPath = 'frontend.themes.' . $conference->frontend_theme . '.';
		if ( (Request::hasHeader( 'X-App-Type' ) && Request::header( 'X-App-Type' ) == 'gbf-mobile-react-native') || Request::get('mobile_mode') ) {
			$viewPath .= 'mobile-view.';
		}

		return view(
			$viewPath . $view,
			$args
		);
	}
}
