<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\ConferenceAsset;
use App\Content;
use App\Country;
use App\Delegate;
use App\DelegateMeta;
use App\Helpers\AssetSave;
use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Mail\NewDelegateNotification;
use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use Validator;
use App\Conference;

/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;
	use AssetSave;

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	public $assetType = 'delegate';

	private $conference;


	/**
	 * Create a new controller instance.
	 *
	 * @var Request $request
	 *
	 * @return void
	 */
	public function __construct( Request $request ) {
		$this->redirectTo = General::conferenceRoute( 'frontend.index' );
		$this->middleware( 'delegate.guest' );

		$currentConference = $request->segment( 1 );
		$this->conference  = Conference::where( 'url_slug', $currentConference )->first();
	}

	protected function getConferenceExtraFields( Conference $conference ) {
		$extra = [];
		if ( $conference->url_slug === 'latin-america' || $conference->url_slug === 'dev-conf' ) {
			$extra = [
//				'GBFConnect_Onsite' => [
//					'id'        => "gbf_connect_onsite",
//					'name'      => "I am interested in the business matching service for face-to-face meetings with relevant UAE-based companies during the forum (28th Feb). The deadline for registration for GBFConnect Onsite is Sunday, 21st Jan 2018 or once maximum capacity is reached. ",
//					'title'     => 'GBFConnect Onsite (Available for Latin American and International delegates only)',
//					'type'      => "boolean",
//					'css_class' => 'col-sm-offset-3',
//					'popup'     => [
//						'title'   => 'GBFConnect Onsite',
//						'content' => 'GBFConnect Onsite is the GBF Latin America business matching service especially aimed at our international guests, but also open to UAE-based delegates. The GBF Member Relations team will schedule face-to-face meetings for international delegates with relevant UAE-based companies during the afternoon of 28th February. Meetings last 30 minutes and will take place in the Forum meeting zone. Visitors registered for the service will be introduced to their meeting guests by the GBF Team. To register your interest in the service please tick the GBFConnect Onsite box on the registration page or email us on gbflatam@economist.com.'
//					]
//				],
				'BRIDGE_PROGRAMME'  => [
					'id'        => "bridge_programme",
					'name'      => "I am interested in the pre-forum programme taking place on 25th & 26th February.",
					'title'     => 'BRIDGE PROGRAMME (Available for Latin American and International delegates only)',
					'type'      => "boolean",
					'css_class' => 'col-sm-offset-3',
					'popup'     => [
						'title'   => 'GBF LATIN AMERICA BRIDGE PROGRAMME',
						'content' => 'The Dubai Chamber of Commerce & Industry will be hosting a Bridge Programme on February 25th and 26th for those who will be in Dubai ahead of the forum. Day 1 will include sessions on Dubai’s outlook, the Halal industry, and how to set up a business in Dubai. Day 2 will start off with a traditional Emirati breakfast at the Sheikh Mohammed Centre for Cultural Understanding (SMCCU), followed by an option to visit to DP World Terminal 3 at Jebel Ali Port, the world’s largest semi-automated facility or a visit and presentation about the founding of the UAE at the new Al Etihad Museum. A full schedule is available in the programme. To register your interest in attending, please tick the Bridge Programme box on the registration page or email us on gbflatam@economist.com.'
					],
					'condition' => [
						'field' => 'country',
						'compare' => '!=',
						'value' => 'United Arab Emirates'
					]
				],
				'PHOTO_CONSENT'     => [
					'id'        => "photography_check",
					'name'      => "I have read and consent to the filming and photography notice.",
					'title'     => 'PHOTOGRAPHY CONSENT',
					'required'  => true,
					'type'      => "boolean",
					'css_class' => 'col-sm-offset-3',
					'popup'     => [
						'title'   => 'NOTICE OF FILMING AND PHOTOGRAPHY',
						'content' => 'Please be advised that photos, video and audio footage will be taken at this event. By entering the event premises, you give consent to Dubai Chamber to photograph and video record your presence at the event, and its subsequent release, publication, exhibition, or reproduction to be used for news, promotional purposes, advertising, inclusion on websites, social media, or any other purpose by Dubai Chamber and its partners. You release Dubai Chamber, its employees, and every person involved from any liability connected with the taking, recording, digitizing, or publication and use of interviews, photographs, video and audio recordings. By entering the event premises, you waive all rights you may have to any claims for payment or royalties in connection with any use, exhibition, streaming, web-casting, televising, or other publication of these materials. You also waive any right to inspect or approve any photo, video, or audio recording taken by Dubai Chamber or the person or entity designated to do so by Dubai Chamber. By checking this box, You acknowledge that you have been fully informed of your consent, waiver of liability, and release before entering the event.'
					]
				]
			];
		} else if ( $conference->url_slug == 'africa' ) {
			$extra = [
				'PHOTO_CONSENT' => [
					'id'        => "photography_check",
					'name'      => "I have read and consent to the filming and photography notice.",
					'title'     => 'PHOTOGRAPHY CONSENT',
					'required'  => true,
					'type'      => "boolean",
					'css_class' => 'col-sm-offset-3',
					'popup'     => [
						'title'   => 'NOTICE OF FILMING AND PHOTOGRAPHY',
						'content' => 'Please be advised that photos, video and audio footage will be taken at this event. By entering the event premises, you give consent to Dubai Chamber to photograph and video record your presence at the event, and its subsequent release, publication, exhibition, or reproduction to be used for news, promotional purposes, advertising, inclusion on websites, social media, or any other purpose by Dubai Chamber and its partners. You release Dubai Chamber, its employees, and every person involved from any liability connected with the taking, recording, digitizing, or publication and use of interviews, photographs, video and audio recordings. By entering the event premises, you waive all rights you may have to any claims for payment or royalties in connection with any use, exhibition, streaming, web-casting, televising, or other publication of these materials. You also waive any right to inspect or approve any photo, video, or audio recording taken by Dubai Chamber or the person or entity designated to do so by Dubai Chamber. By checking this box, You acknowledge that you have been fully informed of your consent, waiver of liability, and release before entering the event.'
					]
				]
			];
		}

		return $extra;
	}

	/**
	 * Show the application registration form.
	 *
	 * @param \App\Conference $conference
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showRegistrationForm( Conference $conference ) {

		if ( ! $conference->registration_enabled ) {
			return response()->redirectToRoute( 'frontend.index', [ $conference->url_slug ] );
		}

		$extraFields = $this->getConferenceExtraFields( $conference );

		$data = [
			'global'       => Content::GetContent( 'global', $conference ),
			'register'     => Content::GetContent( 'register', $conference ),
			'countries'    => Country::select( [
				'name as value',
				'name as text',
				'calling_code as cc'
			] )->orderBy('name','ASC')->get()->toJson(),
			'extra_fields' => $extraFields
		];

		return view(
			'frontend.themes.' . $conference->frontend_theme . '.register',
			$data
		);
	}


	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function register( Request $request ) {
		$this->validator( $request->all() )->validate();

		event( new Registered( $user = $this->create( $request->all() ) ) );

//		$this->guard()->login($user);

		return $this->registered( $request, $user )
			?: redirect( $this->redirectPath() );
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator( array $data ) {
		$validateRules = [
			'email'        => 'required|email|max:255',
			'uic'          => 'required',
			'first_name'   => 'required|max:255',
			'last_name'    => 'required|max:255',
			'title'        => 'required|max:255',
			'age_group'    => 'required|max:255',
			'nationality'  => 'required|max:255',
			'address1'     => 'required|max:255',
			'postcode'     => 'required|max:255',
			'country'      => 'required|max:255',
			'job_title'    => 'required|max:255',
			'organisation' => 'required|max:255',
			'phone_mobile' => 'required|max:255',
			'industry'     => 'required|max:255',
			'photo'        => 'nullable|image64:jpeg,jpg,png',
		];
		if($this->conference->login_enabled) {
			$validateRules['password'] = 'required|min:6|confirmed';
		}
		return Validator::make(
			$data,
			$validateRules
		);
	}

	public function checkUIC( Conference $conference, Request $request ) {
		Validator::make( $request->all(), [
			'uic' => 'required',
		] )->validate();

		$success  = false;
		$delegate = Delegate::where( 'conference_id', $conference->id )
		                    ->where( 'uic', $request->get( 'uic' ) )
		                    ->where( 'enabled', true )
		                    ->first();
		if ( $delegate ) {
			$success = true;
		}

		return response()->json( [
			'success'  => $success,
			'delegate' => $delegate
		] );
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 *
	 * @return Delegate
	 */
	protected function create( array $data ) {
		$data = array_filter(
			$data,
			function ( $el ) {
				return ! is_null( $el );
			}
		);

		$delegate = new Delegate();
		$photo    = 0;
		if ( isset( $data['photo'] ) && is_array( $data['photo'] ) ) {
			$photo = $this->savePhoto( $data['photo'] );
		}

		$extraFields  = $this->getConferenceExtraFields( $this->conference );
		$extraExisted = [];
		foreach ( $extraFields as $extra_field ) {
			if ( isset( $data[ $extra_field['id'] ] ) ) {
				$extraExisted[ $extra_field['id'] ] = $data[ $extra_field['id'] ];
			}
		}

		$fillable = $delegate->getFillableColumns();

		foreach ( $data as $key => $val ) {
			if ( ! in_array( $key, $fillable ) ) {
				unset( $data[ $key ] );
			}
		}
		$delegate      = Delegate::where( 'uic', $data['uic'] )->first();
		$data['photo'] = $photo;
		$delegate->fill( $data );
		$delegate->email         = $data['email'];
		if($this->conference->login_enabled) {
			$delegate->password      = Hash::make( $data['password'] );
		} else {
			$delegate->password      = Hash::make( str_random(8) );
		}
		$delegate->conference_id = $this->conference->id;
		$delegate->enabled       = true;
		$delegate->confirmed     = true;
		$delegate->save();

		foreach ( $extraExisted as $extra_key => $extra_val ) {
			$meta = new DelegateMeta( [
				'meta_key'   => $extra_key,
				'meta_value' => $extra_val
			] );
			$delegate->meta()->save( $meta );
		}

		try {
			\Mail::to( $delegate->email )->send( new NewDelegateNotification( $delegate, 'delegate' ) );
			\Mail::to( env( 'MAIL_FROM_ADDRESS' ) )->send( new NewDelegateNotification( $delegate, 'admin' ) );
		} catch ( \Exception $e ) {
			logger( 'RegisterDelegate Mail Notification ::: ', [ $e->getMessage(), $e->getFile(), $e->getLine() ] );
		}

		return $delegate;
	}


	/**
	 * Store photo, return asset id
	 *
	 * @param $photo
	 *
	 * @return bool|mixed
	 */
	private function savePhoto( $photo ) {
		return $this->saveFile( $photo, $this->assetType );
	}


	/**
	 * The user has been registered.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  mixed $user
	 *
	 * @return mixed
	 */
	protected function registered( Request $request, $user ) {
		return response()->json( [
			'success'  => true,
			'delegate' => $user
		] );
	}


	/**
	 * Get the guard to be used during registration.
	 *
	 * @return \Illuminate\Contracts\Auth\StatefulGuard
	 */
	protected function guard() {
		return Auth::guard( 'delegate' );
	}
}
