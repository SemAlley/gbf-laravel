<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Content;
use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Conference;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        attemptLogin as attemptLoginAtAuthenticatesUsers;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    public function __construct(Conference $conference, Request $request)
    {
    	$this->redirectTo = General::conferenceRoute('frontend.dashboard');
    	if($request->get('login_dest')) {
		    $this->redirectTo = General::conferenceRoute($request->get('login_dest')). '?mobile_mode=1';
	    }
        $this->middleware('delegate.guest', ['except' => ['logout', 'mobileLogout', 'mobileLogin']]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm(Conference $conference)
    {
    	//TODO: Return to prev solution if need
//    	$data = [
//		    'global' => $conference->content->where('page', 'global')->keyBy('identifier'),
//		    'login'  => $conference->content->where('page', 'login')->keyBy('identifier'),
//	    ];
	    $data = [
		    'global' => Content::GetContent('global', $conference),
		    'login'  => Content::GetContent('login', $conference),
	    ];
        return $this->getView(
            'login',
	        $data,
	        $conference
        );
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect(General::conferenceRoute('frontend.login'));
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
//	    return $this->guard()->attempt(
//		    $this->credentials($request), $request->has('remember'), 'enabled'
//	    );
        return $this->attemptLoginAtAuthenticatesUsers($request);
    }

    public function mobileLogin(Request $request){
        $login =  $this->attemptLoginAtAuthenticatesUsers($request);

        return response()->json([
            'success' => $login
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function credentials(Request $request)
    {

	    $currentConference = $request->segment( 1 );
	    $conference = Conference::where( 'url_slug', $currentConference )->first();

        $creds = $request->only($this->username(), 'password');
        $creds['enabled'] = true;
        $creds['conference_id'] = $conference->id;

        return $creds;
    }

	/**
	 * The user has been authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  mixed  $user
	 * @return mixed
	 */
	protected function authenticated(Request $request, $user)
	{
		return response()->json(['success' => true, 'redirect' => $this->redirectPath()]);
	}


	protected function guard()
	{
		return Auth::guard('delegate');
	}
}
