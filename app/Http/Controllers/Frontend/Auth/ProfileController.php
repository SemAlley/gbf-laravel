<?php

namespace App\Http\Controllers\Frontend\Auth;


use App\Conference;
use App\Content;
use App\Country;
use App\Day;
use App\Delegate;
use App\Helpers\AssetSave;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\BaseController;
use App\Mail\MyPlannerReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class ProfileController extends BaseController {

	use AssetSave;

	public $assetType = 'delegate';

	private $conference;

	/**
	 * ProfileController constructor.
	 */
	public function __construct( Request $request ) {
		$this->middleware( 'delegate.auth', ['except' => ['mobileIndex', 'updatePhoto']] );

		$currentConference = $request->segment( 1 );
		$this->conference  = Conference::where( 'url_slug', $currentConference )->first();
	}

	/**
	 * @param Conference $conference
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$data     = [
			'global'        => Content::GetContent( 'global', $conference ),
			'content'       => Content::GetContent( 'dashboard', $conference ),
			'delegate'      => $delegate,
			'countries'     => Country::select( [
				'name as value',
				'name as text',
				'calling_code as cc'
			] )->orderBy( 'name', 'asc' )->get()->toJson(),
			'profile_photo' => Content::AssetPath( $delegate->photo )
		];

		return $this->getView(
			'dashboard.index',
			$data,
			$conference
		);
	}

    public function mobileIndex( Conference $conference ) {
	    if(!Auth::guard('delegate')->user()){
	        return response()->json([
	            'success' => false,
            ]);
        }
        $delegate = Auth::guard( 'delegate' )->user();
        $data     = [
            'global'        => Content::GetContent( 'global', $conference ),
            'content'       => Content::GetContent( 'dashboard', $conference ),
            'days'          => Day::with('meetingTimeslots')->where('conference_id',$conference->id)->get(),
            'delegate'      => $delegate,
            'countries'     => Country::select( [
                'name as value',
                'name as text',
                'calling_code as cc'
            ] )->orderBy( 'name', 'asc' )->get()->toJson(),
            'profile_photo' => Content::AssetPath( $delegate->photo )
        ];

        return response()->json([
            'data' => $data,
            'conference' => $conference
        ]);
    }

	public function appMobilePlanner( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$data     = [
			'global'        => Content::GetContent( 'global', $conference ),
			'content'       => Content::GetContent( 'dashboard', $conference ),
			'delegate'      => $delegate,
			'countries'     => Country::select( [
				'name as value',
				'name as text',
				'calling_code as cc'
			] )->orderBy( 'name', 'asc' )->get()->toJson(),
			'profile_photo' => Content::AssetPath( $delegate->photo )
		];

		return $this->getView(
			'my-planner',
			$data,
			$conference
		);
	}

	public function appMobileProfile( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$data     = [
			'global'        => Content::GetContent( 'global', $conference ),
			'content'       => Content::GetContent( 'dashboard', $conference ),
			'delegate'      => $delegate,
			'countries'     => Country::select( [
				'name as value',
				'name as text',
				'calling_code as cc'
			] )->orderBy( 'name', 'asc' )->get()->toJson(),
			'profile_photo' => Content::AssetPath( $delegate->photo )
		];

		return $this->getView(
			'profile',
			$data,
			$conference
		);
	}

	public function appMobileDelegates( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$data     = [
			'global'        => Content::GetContent( 'global', $conference ),
			'content'       => Content::GetContent( 'dashboard', $conference ),
			'delegate'      => $delegate,
			'countries'     => Country::select( [
				'name as value',
				'name as text',
				'calling_code as cc'
			] )->orderBy( 'name', 'asc' )->get()->toJson(),
			'profile_photo' => Content::AssetPath( $delegate->photo )
		];

		return $this->getView(
			'delegates',
			$data,
			$conference
		);
	}

	/**
	 * @param Conference $conference
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function update( Conference $conference, Request $request ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$data     = $request->all();
		$this->validator( $data )->validate();
		$success  = false;
		$newPhoto = null;

		$data = array_filter(
			$data,
			function ( $el ) {
				return ! is_null( $el );
			}
		);

		if ( isset( $data['photo'] ) && is_array( $data['photo'] ) ) {
			$photo         = $this->savePhoto( $data['photo'] );
			$data['photo'] = $photo;
			$newPhoto      = Content::AssetPath( $photo );
		}

		$fillable = $delegate->getFillableColumns();

		foreach ( $data as $key => $val ) {
			if ( ! in_array( $key, $fillable ) ) {
				unset( $data[ $key ] );
			}
		}
		try {
			$delegate->fill( $data );
			$delegate->email = $data['email'];
			$success         = $delegate->save();
		} catch ( \Exception $e ) {
			logger( 'Profile update exception', [ $e->getMessage(), $e->getFile() ] );
			logger( 'Profile to update', [ $delegate ] );
		}

		return response()->json( [
			'success'  => $success,
			'delegate' => $delegate,
			'newPhoto' => $newPhoto
		] );
	}

	/**
	 * Update profile photo
	 *
	 * @param Conference $conference
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updatePhoto( Conference $conference, Request $request ) {

		$delegate = Auth::guard( 'delegate' )->user();
		$data     = $request->all();
		Validator::make( $data, [
			'photo' => 'required|image64:jpeg,jpg,png',
		] )->validate();
		$photo    = $this->savePhoto( $data['photo'] );
		$newPhoto = Content::AssetPath( $photo );

		$delegate->photo = $photo;
		$success         = $delegate->save();

		return response()->json( [
			'success'  => $success,
			'newPhoto' => $newPhoto
		] );
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 *
	 * @return mixed
	 */
	protected function validator( array $data ) {
		$delegate = Auth::guard( 'delegate' )->user();

		return Validator::make(
			$data,
			[
				'email'        => 'required|email|max:255|unique:delegates,email,' . $delegate->id . ',id,conference_id,' . $delegate->conference_id,
//				'password'     => 'required|min:6|confirmed',
				'first_name'   => 'required|max:255',
				'last_name'    => 'required|max:255',
				'title'        => 'required|max:255',
				'age_group'    => 'required|max:255',
				'nationality'  => 'required|max:255',
				'address1'     => 'required|max:255',
				'postcode'     => 'required|max:255',
				'country'      => 'required|max:255',
				'job_title'    => 'required|max:255',
				'organisation' => 'required|max:255',
				'phone_mobile' => 'required|max:255',
				'industry'     => 'required|max:255',
				'photo'        => 'nullable|image64:jpeg,jpg,png',
			]
		);
	}

	/**
	 * Store photo, return asset id
	 *
	 * @param $photo
	 *
	 * @return bool|mixed
	 */
	private function savePhoto( $photo ) {
		return $this->saveFile( $photo, $this->assetType );
	}

	private function getDeledatePlanner( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();

		ini_set('memory_limit', '-1');
		logger('me', [$delegate->toArray()]);
		$unavailableList = $delegate->unavailableTimslots->groupBy( 'type' )->toArray();

		foreach ( $unavailableList as $key => $item ) {
			$unavailableList[ $key ] = array_map( function ( $timeslot ) {
				return $timeslot['timeslot_id'];
			}, $item );
		}

		$days = $conference->days()->with( [
			'scheduleTimeslots'                  => function ( $q ) use ( $unavailableList ) {
				if ( isset( $unavailableList['programs'] ) ) {
					$q->whereNotIn( 'id', $unavailableList['programs'] );
				}
			},
			'scheduleTimeslots.speakers',
			'meetingTimeslots',
			'meetingTimeslots.meetingRequests'   => function ( $q ) use ( $delegate ) {
				$q->where( 'status', '!=', 'declined' )->where( function ( $query ) use ( $delegate ) {
					$query->where( 'delegate_id', $delegate->id )->orWhere( 'target_id', $delegate->id );
				} );
				if ( isset( $unavailableList['meetings'] ) ) {
					$q->whereNotIn( 'id', $unavailableList['meetings'] );
				}
			},
			'meetingTimeslots.meetingRequests.target',
			'meetingTimeslots.meetingRequests.target.photo',
			'meetingTimeslots.meetingRequests.delegate',
			'meetingTimeslots.meetingRequests.delegate.photo',
			'personalAppointments'               => function ( $q ) use ( $delegate ) {
				$q->where( 'delegate_id', $delegate->id );
				if ( isset( $unavailableList['appointments'] ) ) {
					$q->whereNotIn( 'id', $unavailableList['appointments'] );
				}
			}
		] )->get();

		return $days;
	}

	/**
	 * Retrieve Meeting time slots
	 *
	 * @param Conference $conference
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getMeetingTimeSlots( Conference $conference ) {
		try {
			$days = $this->getDeledatePlanner( $conference );
		} catch (\Exception $e) {
			$days = [];
			logger('@getMeetingTimeSlots Exception', [$e->getMessage(), $e->getFile(), $e->getLine()]);
		}

		return response()->json( $days );
	}

	/**
	 * Retrieve list for all available industries
	 *
	 * @param Conference $conference
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getDelegateIndustries( Conference $conference ) {

		$industries = $conference->delegates()->select( 'industry' )->whereNotNull( 'industry' )->where( 'confirmed', 1 )->where( 'industry', '!=', '' )->distinct()->orderBy( 'industry', 'asc' )->get()->toArray();

		$industries = array_map( function ( $item ) {
			return strtoupper( $item['industry'] );
		}, $industries );

		return response()->json( $industries );
	}

	/**
	 * Get filtered delegates list
	 *
	 * @param Conference $conference
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getFilteredDelegates( Conference $conference, Request $request ) {
		$delegate = Auth::guard( 'delegate' )->user();

		$delegatesQuery = $conference->delegates()->with( 'photo' )
		                             ->where( 'id', '!=', $delegate->id )
		                             ->where( 'enabled', 1 )
		                             ->where( 'confirmed', 1 )
		                             ->where( 'is_hidden', 0 );

		if ( $request->get( 'industry' ) ) {
			$delegatesQuery->where( 'industry', $request->get( 'industry' ) );
		}

		if ( $request->get( 'country' ) ) {
			$delegatesQuery->where( 'country', $request->get( 'country' ) );
		}

		if ( $request->get( 'sort' ) ) {
			$delegatesQuery->orderBy( 'first_name', $request->get( 'sort' ) );
		}

		$delegates = $delegatesQuery->get();

		return response()->json( $delegates );
	}

	/**
	 * Send Planner table via Email
	 *
	 * @param Conference $conference
	 */
	public function plannerEmail( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$days     = $this->getDeledatePlanner( $conference )->toArray();
		\Mail::to( $delegate->email )->send( new MyPlannerReport( $delegate, $days ) );
	}


	/**
	 * Print Planner table as PDF
	 *
	 * @param Conference $conference
	 *
	 * @return mixed
	 */
	public function plannerPdf( Conference $conference ) {
		$delegate = Auth::guard( 'delegate' )->user();
		$days     = $this->getDeledatePlanner( $conference )->toArray();
		foreach ( $days as $i => $day ) {
			foreach ( $day['meeting_timeslots'] as $meeting_timeslot ) {
				$meeting_timeslot['type']           = 'meeting_timeslots';
				$days[ $i ]['schedule_timeslots'][] = $meeting_timeslot;
			}
			foreach ( $day['personal_appointments'] as $personal_appointment ) {
				$personal_appointment['type']       = 'personal_appointments';
				$days[ $i ]['schedule_timeslots'][] = $personal_appointment;
			}
			usort( $days[ $i ]['schedule_timeslots'], function ( $a, $b ) {
				$valA = floatval( str_replace( ':', '.', $a['time_from'] ) );
				$valB = floatval( str_replace( ':', '.', $b['time_from'] ) );
				if ( $valA == $valB ) {
					return 0;
				}

				return ( $valA < $valB ) ? - 1 : 1;
			} );
		}
		$global = Content::GetContent( 'global', $conference );
		$pdf    = \PDF::loadView(
			'frontend.themes.' . $conference->frontend_theme . '.pdf.planner',
			compact( 'delegate', 'days', 'global' )
		);

		return $pdf->stream();
	}
}
