<?php

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\Content;
use App\Day;
use App\Page;
use App\PageAsset;
use App\Http\Controllers\Controller;
use App\ScheduleTimeslot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Mail\SpeakerQuestion;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MobileController extends Controller
{

    public function index(Request $request)
    {
        $headers = [

            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',

            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin'

        ];

        if ($request->getMethod() == "OPTIONS") {

            return Response::make('OK', 200, $headers);

        }

    }

    public function getApp()
    {

        header('Access-Control-Allow-Origin: *');

        return response()->json([
            'home' => [
                'images' => [
                    'programme' => asset('mobile-images/2.jpg'),
                    'delegates' => asset('mobile-images/3.jpg'),
                    'speakers' => asset('mobile-images/slack-imgs.com.jpg'),
                    'sponsors' => asset('mobile-images/5.jpg'),
                    'travelvenue' => asset('mobile-images/6.jpg'),
                    'logo' => asset('mobile-images/1.jpg')
                ]
            ],
            'auth' => [
                'check' => Auth::check()
            ]
        ]);
    }

    public function mobileLogout(Request $request){

//        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/latin-america/mobilelogoutdone');

    }

    public function getCurrentSession(Conference $conference)
    {

        $date = \Carbon\Carbon::now('Asia/Dubai');

        $days = $conference->days()->with('Scheduletimeslots')->get();

        $end_date_1 = "2018-02-27";
        $end_date_2 = "2018-02-28";

        $isConferenceDay = false;
        $currentDay = null;
        $isSessionTime = false;
        $currentSession = null;

        $date_now = $date->format('Y-m-d');
        $time_now = $date->format('H:i');

        foreach($days as $d){
            if($date_now == $d->date){
                $isConferenceDay = true;
                $currentDay = $d->name;
                foreach($d->Scheduletimeslots as $s){
                    if($time_now >= $s->time_from && $time_now <= $s->time_to){
                        $isSessionTime = true;
                        $currentSession = $s->title;
                    }
                }
            }
        }

        $isEndOfDay = false;
        $end_day = null;

        if($date_now == $end_date_1){ //is day 1
            $end_day = 1;
            if($date->between(
                \Carbon\Carbon::create('2018','02', '27', '17','00', '0','Asia/Dubai'),
                \Carbon\Carbon::create('2018','02', '27', '17','45', '0', 'Asia/Dubai')
            )){
                $isEndOfDay = true;
            }
        }

        if($date_now == $end_date_2){ //is day 2
            $end_day = 2;
            if($date->between(
                \Carbon\Carbon::create('2018','02', '28', '12','25', '0','Asia/Dubai'),
                \Carbon\Carbon::create('2018','02', '28', '12','35', '0', 'Asia/Dubai')
            )){
                $isEndOfDay = true;
            }
        }

        return response()->json([
            'success' => true,
            'programme' => [
                'date' => $date,
                'timeNow' => $time_now,
                'isConferenceDay' => $isConferenceDay,
                'currentDay' => $currentDay,
                'isSessionTime' => $isSessionTime,
                'currentSession' => $currentSession,
                'isEndOfDay' => $isEndOfDay,
                'endDay' => $end_day
            ]
        ]);
    }

    public function putPushToken(Request $r){

        $check = DB::table('mobile_device')->where('device', $r->input('device'))->first();

        if($check){
            return response()->json([
                'success' => true
            ]);
        }

        $expo = \ExponentPhpSDK\Expo::normalSetup();

        $expo->subscribe($r->input('device'),$r->input('token'));

        DB::table('mobile_device')->where('pushtoken', $r->input('token'))->delete();

        DB::table('mobile_device')->insert([
            'pushtoken' => $r->input('token'),
            'device' => $r->input('device'),
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

        return response()->json([
            'success' => true
        ]);

    }

    public function getSlidoUrl(){
        return redirect('https://app2.sli.do/event/20turbg2');
    }

    public function getImage(Conference $conference, $id)
    {
        return redirect(asset(Content::AssetPath($id)));
    }

    public function getKhReports(Conference $conference)
    {
        $data = [
            'global' => Content::GetContent('global', $conference),
            'content' => Content::GetContent('knowledge-hub', $conference),
            'reports' => PageAsset::with(['asset', 'preview'])
                ->where('conference_id', $conference->id)
                ->where('page', 'knowledge-hub')
                ->where('type', 'report')->get()->toArray()
        ];

        return view(
            'frontend.themes.' . $conference->frontend_theme . '.mobile-view/kh-reports',
            $data
        );
    }

    public function getKhGallery(Conference $conference)
    {
        $data = [
            'global' => Content::GetContent('global', $conference),
            'content' => Content::GetContent('knowledge-hub', $conference),
            'galleries' => PageAsset::with(['gallery', 'gallery.preview'])
                ->where('conference_id', $conference->id)
                ->where('page', 'knowledge-hub')
                ->where('type', 'gallery')->get()->toArray(),
        ];

        return view(
            'frontend.themes.' . $conference->frontend_theme . '.mobile-view/kh-gallery',
            $data
        );
    }

    public function getKhPress(Conference $conference)
    {
        $data = [
            'global' => Content::GetContent('global', $conference),
            'content' => Content::GetContent('knowledge-hub', $conference),
            'press_releases' => PageAsset::with(['asset', 'preview'])
                ->where('conference_id', $conference->id)
                ->where('page', 'knowledge-hub')
                ->where('type', 'press_release')->get()->toArray(),
        ];

        return view(
            'frontend.themes.' . $conference->frontend_theme . '.mobile-view/kh-press',
            $data
        );
    }

    public function getMyPlanner(Conference $conference)
    {

        $delegate = Auth::guard('delegate')->user();
        $data = [
            'global' => Content::GetContent('global', $conference),
            'content' => Content::GetContent('dashboard', $conference),
            'delegate' => $delegate,
            'countries' => Country::select([
                'name as value',
                'name as text',
                'calling_code as cc'
            ])->orderBy('name', 'asc')->get()->toJson(),
            'profile_photo' => Content::AssetPath($delegate->photo)
        ];

        return $this->getView(
            'dashboard.index',
            $data,
            $conference
        );
    }

    private $images = [

        "15071220358.jpg" => 1703,

        "15071220357.jpg" => 1702,

        "Al-Iktissad-Wal-Aamal.jpg" => 1701,

        "15069374724.jpg" => 1700,

        "15058383558.jpg" => 1699,

        "Dubai_Islamic_Bank.jpg" => 1781

    ];

    public function getSponsorImageExists(Conference $conference, $id)
    {

        if (isset($this->images[$id])) {
            return response()->json(['success' => true, 'exists' => true]);
        } else {
            return response()->json(['success' => true, 'exists' => false]);
        }
    }

    public function getSponsorImage(Conference $conference, $id)
    {

        if (isset($this->images[$id])) {
            return redirect(asset(Content::AssetPath($this->images[$id])));
        } else {
            return redirect('/no/image/found');
        }
    }

    public function getSpeakers(Conference $conference)
    {

        $content = Content::GetContent('speakers', $conference);
        $confirmed_speakers = isset($content['confirmed_speakers']) ? \GuzzleHttp\json_decode($content['confirmed_speakers']) : [];
        $previous_speakers = isset($content['previous_speakers']) ? \GuzzleHttp\json_decode($content['previous_speakers']) : [];

        if (!empty($confirmed_speakers)) {
            $ids_ordered = implode(',', $confirmed_speakers);
            $confirmed = $conference->speakers()->whereIn('id', $confirmed_speakers)
                ->where('enabled', true)
                ->orderByRaw('name', 'ASC')
                ->get()
                ->keyBy('name');
        } else {
            $confirmed = $conference->speakers()->where('type', '1')->where('enabled', true)->get();
        }

        if (!empty($previous_speakers)) {
            $ids_ordered = implode(',', $previous_speakers);
            $prev = $conference->speakers()->whereIn('id', $previous_speakers)
                ->where('enabled', true)
                ->orderByRaw('name', 'ASC')
                ->get()
                ->keyBy('name');
        } else {
            $prev = $conference->speakers()->where('type', '2')->where('enabled', true)->get();
        }

        return response()->json([
            'speakers' => [
                'confirmed' => $confirmed,
                'previous' => $prev,
            ]
        ]);
    }

    public function speakerQuestion(Request $r){
        DB::table('speaker_questions')->insert([
//            'speaker' => $r->input('speaker'),
            'question' => $r->input('question'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        \Mail::to('gbflatam@economist.com')->send(new SpeakerQuestion([
//            'speaker' => $r->input('speaker'),
            'question' => $r->input('question')
        ]));

        return response()->json([
            'success' => true,
//            'speaker' => $r->input('speaker')
        ]);
    }

    public function eventFeedback(Request $r){
        DB::table('event_feedback')->insert([
            'feedback' => $r->input('feedback'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function getTabById(Conference $conference, $tab, $index)
    {

        $page = Page::with(['tabs', 'tabs.blocks', 'tabs.blocks.data'])
            ->where('conference_id', $conference->id)
            ->where(function ($q) use ($tab) {
                $q->where('id', $tab)
                    ->orWhere('slug', $tab);
            })
            ->first();

        return view('frontend.themes.GBF2017.mobile-view.single-tab', [
                'global' => Content::GetContent('global', $conference),
                'content' => $page->toArray(),
                'tab' => $page['tabs'][$index]
            ]
        );
    }

    public function getTabJsonById(Conference $conference, $tab, $index)
    {

        $page = Page::with(['tabs', 'tabs.blocks', 'tabs.blocks.data'])
            ->where('conference_id', $conference->id)
            ->where(function ($q) use ($tab) {
                $q->where('id', $tab)
                    ->orWhere('slug', $tab);
            })
            ->first();

        return response()->json([
                'content' => $page->toArray(),
                'tab' => $page['tabs'][$index]
            ]
        );
    }

    public function getProgrammeTabById(Conference $conference, $tab, $index)
    {

        $page = Page::with(['tabs', 'tabs.blocks', 'tabs.blocks.data'])
            ->where('conference_id', $conference->id)
            ->where(function ($q) use ($tab) {
                $q->where('id', $tab)
                    ->orWhere('slug', $tab);
            })
            ->first();

        return view('frontend.themes.GBF2017.mobile-view.single-programme', [
                'global' => Content::GetContent('global', $conference),
                'content' => $page->toArray(),
                'tab' => $page['tabs'][$index]
            ]
        );
    }

    public function options()
    {
        return response(200);
    }
}
