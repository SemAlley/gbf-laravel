<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\Speaker;
use App\Content;
use App\Http\Controllers\Controller;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ProgrammeController extends BaseController
{

	/**
	 * Render Programme page
	 *
	 * @param Conference $conference
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function index(Conference $conference)
    {
        return $this->getView(
            'programme',
            [
                'global'    => Content::GetContent('global', $conference),
                'programme' => Content::GetContent('programme', $conference),
            ]
        );
    }
}
