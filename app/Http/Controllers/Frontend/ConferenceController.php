<?php

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\Content;
use App\Http\Controllers\Controller;
use DB;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ConferenceController extends Controller
{

    public function index(Conference $conference)
    {
        $speakerContent = Content::GetContent('speakers', $conference);
        $confirmed_speakers = isset($speakerContent['confirmed_speakers']) ? \GuzzleHttp\json_decode($speakerContent['confirmed_speakers']) : [];
        $ids_ordered = implode(',', $confirmed_speakers);
        if (!empty($ids_ordered)) {
            $confirmed = $conference->speakers()->whereIn('id', $confirmed_speakers)->where('featured', 1)->where('enabled', 1)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
        } else {
            $confirmed = [];
        }

        return view(
            'frontend.themes.'.$conference->frontend_theme.'.home',
            [
                'global'            => Content::GetContent('global', $conference),
                'home'              => Content::GetContent('home', $conference),
                'featured_speakers' => $confirmed,
                'conference_data'   => $conference,
            ]
        );
    }
}
