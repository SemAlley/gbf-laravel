<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\Gallery;
use App\Content;
use App\Http\Controllers\Controller;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class GalleryController extends Controller {

	public function single( Conference $conference, Gallery $gallery ) {
		$data = [
			'global'  => Content::GetContent( 'global', $conference ),
			'content' => Content::GetContent( 'gallery', $conference ),
			'gallery' => $gallery,
		];

		return view(
			'frontend.themes.' . $conference->frontend_theme . '.gallery.gallery-single',
			$data
		);
	}

}
