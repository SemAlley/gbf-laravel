<?php

namespace App\Http\Controllers\Frontend;

use App\Conference;
use App\Contact;
use App\Content;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactNotification;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ContactController extends BaseController
{

    public function index(Conference $conference)
    {

        return $this->getView(
            'contact',
            [
                'global'    => Content::GetContent('global', $conference),
                'contact'   => Content::GetContent('contact', $conference),
                'countries' => Country::select(['name as value', 'name as text'])->get()->toArray(),
            ], $conference
        );
    }


    public function store(Conference $conference, ContactRequest $request)
    {
        $data = $request->all();

        if (isset($data['countries_operate']) && is_array($data['countries_operate'])) {
            $data['countries_operate'] = implode(', ', $data['countries_operate']);
        }

        try {
            $contact = new Contact($data);
            $contact->conference_id = $conference->id;
            $contact->save();
            \Mail::to(env('MAIL_FROM_ADDRESS'))->send(new ContactNotification($contact, $conference));
        } catch (\Exception $e) {
            \Log::info('ContactController@store: ', [$e->getFile(), $e->getLine(), $e->getMessage()]);

            return response()->json(['success' => false]);
        }

        return response()->json(['success' => true]);
    }
}
