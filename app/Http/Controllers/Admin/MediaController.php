<?php namespace App\Http\Controllers\Admin;

use App\Helpers\CSVExporter;
use App\Media;
use Illuminate\Support\Facades\App;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MediaController extends BaseController
{

    use CSVExporter;


    /**
     * Display list of submitted contacts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $values = Media::with(['photo', 'article1', 'article2', 'article3'])->where('conference_id', $this->getActiveConferenceId())->get()->toArray();

        $values = array_map(
            function ($item) {
                $item['articles'] = [
                    $item['article1'],
                    $item['article2'],
                    $item['article3'],
                ];
                $item['links'] = [
                    $item['link1'],
                    $item['link2'],
                    $item['link3'],
                ];
                $item['articles'] = array_filter($item['articles']);
                $item['links'] = array_filter($item['links']);
                unset($item['article1'], $item['article2'], $item['article3']);
                unset($item['link1'], $item['link2'], $item['link3']);

                return $item;
            },
            $values
        );

        return view('admin.media.index', ['values' => $values]);
    }


    /**
     * Export data as CSV
     *
     * @return bool|mixed
     */
    public function export()
    {
	    $conferenceId = $this->getActiveConferenceId();
	    $list = Media::where('conference_id', $conferenceId)->with(['photo', 'article1', 'article2', 'article3'])->get()->toArray();

	    foreach ($list as $key => $item) {
	    	if($item['photo']) {
			    $list[$key]['photo'] = App::make('url')->to($item['photo']['path']);
		    }
		    if($item['article1']) {
			    $list[$key]['article1'] = App::make('url')->to($item['article1']['path']);
		    }
		    if($item['article2']) {
			    $list[$key]['article2'] = App::make('url')->to($item['article2']['path']);
		    }
		    if($item['article3']) {
			    $list[$key]['article3'] = App::make('url')->to($item['article3']['path']);
		    }
	    }

        return $this->exportCSV($list, 'media');
    }

}
