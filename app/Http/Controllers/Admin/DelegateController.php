<?php namespace App\Http\Controllers\Admin;

use App\Conference;
use App\ConferenceAsset;
use App\Helpers\CSVExporter;
use App\Http\Requests\Delegate\MassAppointmentRequest;
use App\Http\Requests\Delegate\PutDelegateRequest;
use App\Http\Requests\Delegate\UploadDelegateCsvRequest;
use App\Mail\DelegateBlankNotification;
use App\Mail\NewDelegateNotification;
use App\Mail\SendDelegateNotification;
use App\Matrix;
use App\User;
use Auth;
use Carbon\Carbon;
use Excel;
use Hash;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Collections\RowCollection;
use App\Http\Requests\EmptyRequest;
use App\Http\Requests\Delegate\PatchDelegateRequest;
use App\Delegate;
use File;
use Validator;
use ZipArchive;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class DelegateController extends BaseController {

	use CSVExporter;

	public function getAll() {
		$columns = [
			'id',
			'uic',
			'first_name',
			'last_name',
			'email',
			'country',
			'confirmed',
			'enabled',
			'updated_at'
		];
		$values  = Delegate::select( $columns )->where( 'conference_id', $this->getActiveConferenceId() )->get();

		return view( 'admin.delegate.index', [ 'values' => $values ] );
	}

	/**
	 * Retrieve single delegate
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function get( $id ) {
		$delegate = Delegate::find( $id );

		if ( ! $delegate ) {
			abort( 404 );
		}

		return response()->json( $delegate );
	}

	/**
	 * Update delegate
	 * Validate unique UIC
	 *
	 * @param $id
	 * @param PatchDelegateRequest $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function patch( $id, PatchDelegateRequest $request ) {
		$conferenceId = $this->getActiveConferenceId();
		$delegate     = Delegate::find( $id );

		if ( ! $delegate ) {
			abort( 404 );
		}

		$delegateData = $this->filterDelegate( $request->all() );

		if ( isset( $delegateData['uic'] ) ) {
			if ( Delegate::where( 'id', '!=', $id )
			             ->where( 'uic', $delegateData['uic'] )
			             ->where( 'conference_id', $conferenceId )
			             ->first()
			) {
				return response()->json( [
					'success' => false,
					'uic'     => 'The uic has already been taken.'
				], 422 );
			}
		}

		$fillable = $delegate->getFillableColumns();


		foreach ( $fillable as $f ) {
			if ( ! isset( $delegateData[ $f ] ) ) {
				continue;
			}
			if ( $f === "password" ) {
				if ( ! empty( $request->input( $f ) ) ) {
					$delegate->$f = Hash::make( $request->input( $f ) );
				}
			} else {
				$delegate->$f = $request->input( $f );
			}
		}

		$delegate->email         = $delegateData['email'] ?? null;
		$delegate->photo         = isset( $delegateData['photo'] ) ? $delegateData['photo'] : 0;
		$delegate->uic           = isset( $delegateData['uic'] ) ? $delegateData['uic'] : $this->getUniqueUIC( $conferenceId );
        $delegate->conference_id = $conferenceId;
        $delegate->rank          = isset( $delegateData['rank'] ) ? $delegateData['rank'] : null;
        $matrix                  = new Matrix();
        $matrix->reshuffle('all');

        return response()->json( [
			'success'  => $delegate->save(),
			'delegate' => $delegate
		] );
	}

	/**
	 * Create new delegate
	 *
	 * @param PutDelegateRequest $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function put( PutDelegateRequest $request ) {
		$conferenceId = $this->getActiveConferenceId();
		$delegate     = new Delegate();

		$delegateData = $this->filterDelegate( $request->all(), true );

		$delegate->fill( $delegateData );
		$delegate->email         = $delegateData['email'] ?? null;
		$delegate->uic           = isset( $delegateData['uic'] ) ? $delegateData['uic'] : $this->getUniqueUIC( $conferenceId );
		$delegate->password      = Hash::make( $request->password );
		$delegate->conference_id = $conferenceId;
        $matrix                  = new Matrix();
        $matrix->reshuffle('all');

		return response()->json( [
			'success'  => $delegate->save(),
			'delegate' => $delegate
		] );
	}

	/**
	 * Delete delegates by id list
	 *
	 * @param EmptyRequest $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteIds( EmptyRequest $request ) {
		$success = Delegate::whereIn( 'id', $request->input( 'ids' ) )->delete();
        $matrix  = new Matrix();
        $matrix->reshuffle('all');

		return response()->json( [
			'success' => $success
		] );
	}

	/**
	 * Delete delegate by id
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete( $id ) {
		/** @var Delegate $delegate */
		$delegate = Delegate::find( $id );

		if ( ! $delegate ) {
			abort( 404 );
		}

		$delegate->delete();
        $matrix = new Matrix();
        $matrix->reshuffle('all');

		return redirect()->back();
	}

	/**
	 * API delete delegate
	 *
	 * @param Delegate $delegate
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function remove( Delegate $delegate ) {
		$success = false;
		if ( $delegate ) {
			$success = $delegate->delete();
            $matrix  = new Matrix();
            $matrix->reshuffle('all');
		}

		return response()->json(
			[
				'success' => $success,
			]
		);
	}

	/**
	 * Retrieve blank csv
	 */
	public function getBlankCsv() {
		/** @var User $self */
		$self = Auth::user();
		/** @var Conference $conference */
		$conference = $self->conference;

		if ( ! $conference ) {
			$conference = $self->conference()->first();
		}
		$name = 'Delegates';
		if ( $conference->name ) {
			$footer = " - delegates";
			$name   = substr( $conference->name, 0, 31 - strlen( $footer ) ) . $footer;
		}

		$columns = ( new Delegate() )->getFillableColumns();

		Excel::create( $name, function ( $excel ) use ( $name, $columns ) {
			$excel->sheet( $name, function ( $sheet ) use ( $columns ) {
				$sheet->fromArray( $columns, null, 'A1', true );
			} );
		} )->export( 'xls' );
	}

	/**
	 * Parse CSV with delegates
	 *
	 * @param UploadDelegateCsvRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function uploadCsv( UploadDelegateCsvRequest $request ) {
		$file         = $request->file( 'file' );
		$conferenceId = $this->getActiveConferenceId();

		$rows = $this->parseFileExcel( $file );

		/** @var MessageBag $errorsBag */
		$errorsBag = $this->validateCsv( $rows, $conferenceId );

		if ( count( $errorsBag ) > 0 ) {
			$errorsArray = $errorsBag->toArray();
			$errors      = [];

			foreach ( $errorsArray as $subArr ) {
				foreach ( $subArr as $v ) {
					$errors[] = $v;
				}
			}

			sort( $errors );

			return redirect()->route( 'admin.delegate.upload-csv-page' )->with( 'errors', $errors );
		}

		try {
			$this->saveRows( $rows, $conferenceId );
		} catch ( QueryException $e ) {
			\Log::info( 'DelegateController::uploadCsv::QueryException', [ $e->getMessage() ] );

			return redirect()->route( 'admin.delegate.upload-csv-page' )->with( 'errors', [ 'fatal' => 'Error during database insert' ] );
		} catch ( \Exception $e ) {
			\Log::info( 'DelegateController::uploadCsv::Exception', [ $e->getMessage() ] );

			return redirect()->route( 'admin.delegate.upload-csv-page' )->with( 'errors', [ 'fatal' => 'Something went wrong' ] );
		}


		return redirect()->route( 'admin.delegate.get' );
	}

	/**
	 * @param $rows
	 * @param $conferenceId
	 *
	 * @return MessageBag
	 */
	private function validateCsv( $rows, $conferenceId ) {
		$errors = new MessageBag();

		$messages = [
			'required' => 'The :attribute field is required.',
		];

		foreach ( $rows as $key => $val ) {
			$messages["$key.email.email"]       = ( $key + 1 ) . " <strong>email</strong> is not a valid email";
			$messages["$key.email.unique"]      = ( $key + 1 ) . " <strong>email</strong> has already been taken";
			$messages["$key.age_group.numeric"] = ( $key + 1 ) . " <strong>age_group</strong> is not numeric";
			$messages["$key.uic.unique"]        = ( $key + 1 ) . " <strong>uic</strong> has already been taken";
		}

		try {
			$validator = Validator::make( $rows, [
				'*.email'     => [
//					'required',
					'nullable',
					'email',
					Rule::unique( 'delegates' )->where( function ( $query ) use ( $conferenceId ) {
						$query->where( 'conference_id', $conferenceId );
					} )
				],
				'*.age_group' => 'nullable|numeric',
				'*.uic'       => [
					Rule::unique( 'delegates' )->where( function ( $query ) use ( $conferenceId ) {
						$query->where( 'conference_id', $conferenceId );
					} )
				],
			], $messages );

			if ( $validator->fails() ) {
				$errors = $validator->messages();
			} else {
				$uicCount = 0;
				$uicList  = [];
				foreach ( $rows as $key => $row ) {
					if ( ! isset( $row['uic'] ) || empty( $row['uic'] ) ) {
						continue;
					}
					$uicCount ++;
					if ( in_array( $row['uic'], $uicList, true ) ) {
						continue;
					}
					$uicList[] = $row['uic'];
				}
				if ( $uicCount != count( $uicList ) ) {
					$errors->add( 'uic_duplicate', 'Duplicate UIC in file' );
				}
			}
		} catch ( \Exception $e ) {
			$errors->add( 'Fatal error', $e->getMessage() );
		}

		return $errors;
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function uploadCsvPage() {
		return view( 'admin.delegate.upload-page' );
	}

	/**
	 * @param UploadedFile $file
	 *
	 * @return array
	 */
	private function parseFileExcel( $file ) {
		$path = $file->getRealPath();


		$excel = Excel::load( $path )->get();
		/** @var RowCollection $rows */
		$rows = $excel->map( function ( $line ) {
			/** @var CellCollection $line */
			return array_filter( $line->toArray() );
		} );

		$rows = $rows->toArray();

		/** @var array $rows */
		if ( isset( $rows[''] ) ) {
			unset( $rows[''] );
		}

		if ( count( $rows ) == 2 && empty( $rows[1] ) ) {
			return array_shift( $rows );
		}

		return $rows;
	}

	/**
	 * @param $rows
	 * @param $conferenceId
	 */
	private function saveRows( $rows, $conferenceId ) {
		$fillable = ( new Delegate() )->getFillableColumns();

		foreach ( $rows as $row ) {
			$delegate = new Delegate();

			$row = $this->filterDelegate( $row, true );

			foreach ( $fillable as $f ) {
				if ( ! isset( $row[ $f ] ) ) {
					continue;
				}
				$delegate->$f = $row[ $f ];
			}

			$delegate->uic           = ! empty( $row['uic'] ) ? $row['uic'] : $this->getUniqueUIC( $conferenceId );
			$delegate->email         = $row['email'];
			$delegate->password      = Hash::make( $row['password'] );
			$delegate->conference_id = $conferenceId;


			$delegate->save();
		}
	}

	/**
	 * Get UIC
	 *
	 * @param $conferenceId
	 *
	 * @return string
	 */
	private function getUniqueUIC( $conferenceId ) {
		$random_string = $this->generateRandomString() . rand( 1000, 9999 );
		if ( Delegate::where( 'uic', $random_string )->where( 'conference_id', $conferenceId )->first() ) {
			\Log::info( 'DelegateController::getUniqueUIC::MiracleDuplicatedRandomString', [ $random_string ] );
			$random_string = $this->getUniqueUIC( $conferenceId );
		}

		return $random_string;
	}

	/**
	 * @param int $length
	 *
	 * @return string
	 */
	private function generateRandomString( $length = 3 ) {
		$characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen( $characters );
		$randomString     = '';
		for ( $i = 0; $i < $length; $i ++ ) {
			$randomString .= $characters[ rand( 0, $charactersLength - 1 ) ];
		}

		return $randomString;
	}

	/**
	 * @param $data
	 * @param bool $fillRequired
	 *
	 * @return array
	 */
	private function filterDelegate( $data, $fillRequired = false ) {
		if ( $fillRequired ) {
			$required = [
				'first_name' => 'Empty',
				'last_name'  => 'Empty',
				'country'    => 'Not Selected',
				'password'   => 'password'
			];
			foreach ( $required as $field => $value ) {
				if ( ! isset( $data[ $field ] ) || empty( $data[ $field ] ) ) {
					$data[ $field ] = $value;
				}
			}
		}
		$data = array_filter(
			$data,
			function ( $el ) {
				return ! is_null( $el );
			}
		);

		return $data;
	}

	/**
	 * Export all delegates as CSV
	 * @return bool|mixed
	 */
	public function export() {
		$conferenceId = $this->getActiveConferenceId();

		$list = Delegate::with( [ 'meta' ] )->where( 'conference_id', $conferenceId )->get()->toArray();

		foreach ( $list as $key => $item ) {
			$list[ $key ]['meta'] = \GuzzleHttp\json_encode( $item['meta'] );
		}

		return $this->exportCSV( $list, 'delegates' );
	}

	/**
	 * Export all delegates as archive with media and CSV
	 * @return bool|mixed
	 */
	public function exportMedia() {
		$user       = Auth::user();
		$conference = $user->conference;

		$delegates = Delegate::with( [ 'meta' ] )->where( 'conference_id', $conference->id )->get();

		$list = $delegates->toArray();

		foreach ( $list as $key => $item ) {
			$list[ $key ]['meta'] = \GuzzleHttp\json_encode( $item['meta'] );
		}

		$files = [];

		foreach ( $delegates as $delegate ) {
			if ( ! $delegate->photo ) {
				continue;
			}
			$asset    = ConferenceAsset::find( $delegate->photo );
			$fileName = $delegate->uic;
			if ( ! $fileName ) {
				$fileName = Str::slug( $delegate->first_name . ' ' . $delegate->last_name . ' ' . $delegate->id, '_' );
			}
			$filePath = $asset->path;
			if(!is_file($filePath)) {
				$filePath = str_replace( '//', '/', public_path( $asset->path ) );
			}
			$files[ $fileName ] = [
				'path' => $filePath,
				'type' => File::extension( $filePath )
			];
		}

		$tempPath    = storage_path( 'app/public/cms/' . $conference->url_slug . '/tmp' );
		$zipFileName = 'delegates.zip';
		$csvFileName = 'delegates.csv';
		$zip         = new ZipArchive;

		if ( ! is_dir( $tempPath ) ) {
			mkdir( $tempPath, 0777 );
		}

		if(is_file($tempPath . '/' . $zipFileName)) {
			if ( $zip->open( $tempPath . '/' . $zipFileName, ZipArchive::OVERWRITE ) !== true ) {
				return response( [ 'success' => false ], 403 );
			}
		} else {
			if ( $zip->open( $tempPath . '/' . $zipFileName, ZipArchive::CREATE ) !== true ) {
				return response( [ 'success' => false ], 403 );
			}
		}

		$zip->addEmptyDir( 'photos' );

		foreach ( $files as $fileName => $file ) {
			if ( is_file( $file['path'] ) ) {
				$zip->addFile( $file['path'], 'photos/' . $fileName . '.' . $file['type'] );
			}
		}

		if ( $this->saveCSV( $list, $tempPath . '/' . $csvFileName ) ) {
			$zip->addFile( $tempPath . '/' . $csvFileName, $csvFileName );
		}

		$zip->close();

		$headers    = array(
			'Content-Type' => 'application/octet-stream',
		);
		$filetopath = $tempPath . '/' . $zipFileName;

		if ( file_exists( $filetopath ) ) {
			return response()->download( $filetopath, $zipFileName, $headers );
		}

		return [ 'status' => 'file does not exist' ];
	}

	/**
	 * Send welcome notifications to delegates
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function notifyWelcome( Request $request ) {
		$delegateIds = $request->get( 'delegates' );
		if ( $delegateIds === '*' ) {
			$delegates = Delegate::where( 'enabled', 1 )->where( 'confirmed', 0 )->get();
		} else {
			$delegates = Delegate::find( $delegateIds );
		}
		if ( $delegates ) {
			foreach ( $delegates as $delegate ) {
				if ( ! $delegate->email ) {
					continue;
				}
				\Mail::to( $delegate->email )->send( new SendDelegateNotification( $delegate ) );
			}
		}

		return response()->json( [
			'success' => true,
			'data'    => $request->get( 'delegates' )
		] );
	}

	/**
	 * Send E Ticket notifications to delegates
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function notifyEticket( Request $request ) {

		$delegateIds = $request->get( 'delegates' );
		if ( $delegateIds === '*' ) {
			$delegates = Delegate::where( 'confirmed', 1 )->get();
		} else {
			$delegates = Delegate::find( $delegateIds );
		}
		if ( $delegates ) {
			foreach ( $delegates as $delegate ) {
				if ( ! $delegate->email ) {
					continue;
				}
				\Mail::to( $delegate->email )->send( new NewDelegateNotification( $delegate, 'delegate' ) );
			}
		}

		return response()->json( [
			'success' => true,
			'data'    => $request->get( 'delegates' )
		] );
	}

	/**
	 * Send Blank notifications to delegates
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function notifyBlank( Request $request ) {

		$delegateIds = $request->get( 'delegates' );
		if ( $delegateIds === '*' ) {
			$delegates = Delegate::where( 'confirmed', 1 )->get();
		} else {
			$delegates = Delegate::find( $delegateIds );
		}
		if ( $delegates ) {
			foreach ( $delegates as $delegate ) {
				if ( ! $delegate->email ) {
					continue;
				}
				\Mail::to( $delegate->email )->send( new DelegateBlankNotification( $delegate, 'delegate' ) );
			}
		}

		return response()->json( [
			'success' => true,
			'data'    => $request->get( 'delegates' )
		] );
	}

	/**
	 * Mass appointment assignment
	 *
	 * @param MassAppointmentRequest $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function massAppointment( MassAppointmentRequest $request ) {
		$data = $request->all();

		$delegates = Delegate::find( $data['ids'] );

		foreach ( $delegates as $delegate ) {
			$appointment = $delegate->personalAppointments()->create( [
				'day_id'       => $data['day'],
				'title'        => $data['title'],
				'comment'      => $data['comment'],
				'time_from'    => $data['time_from'],
				'time_to'      => $data['time_to'],
				'custom_color' => $data['custom_color'] ?? '',
			] );
			if ( ! ( $data['available'] ?? true ) ) {
				$delegate->unavailableTimslots()->create( [
					'timeslot_id' => $appointment->id,
					'type'        => 'appointments'
				] );
			}
		}

		return response()->json( [ 'success' => true ] );
	}
}
