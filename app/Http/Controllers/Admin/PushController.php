<?php namespace App\Http\Controllers\Admin;

use App\Conference;
use App\Content;
use App\Day;
use App\Page;
use App\PageAsset;
use App\Http\Controllers\Controller;
use App\ScheduleTimeslot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

/**
 * Class PushController
 * @package App\Http\Controllers
 */
class PushController extends BaseController {

    public function sendPush(){

        $devices = DB::table('mobile_device')->get();

        return view('admin.push.index', [
            'devices' => $devices
        ]);
    }

    public function sendPushPost(Request $r){

        ini_set('memory_limit', '-1');
        ini_set('max_input_time', '400000');
        ini_set('max_execution_time', '400000');

        $notification = ['body' => $r->input('content')];

        DB::table('mobile_device')->orderBy('id','DESC')->chunk(90, function($devices) use($notification){

            $expo = \ExponentPhpSDK\Expo::normalSetup();

            $dlist = [];

            foreach($devices as $d){
                $dlist[]=$d->device;
            }
            $expo->notify($dlist, $notification);

        });

    }
}
