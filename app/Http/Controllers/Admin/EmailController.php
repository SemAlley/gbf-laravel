<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class EmailController extends BaseController {

	/**
	 * HotelController constructor.
	 */
	public function __construct() {
		$this->middleware( 'auth' );
	}

	public function globalEmail() {
		$content = Content::GetContentVue( 'email-global', Auth::user()->conference );

		return view( 'admin.email.global', [
			'content' => $content
		] );
	}

	public function globalEmailSave( Request $r ) {
		$content = [
			'header' => $r->input( 'header' ),
			'footer' => $r->input( 'footer' ),
		];

		$save = Content::SaveContent( 'email-global', $content );

		return response()->json(
			[
				'success' => true,
				'updated' => $save,
			]
		);
	}

	public function editPage() {
		$welcome = Content::GetContentVue( 'email-welcome', Auth::user()->conference );
		$eticket = Content::GetContentVue( 'email-eticket', Auth::user()->conference );
		$blank = Content::GetContentVue( 'email-blank', Auth::user()->conference );
		return view( 'admin.email.edit', [
			'welcome' => $welcome,
			'eticket' => $eticket,
			'blank' => $blank,
		] );
	}

	public function edit(Request $r) {
		$save = Content::SaveContent( 'email-welcome', [
			'content' => $r->input( 'welcome_text' ),
			'subject' => $r->input( 'welcome_subject' ),
		] );
		$save = Content::SaveContent( 'email-eticket', [
			'content' => $r->input( 'eticket_text' ),
			'subject' => $r->input( 'eticket_subject' ),
		] );
		$save = Content::SaveContent( 'email-blank', [
			'content' => $r->input( 'blank_text' ),
			'subject' => $r->input( 'blank_subject' ),
		] );

		return response()->json(
			[
				'success' => true,
				'updated' => $save,
			]
		);
	}

	public function send() {
		return view( 'admin.email.send' );
	}

}

