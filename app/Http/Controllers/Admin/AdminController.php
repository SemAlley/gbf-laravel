<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers\Admin;

use App\Delegate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class AdminController extends Controller {
	public function __construct() {
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @param int $id
	 *
	 * @return \Response
	 */
	public function switchConference( $id ) {
		Auth::user()->switchConference( $id );

		return back();
	}

	public function index() {
		return view( 'admin.home' );
	}

	public function delegateView() {
		$admin         = Auth::user();
		$delegateAdmin = $admin->conference->delegates()->where( 'email', $admin->email )->first();

		if ( ! $delegateAdmin ) {
			$delegateAdmin                = new Delegate();
			$delegateAdmin->email         = $admin->email;
			$delegateAdmin->password      = $admin->password;
			$delegateAdmin->conference_id = $admin->conference->id;
			$delegateAdmin->first_name    = $admin->name;
			$delegateAdmin->last_name     = $admin->name;
			$delegateAdmin->country       = 'required';
			$delegateAdmin->enabled       = true;
			$delegateAdmin->confirmed     = false;
			$delegateAdmin->save();
		}

		Auth::guard('delegate')->loginUsingId($delegateAdmin->id);

		return redirect()->route('frontend.dashboard', ['conference' => $admin->conference->url_slug]);
	}
}
