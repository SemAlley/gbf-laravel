<?php namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Helpers\CSVExporter;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ContactController extends BaseController
{

    use CSVExporter;


    /**
     * Display list of submitted contacts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $columns = [
            'id',
            'first_name',
            'last_name',
            'email',
            'nature_of_enquiry',
            'job_title',
            'company',
            'industry',
            'countries_operate',
            'country',
            'website',
            'comments',
            'created_at',
        ];
        $values = Contact::select($columns)->where('conference_id', $this->getActiveConferenceId())->get();

        return view('admin.contact.index', ['values' => $values]);
    }


    /**
     * Export data as CSV
     *
     * @return bool|mixed
     */
    public function export()
    {
	    $conferenceId = $this->getActiveConferenceId();
        $list = Contact::where('conference_id', $conferenceId)->get()->toArray();

        return $this->exportCSV($list, 'contacts');
    }

}
