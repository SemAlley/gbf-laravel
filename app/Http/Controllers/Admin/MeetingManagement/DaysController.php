<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Day;

class DaysController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Day $day)
    {
        $conference = Auth::user()->conference;
        $conferenceDays = $day->getAllConferenceDays($conference->id);

        return view( 'admin.meeting_management.days.index', ['conference'=>$conference, 'days'=>$conferenceDays]);
    }


    public function store(Request $request)
    {
        $day = new Day($request->all());
        $day->save();

        return response()->json(['day'=>$day]);
    }


    public function getDaysAjax(Day $day)
    {
        return response()->json($day->getAllConferenceDays(Auth::user()->conference->id));
    }


    public function update(Day $day)
    {
        $day->update(request()->all());

        return response()->json(['day'=>$day]);
    }

    public function delete(Day $day)
    {
        $day->delete();

        return response()->json(['success'=>true]);
    }
}
