<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use App\MeetingRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\CSVExporter;
use Illuminate\Support\Facades\Auth;
use App\Day;

class DelegateController extends Controller
{

    use CSVExporter;

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $conference = Auth::user()->conference;

        return view('admin.meeting_management.delegates.index', ['conference' => $conference]);
    }

    public function getRequestsAjax()
    {

        $delegates_requests = MeetingRequest::with(['delegate', 'target', 'meetingTimeslot'])->get();

        $delegates_requests = $delegates_requests->filter(function ($value) {
            return $value->delegate->conference_id == Auth::user()->conference->id;
        })->values();

        $result = [];

        foreach ($delegates_requests as $d) {
            $result[] = [
                'id'               => $d->id,
                'status'           => $d->status,
                'delegate'         => $d->delegate->first_name . " " . $d->delegate->last_name . " | " . $d->delegate->email,
                'target'           => $d->target->first_name . " " . $d->target->last_name . " | " . $d->target->email,
                'meeting_timeslot' => $d->meetingTimeslot->title . " | " . $d->meetingTimeslot->time_from . " - " . $d->meetingTimeslot->time_to,
                'updated_at'       => $d->updated_at->format("d-m-Y H:ia"),
            ];
        }

        return response()->json($result);
    }

    public function export()
    {

        $delegates_requests = MeetingRequest::with(['delegate', 'target', 'meetingTimeslot'])->get();

        $delegates_requests = $delegates_requests->filter(function ($value) {
            return $value->delegate->conference_id == Auth::user()->conference->id;
        })->values();

        $result = [];

        foreach ($delegates_requests as $d) {
            $result[] = [
                'id'                  => $d->id,
                'status'              => $d->status,
                'requesting_delegate' => $d->delegate->first_name . " " . $d->delegate->last_name . " | " . $d->delegate->email,
                'target_delegate'     => $d->target->first_name . " " . $d->target->last_name . " | " . $d->target->email,
                'timeslot'            => $d->meetingTimeslot->title . " | " . $d->meetingTimeslot->time_from . " - " . $d->meetingTimeslot->time_to,
                'last_updated'        => $d->updated_at,
            ];
        }

        return $this->exportCSV($result, 'delegates');
    }
}
