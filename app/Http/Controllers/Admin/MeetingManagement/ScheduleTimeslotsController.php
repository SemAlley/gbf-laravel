<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use App\Day;
use App\ScheduleTimeslot;
use App\Speaker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ScheduleTimeslotsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $conference = \Auth::user()->conference;
        $days = Day::where('conference_id', $conference->id)->with(['scheduleTimeslots','scheduleTimeslots.speakers'])->get();
        $speakers = Speaker::where('conference_id', $conference->id)->get();

        return view( 'admin.meeting_management.schedule_timeslots.index', [
            'conference'=>$conference,
            'days'=>$days,
            'speakers' => $speakers
        ]);
    }


    public function store()
    {
        $scheduleTimeslot = new ScheduleTimeslot(request()->all());
        $scheduleTimeslot->save();

        $scheduleTimeslot->speakers()->attach(request()->speakers);

        return response()->json($scheduleTimeslot);
    }


    public function getScheduleTimeslotsAjax()
    {
        $conference = \Auth::user()->conference;
        $days = Day::where('conference_id', $conference->id)->with(['scheduleTimeslots','scheduleTimeslots.speakers'])->get();

        return response()->json($days);
    }


    public function update(ScheduleTimeslot $scheduleTimeslot)
    {
        $scheduleTimeslot->update(request()->all());

        $scheduleTimeslot->speakers()->sync(request()->speakers);

        return response()->json($scheduleTimeslot);
    }


    public function delete(ScheduleTimeslot $scheduleTimeslot)
    {
        $scheduleTimeslot->speakers()->detach();

        $scheduleTimeslot->delete();

        return response()->json(['success' => true]);
    }

}
