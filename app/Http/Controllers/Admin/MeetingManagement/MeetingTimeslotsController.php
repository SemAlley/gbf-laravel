<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use App\Day;
use App\MeetingTimeslot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MeetingTimeslotsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $conference = \Auth::user()->conference;
        $days = Day::where('conference_id', $conference->id)->with(['meetingTimeslots'])->get();

        return view( 'admin.meeting_management.meeting_timeslots.index', [
            'conference'=>$conference,
            'days'=>$days,
        ]);
    }


    public function getMeetingTimeslotsAjax()
    {
        $conference = \Auth::user()->conference;
        $days = Day::where('conference_id', $conference->id)->with(['meetingTimeslots'])->get();

        return response()->json($days);
    }


    public function store()
    {
        $meetingTimeslot = new MeetingTimeslot(request()->all());
        $meetingTimeslot->save();

        return response()->json($meetingTimeslot);
    }


    public function update(MeetingTimeslot $meetingTimeslot)
    {
        $meetingTimeslot->update(request()->all());

        return response()->json($meetingTimeslot);
    }


    public function delete(MeetingTimeslot $meetingTimeslot)
    {
        $meetingTimeslot->delete();

        return response()->json(['success'=>true]);
    }

}
