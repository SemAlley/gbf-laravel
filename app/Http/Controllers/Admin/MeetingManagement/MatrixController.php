<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use App\Corporate;
use App\Day;
use App\Delegate;
use App\Matrix;
use App\MeetingTimeslot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MatrixController extends Controller
{
    public function index()
    {
        $conference = \Auth::user()->conference;
        $matrixData = $this->prepareMatrix(new Matrix);

        return view('admin.meeting_management.matrix.index', ['matrix'=>collect($matrixData), 'conference' => $conference]);
    }

    public function getAgenda(Request $request)
    {
        $conference = \Auth::user()->conference;
        $days = Day::with('meetingTimeslots')->where('conference_id', $conference->id)->get();
        $response = [];
        if ($request->get('type') == 'corp') {
            $setMeetings = Matrix::where([['corporate_id', $request->get('id')],['status','set']])->with('investor')->get();
            foreach ($days as $day) {
                $response[$day->id]['day'] = $day;
                foreach ($day->meetingTimeslots as $meetingTimeslot) {
                    if ($setMeetings->whereIn('timeslot_id', $meetingTimeslot->id)->count()) {
                        $response[$day->id]['timeslots'][$meetingTimeslot->id]['status'] = 'busy';
                        $guests = $setMeetings->map(function ($item, $key) use ($meetingTimeslot, $request){
                            if ($item->timeslot_id == $meetingTimeslot->id && $item->corporate_id == $request->get('id'))
                                return $item->investor;
                        });
                        $response[$day->id]['timeslots'][$meetingTimeslot->id]['guests'] = $guests;
                    } else {
                        $response[$day->id]['timeslots'][$meetingTimeslot->id] = 'free';
                    }
                }
            }
        } else {
            $setMeetings = Matrix::where([['delegate_id', $request->get('id')],['status','set']])->get();
            foreach ($days as $day) {
                $response[$day->id]['day'] = $day;
                foreach ($day->meetingTimeslots as $meetingTimeslot) {
                    if ($setMeetings->whereIn('timeslot_id', $meetingTimeslot->id)->count()) {
                        $response[$day->id]['timeslots'][$meetingTimeslot->id] = 'busy';
                    } else {
                        $response[$day->id]['timeslots'][$meetingTimeslot->id] = 'free';
                    }
                }
            }
        }

        return response()->json(['agenda'=>$response]);
    }

    public function reshuffle(Request $request)
    {
        $level = $request->get('shuffle');
        $matrix = new Matrix();
        $matrix->reshuffle($level);
        $data = $this->prepareMatrix(new Matrix);
        return response()->json(['matrix'=>$data]);
    }

    private function prepareMatrix(Matrix $matrix)
    {
        $days = Day::all();
        $dataRaw = $matrix->get()->groupBy('timeslot_id');
        $timeSlots = MeetingTimeslot::all();
        $corporates = Corporate::all();
        $investors = Delegate::where('rank', '<>', null)->get();
        $matrixData = [];

        foreach ($dataRaw as $key => $data) {
            $matrixRow = [
                'timeslotId' => $key,
                'day' => $days->where('id', $data->first()->day_id)->first()->name,
                'time' => $timeSlots->where('id', $key)->first()->time_from . ' - ' . $timeSlots->where('id', $key)->first()->time_to,
                'corporate' => [],
                'investor' => [],
                'status' => [],
                'action' => []
            ];

            foreach ($data as $guestsInfo) {
//                dd($guestsInfo);
                $matrixRow['corporate'][$guestsInfo->corporate_id] = ['id' => $guestsInfo->corporate_id, 'title' => $corporates->where('id', $guestsInfo->corporate_id)->first()->title];
                $matrixRow['investor'][$guestsInfo->corporate_id][] = ['id' => $guestsInfo->delegate_id, 'val' => $investors->where('id', $guestsInfo->delegate_id)->first()->first_name . ' ' . $investors->where('id', $guestsInfo->delegate_id)->first()->last_name . ' (' . $investors->where('id', $guestsInfo->delegate_id)->first()->rank . ')'];
                $matrixRow['status'][$guestsInfo->corporate_id][] = $guestsInfo->status;
                switch ($guestsInfo->status) {
                    case 'set':
                        $matrixRow['action'][$guestsInfo->corporate_id][] = ['invId' => $guestsInfo->delegate_id, 'action' => 'unset'];
                        break;
                    case 'unset':
                        $matrixRow['action'][$guestsInfo->corporate_id][] = ['invId' => $guestsInfo->delegate_id, 'action' => 'set'];
                        break;
                    case 'forced':
                        $matrixRow['action'][$guestsInfo->corporate_id][] = ['invId' => $guestsInfo->delegate_id, 'action' => 'delete'];
                        break;
                }
            }

            $matrixData[] = $matrixRow;
        }

        return $matrixData;

    }

    public function changeSetStatus(Request $request)
    {
        Matrix::where([['timeslot_id', '=', $request->get('timeslot')], ['delegate_id', '=', $request->get('investor')], ['corporate_id', '=', $request->get('corporate')]])->update(['status' => $request->get('status')]);
        $matrixData = $this->prepareMatrix(new Matrix);

        return response()->json(['matrix'=>$matrixData]);

    }
}
