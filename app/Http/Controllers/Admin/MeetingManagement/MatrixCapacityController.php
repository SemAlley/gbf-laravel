<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use App\RankCapacity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MatrixCapacityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(RankCapacity $capacity)
    {
        $conference = Auth::user()->conference;
        $data = $capacity->select('id', 'rank', 'capacity')->get();

        return view( 'admin.meeting_management.capacity.index', ['conference'=>$conference, 'data'=>$data]);
    }

    public function update(RankCapacity $capacity)
    {
        foreach (request()->get('data') as $data) {
            $capacity->where('id', $data['id'])->update(['capacity' => $data['capacity']]);
        }

        return response()->json(['data'=>$capacity]);
    }
}
