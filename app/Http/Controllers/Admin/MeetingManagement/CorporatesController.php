<?php

namespace App\Http\Controllers\Admin\MeetingManagement;

use App\Corporate;
use App\Day;
use App\Delegate;
use App\Matrix;
use App\MeetingTimeslot;
use App\ScheduleTimeslot;
use App\Speaker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CorporatesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Corporate $corporate, MeetingTimeslot $slots)
    {
        $conference = \Auth::user()->conference;
        $corporates = $corporate->with('delegate')->get();
        $delegates = Delegate::where([['conference_id', '=', $conference->id], ['rank', '<>', null]])->get();
        $byDay = $slots->with(['day','corporates'])->get();
        $grouped = $byDay->groupBy('day_id', true)->toJson();

        return view('admin.meeting_management.corporates.index', ['corporates'=>$corporates, 'delegates' => $delegates, 'meetings' => $byDay, 'grouped' => $grouped]);
    }

    public function save()
    {
        $data = request()->all();
        $data['delegate_id'] = $data['delegate_id']['value'];
        $corporate = new Corporate($data);
        $corporate->save();
        $prepared_slots = $this->prepareRelation($data['slots'], $corporate->id);
        $corporate->meetingTimeslots()->sync($prepared_slots);
        $matrix = new Matrix();
        $matrix->reshuffle('all');

        return response()->json(['corporate'=>$corporate]);
    }

    public function getCorporatesAjax(Corporate $corporate, MeetingTimeslot $slots)
    {
        $conference = \Auth::user()->conference;
        return response()->json(['data' => $corporate->with('delegate')->get(), 'slots' => $slots->with(['day','corporates'])->get()]);
    }


    public function update(Corporate $corporate)
    {
        $data = request()->all();
        $data['delegate_id'] = $data['delegate_id']['value'];
        $prepared_slots = $this->prepareRelation($data['slots'], request()->get('id'));
        $corporate->update($data);
        $corporate->meetingTimeslots()->sync($prepared_slots);
        $matrix = new Matrix();
        $matrix->reshuffle('all');

        return response()->json(['corporate'=>$corporate]);
    }

    public function delete(Corporate $corporate)
    {
        $corporate->meetingTimeslots()->detach();

        $corporate->delete();
        $matrix = new Matrix();
        $matrix->reshuffle('all');

        return response()->json(['success'=>true]);
    }

    private function prepareRelation($timeSlots, $corporateId)
    {
        $prepared = [];
        foreach ($timeSlots as $slot) {
            if ($slot['value'] != false) {
                $prepared[$slot['id']] = ['corporate_id' => $corporateId];
            } else {
                continue;
            }
        }
        return $prepared;
    }

}