<?php

namespace App\Http\Middleware;

use App\Conference;
use Closure;
use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

class EncryptCookies extends BaseEncrypter {

	/**
	 * The names of the cookies that should not be encrypted.
	 *
	 * @var array
	 */
	protected $except = [//
	];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {

		$currentConference = $request->segment( 1 );
		$conference        = Conference::where( 'url_slug', $currentConference )->first();
		if($conference) {
			config(['session.cookie' => 'gbf_session_' . $conference->id]);
		}
		return parent::handle( $request, $next );
	}
}
