<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DelegateMeta extends Model
{
	protected $table = 'delegates_meta';

	protected $fillable = [
		'meta_key',
		'meta_value'
	];

	/**
	 * Get the delegate that owns the meta.
	 */
	public function delegate()
	{
		return $this->belongsTo('App\Delegate');
	}
}
