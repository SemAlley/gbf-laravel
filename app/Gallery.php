<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	public function preview()
	{
		return $this->belongsTo('App\ConferenceAsset', 'preview', 'id');
	}

	public function assets()
	{
		return $this->belongsToMany('App\ConferenceAsset', 'galleries_conference_assets');
	}
}
