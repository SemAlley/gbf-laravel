<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleTimeslot extends Model
{

    protected $fillable = ['day_id', 'title', 'time_from', 'time_to', 'type', 'description'];


    public function day ()
    {
        return $this->belongsTo(Day::class);
    }


    public function speakers()
    {
        return $this->belongsToMany(Speaker::class, 'scheduletimeslots_speakers');
    }


}
