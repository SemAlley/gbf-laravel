<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    protected $table = 'matrix_prime';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function conference()
    {
        return $this->belongsTo(Conference::class);
    }

    public function corporate()
    {
        return $this->hasOne(Corporate::class);
    }

    public function investor()
    {
        return $this->hasOne(Delegate::class);
    }

    public function timeslot()
    {
        return $this->hasOne(MeetingTimeslot::class);
    }

    public function day()
    {
        return $this->belongsTo(Day::class);
    }

    public function reshuffle($level = 'all')
    {
        $goldLimit = RankCapacity::where('rank', 'gold')->value('capacity');
        $silverLimit = RankCapacity::where('rank', 'silver')->value('capacity');
        $bronzeLimit = RankCapacity::where('rank', 'bronze')->value('capacity');

        switch ($level) {
            case 'all':
                $solid = $this->where([['status', '!=', 'unset'], ['status', '!=', 'null']])->get();
                $this->where('status', 'unset')->delete();
                $matrix = [];
                $corporates = Corporate::with(['meetingTimeslots'])->get();
                $investors = Delegate::where([['rank', '<>', null]])->with(['unavailableTimslots'=> function($query){
                    $query->where('type', '=', 'meetings');
                }])->get();
                $meetingTimeslots = MeetingTimeslot::get();
                $conference = \Auth::user()->conference;
                foreach ($meetingTimeslots as $meetingTimeslot) {
                    foreach ($corporates as $corporate) {
                        $investorPool = [];
                        $used_gold = [];
                        $used_silver = [];
                        $used_bronze = [];
                        $passed = $corporate->meetingTimeslots->whereIn('id', $meetingTimeslot->id);
                        if (!$passed->isEmpty()) {
                            $day_id = $meetingTimeslot->day_id;
                            foreach ($investors as $investor) {
                                if (!$investor->unavailableTimslots->whereIn('timeslot_id', $meetingTimeslot->id)->count()) {
                                    $investorPool[] = $investor->id;
                                }
                            }

                            $solidIds       = $solid->where('timeslot_id', $meetingTimeslot->id)->pluck('delegate_id');
                            $solidGold      = $investors->whereIn('id', $solid->where('timeslot_id', $meetingTimeslot->id)->where('corporate_id', $corporate->id)->pluck('delegate_id'))->where('rank', 'gold')->count();
                            $solidSilver    = $investors->whereIn('id', $solid->where('timeslot_id', $meetingTimeslot->id)->where('corporate_id', $corporate->id)->pluck('delegate_id'))->where('rank', 'silver')->count();
                            $solidBronze    = $investors->whereIn('id', $solid->where('timeslot_id', $meetingTimeslot->id)->where('corporate_id', $corporate->id)->pluck('delegate_id'))->where('rank', 'bronze')->count();
                            $passedInv      = $investors->whereIn('id', $investorPool)->whereNotIn('id', $solidIds);
                            $grouped        = $passedInv->groupBy('rank')->all();
                            $currentGoldLimit   = $silverLimit - $solidGold;
                            $currentSilverLimit = $silverLimit - $solidSilver;
                            $currentBronzeLimit = $silverLimit - $solidBronze;
                            $valid_gold     = isset($grouped['gold']) ? $grouped['gold']->whereNotIn('id', $used_gold) : collect([]);
                            $valid_silver   = isset($grouped['silver']) ? $grouped['silver']->whereNotIn('id', $used_silver) : collect([]);
                            $valid_bronze   = isset($grouped['bronze']) ? $grouped['bronze']->whereNotIn('id', $used_bronze) : collect([]);

                            if ( $valid_gold->count() >= $currentGoldLimit) {
                                $goldArr = $valid_gold->random($currentGoldLimit);
                                foreach ($goldArr as $gold) {
                                    $used_gold[] = $gold->id;
                                    $matrix[] = [
                                        'timeslot_id' => $meetingTimeslot->id,
                                        'corporate_id' => $corporate->id,
                                        'status' => 'unset',
                                        'day_id' => $day_id,
                                        'conferences_id' => $conference->id,
                                        'delegate_id' => $gold->id
                                    ];
                                }
                            }

                            if ($valid_silver->count() > $currentSilverLimit)
                                $silverArr = $valid_silver->random($currentSilverLimit);
                            else
                                $silverArr = $valid_silver;

                            foreach ($silverArr as $silver) {
                                $used_silver[] = $silver->id;
                                $matrix[] = [
                                    'timeslot_id' => $meetingTimeslot->id,
                                    'corporate_id' => $corporate->id,
                                    'status' => 'unset',
                                    'day_id' => $day_id,
                                    'conferences_id' => $conference->id,
                                    'delegate_id' => $silver->id
                                ];
                            }

                            if ($valid_bronze->count() > $currentBronzeLimit)
                                $bronzeArr = $valid_bronze->random($currentBronzeLimit);
                            else
                                $bronzeArr = $valid_bronze;
                            foreach ($bronzeArr as $bronze) {
                                $used_bronze[] = $bronze->id;
                                $matrix[] = [
                                    'timeslot_id' => $meetingTimeslot->id,
                                    'corporate_id' => $corporate->id,
                                    'status' => 'unset',
                                    'day_id' => $day_id,
                                    'conferences_id' => $conference->id,
                                    'delegate_id' => $bronze->id
                                ];
                            }
                        }
                    }
                }
                $this->insert($matrix);
                break;
            case "silver":
                $corporates = Corporate::with(['meetingTimeslots'])->get();
                $investors = Delegate::where([['rank', '=', 'silver']])->with(['unavailableTimslots'=> function($query){
                    $query->where('type', '=', 'meetings');
                }])->get();
                $solid = $this->where([['status', '!=', 'unset'], ['status', '!=', 'null']])->get();
                $investorsIds = Delegate::where([['rank', '=', 'silver']])->select('id')->get()->toArray();
                $this->whereIn('delegate_id', $investorsIds)->where('status', 'unset')->delete();
                $matrix = [];
                $meetingTimeslots = MeetingTimeslot::get();
                $conference = \Auth::user()->conference;
                foreach ($corporates as $corporate) {
                    $investorPool = [];
                    foreach ($meetingTimeslots as $meetingTimeslot) {
                        $passed = $corporate->meetingTimeslots->whereIn('id', $meetingTimeslot->id);
                        if (!$passed->isEmpty()) {
                            $day_id = $meetingTimeslot->day_id;
                            foreach ($investors as $investor) {
                                if (!$investor->unavailableTimslots->whereIn('timeslot_id', $meetingTimeslot->id)->count()) {
                                    $investorPool[] = $investor->id;
                                }
                            }

                            $solidIds       = $solid->where('timeslot_id', $meetingTimeslot->id)->pluck('delegate_id');
                            $solidSilver    = $investors->whereIn('id', $solid->where('timeslot_id', $meetingTimeslot->id)->where('corporate_id', $corporate->id)->pluck('delegate_id'))->where('rank', 'silver')->count();
                            $passedInv      = $investors->whereIn('id', $investorPool)->whereNotIn('id', $solidIds);
                            $grouped        = $passedInv->groupBy('rank')->all();
                            $currentSilverLimit = $silverLimit - $solidSilver;

                            if (isset($grouped['silver'])) {
                                if ($grouped['silver']->count() > $currentSilverLimit)
                                    $silverArr = $grouped['silver']->random($currentSilverLimit);
                                else
                                    $silverArr = $grouped['silver'];
                                foreach ($silverArr as $silver) {
                                    $matrix[] = [
                                        'timeslot_id' => $meetingTimeslot->id,
                                        'corporate_id' => $corporate->id,
                                        'status' => 'unset',
                                        'day_id' => $day_id,
                                        'conferences_id' => $conference->id,
                                        'delegate_id' => $silver->id
                                    ];
                                }
                            }
                        }
                    }
                }
                $this->insert($matrix);
                break;
            case "bronze":
                $corporates = Corporate::with(['meetingTimeslots'])->get();
                $investors = Delegate::where([['rank', '=', 'bronze']])->with(['unavailableTimslots'=> function($query){
                    $query->where('type', '=', 'meetings');
                }])->get();
                $solid = $this->where([['status', '!=', 'unset'], ['status', '!=', 'null']])->get();
                $investorsIds = Delegate::where([['rank', '=', 'bronze']])->select('id')->get()->toArray();
                $this->whereIn('delegate_id', $investorsIds)->where('status', 'unset')->delete();
                $matrix = [];
                $meetingTimeslots = MeetingTimeslot::get();
                $conference = \Auth::user()->conference;
                foreach ($corporates as $corporate) {
                    $investorPool = [];
                    foreach ($meetingTimeslots as $meetingTimeslot) {
                        $passed = $corporate->meetingTimeslots->whereIn('id', $meetingTimeslot->id);
                        if (!$passed->isEmpty()) {
                            $day_id = $meetingTimeslot->day_id;
                            foreach ($investors as $investor) {
                                if (!$investor->unavailableTimslots->whereIn('timeslot_id', $meetingTimeslot->id)->count()) {
                                    $investorPool[] = $investor->id;
                                }
                            }

                            $solidIds       = $solid->where('timeslot_id', $meetingTimeslot->id)->pluck('delegate_id');
                            $solidBronze    = $investors->whereIn('id', $solid->where('timeslot_id', $meetingTimeslot->id)->where('corporate_id', $corporate->id)->pluck('delegate_id'))->where('rank', 'bronze')->count();
                            $passedInv      = $investors->whereIn('id', $investorPool)->whereNotIn('id', $solidIds);
                            $grouped        = $passedInv->groupBy('rank')->all();
                            $currentBronzeLimit = $silverLimit - $solidBronze;

                            if (isset($grouped['bronze'])) {
                                if ($grouped['bronze']->count() > $currentBronzeLimit)
                                    $bronzeArr = $grouped['bronze']->random($currentBronzeLimit);
                                else
                                    $bronzeArr = $grouped['bronze'];
                                foreach ($bronzeArr as $bronze) {
                                    $matrix[] = [
                                        'timeslot_id' => $meetingTimeslot->id,
                                        'corporate_id' => $corporate->id,
                                        'status' => 'unset',
                                        'day_id' => $day_id,
                                        'conferences_id' => $conference->id,
                                        'delegate_id' => $bronze->id
                                    ];
                                }
                            }
                        }
                    }
                }
                $this->insert($matrix);
                break;
        }
    }

}
