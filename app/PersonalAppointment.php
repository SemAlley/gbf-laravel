<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalAppointment extends Model
{
	protected $fillable = ['day_id', 'title', 'time_from', 'time_to', 'comment', 'custom_color'];
}
