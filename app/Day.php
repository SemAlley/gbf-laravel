<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{


    protected $fillable = ['conference_id','name','date'];


    public function conference()
    {
        return $this->belongsTo(Conference::class);
    }


    public function getAllConferenceDays($conferenceId)
    {
        return $this->where('conference_id', $conferenceId)->get();
    }


    public function scheduleTimeslots()
    {
        return $this->hasMany(ScheduleTimeslot::class);
    }


    public function meetingTimeslots()
    {
        return $this->hasMany(MeetingTimeslot::class);
    }

	public function PersonalAppointments()
	{
		return $this->hasMany(PersonalAppointment::class);
	}

}
