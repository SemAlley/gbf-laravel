<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockData extends Model
{

    protected $guarded = ['id'];

    protected $table = 'blocks_data';

}
