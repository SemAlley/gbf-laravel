<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnavailableTimestot extends Model
{
    protected $table = 'unavailable_timeslots';

    protected $fillable = ['timeslot_id', 'type'];
}
