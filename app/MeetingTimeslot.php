<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingTimeslot extends Model
{


    protected $fillable = ['day_id', 'title', 'time_from', 'time_to'];



    public function day ()
    {
        return $this->belongsTo(Day::class);
    }

	/**
	 * Get the meta for the delegate.
	 */
	public function meetingRequests()
	{
		return $this->hasMany(MeetingRequest::class, 'meeting_timeslot_id');
	}

	public function corporates()
    {
        return $this->belongsToMany(Corporate::class, 'meetingtimeslots_corporates');
    }

}
