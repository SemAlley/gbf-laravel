<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ADMIN

/**
 * Guest routes
 */
Route::group( [ 'prefix' => 'a', 'namespace' => 'Admin', 'middleware' => 'guest' ], function () {
	Route::get( 'login', 'Auth\LoginController@showLoginForm' )->name( 'login' );
	Route::post( 'login', 'Auth\LoginController@login' );
} );

/**
 * Routes for authorized users
 */
Route::group(
	[
		'prefix'     => 'a',
		'namespace'  => 'Admin',
		'middleware' => 'auth'
	],
	function () {
		Route::get( '/', 'AdminController@index' )->name( 'admin.dashboard' );

		Route::post( 'logout', 'Auth\LoginController@logout' )->name( 'logout' );

		Route::get( 'register', 'Auth\RegisterController@showRegistrationForm' )->name( 'register' );
		Route::post( 'register', 'Auth\RegisterController@register' );

		Route::get( 'password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm' )->name( 'password.request' );
		Route::post( 'password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail' )->name( 'password.email' );
		Route::get( 'password/reset/{token}', 'Auth\ResetPasswordController@showResetForm' )->name( 'password.reset' );
		Route::post( 'password/reset', 'Auth\ResetPasswordController@reset' );

		Route::get( 'switch/{id}', 'AdminController@switchConference' )->name( 'conference.switch' );

		Route::get( 'delegate-view', 'AdminController@delegateView' )->name( 'admin.delegate-view' );

		Route::group(
			[ 'prefix' => 'conference' ],
			function () {
				Route::get( '/', 'ConferenceController@index' );

				Route::get( '/get/{conference}', 'ConferenceController@get' );

				Route::post( '/create', 'ConferenceController@createPost' );
				Route::post( '/edit/{conference}', 'ConferenceController@edit' );
				Route::get( '/delete/{id}', 'ConferenceController@delete' );
			}
		);

        Route::group( [ 'prefix' => 'push' ], function () {
            Route::get('send', 'PushController@sendPush');
            Route::post('send', 'PushController@sendPushPost');
        });

		Route::group( [ 'prefix' => 'delegates' ], function () {
			Route::get( '/get-csv', 'DelegateController@getBlankCsv' )->name( 'admin.delegate.get-csv-template' );
			Route::get( '/upload-csv', 'DelegateController@uploadCsvPage' )->name( 'admin.delegate.upload-csv-page' );
			Route::post( '/upload', 'DelegateController@uploadCsv' )->name( 'admin.delegate.upload-csv' );
			Route::get( '/export', 'DelegateController@export' )->name( 'admin.delegate.export' );
			Route::get( '/export-media', 'DelegateController@exportMedia' )->name( 'admin.delegate.export-media' );

			Route::get( '/', 'DelegateController@getAll' )->name( 'admin.delegate.get' );
			Route::get( '/{id}', 'DelegateController@get' )->name( 'admin.delegate.get-one' )->where( [ 'id' => "[0-9]+" ] );
			Route::delete( '/{id}', 'DelegateController@delete' )->name( 'admin.delegate.delete-one' )->where( [ 'id' => "[0-9]+" ] );
			Route::put( '/', 'DelegateController@put' )->name( 'admin.delegate.put' );
			Route::patch( '/{id}', 'DelegateController@patch' )->name( 'admin.delegate.edit' )->where( [ 'id' => "[0-9]+" ] );
			Route::delete( '/', 'DelegateController@delete' )->name( 'admin.delegate.delete' );

			Route::post( '/notify/welcome', 'DelegateController@notifyWelcome' )->name( 'admin.delegate.notify-welcome' );
			Route::post( '/notify/eticket', 'DelegateController@notifyEticket' )->name( 'admin.delegate.notify-eticket' );
			Route::post( '/notify/blank', 'DelegateController@notifyBlank' )->name( 'admin.delegate.notify-blank' );

			Route::post( '/mass-appointment', 'DelegateController@massAppointment' )->name( 'admin.delegate.mass-appointment' );

			Route::delete( '/delete/{delegate}', 'DelegateController@remove' );
		} );

		Route::group(
			[ 'prefix' => 'speakers' ],
			function () {
				Route::get( '/', 'SpeakerController@index' )->name( 'admin.speakers' );

				Route::get( '/get/{speaker}', 'SpeakerController@get' );

				Route::get( '/company-list', 'SpeakerController@companyList' );

				Route::post( '/create', 'SpeakerController@createPost' );
				Route::post( '/edit/{speaker}', 'SpeakerController@edit' );
				Route::post( '/create', 'SpeakerController@createPost' );
				Route::post( '/edit', 'SpeakerController@edit' );
				Route::delete( '/delete/{speaker}', 'SpeakerController@remove' );
			}
		);

		Route::group(
			[ 'prefix' => 'content' ],
			function () {
				Route::get( 'global', 'ContentController@global' )->name( 'admin.content.global' );

                Route::post( 'set-favicon', 'ContentController@setFavicon' );

				Route::post( 'global', 'ContentController@globalPost' );

				Route::get( 'media-manager', 'ContentController@mediaManager' )->name( 'admin.media-manager' );
				Route::post( 'media-manager', 'ContentController@mediaManagerUpload' );
				Route::post( 'media-manager/save-alt', 'ContentController@mediaManagerSaveAlt' );
				Route::delete( 'media-manager/delete/{asset}', 'ContentController@mediaManagerRemoveAsset' );

				Route::group(
					[ 'prefix' => 'base' ],
					function () {
						Route::get( 'home', 'Content\BaseController@home' )->name( 'admin.content.base.home' );
						Route::post( 'home', 'Content\BaseController@homePost' );

						Route::get( 'speakers', 'Content\BaseController@speakers' )->name( 'admin.content.base.speakers' );
						Route::post( 'speakers', 'Content\BaseController@speakersPost' );

						Route::get( 'programme', 'Content\BaseController@programme' )->name( 'admin.content.base.programme' );
						Route::post( 'programme', 'Content\BaseController@programmePost' );

						Route::get( 'register', 'Content\BaseController@register' )->name( 'admin.content.base.register' );
						Route::post( 'register', 'Content\BaseController@registerPost' );

						Route::get( 'login', 'Content\BaseController@login' )->name( 'admin.content.base.login' );
						Route::post( 'login', 'Content\BaseController@loginPost' );

						Route::get( 'knowledge-hub', 'Content\BaseController@knowledgeHub' )->name( 'admin.content.base.knowledge-hub' );
						Route::post( 'knowledge-hub', 'Content\BaseController@knowledgeHubPost' );

						Route::get( 'contact', 'Content\BaseController@contact' )->name( 'admin.content.base.contact' );
						Route::post( 'contact', 'Content\BaseController@contactPost' );

						Route::get( 'media-registration', 'Content\BaseController@mediaRegistration' )->name( 'admin.content.base.media-registration' );
						Route::post( 'media-registration', 'Content\BaseController@mediaRegistrationPost' );

						Route::get( 'hotel-finder', 'HotelController@contentHotel' )->name( 'admin.content.base.hotel-finder' );
						Route::post( 'hotel-finder', 'HotelController@contentHotelSave' );

						Route::get( 'dashboard', 'Content\BaseController@dashboard' )->name( 'admin.content.base.dashboard' );
						Route::post( 'dashboard', 'Content\BaseController@dashboardPost' );

					}
				);

				Route::group(
					[ 'prefix' => 'galleries' ],
					function () {
						Route::get( '/', 'GalleriesController@index' )->name( 'galleries' );

						Route::get( '/{gallery}', 'GalleriesController@get' );

						Route::post( '/', 'GalleriesController@create' );
						Route::patch( '/{id}', 'GalleriesController@update' );
						Route::delete( '/{id}', 'GalleriesController@delete' );
					}
				);

			}
		);

        Route::group(
            ['prefix' => 'cms'],
            function(){
                Route::get('create', 'Cms\BaseController@createNewPage')->name('admin.cms.create');

                Route::get('list', 'Cms\BaseController@listPages')->name('admin.cms.list');
                Route::get('list-ajax', 'Cms\BaseController@listPagesAjax')->name('admin.cms.list-ajax');

                Route::get('get-assets/{type}', 'Cms\BaseController@getAssets')->name('admin.cms.getAssets');
                Route::post('create', 'Cms\BaseController@savePage')->name('admin.cms.postCreate');

                Route::get('update/{idOrSlug}', 'Cms\BaseController@updatePage')->name('admin.cms.update');
                Route::patch('update/{idOrSlug}', 'Cms\BaseController@patchPage')->name('admin.cms.patchUpdate');

                Route::post('delete/{idOrSlug}', 'Cms\BaseController@deletePage')->name('admin.cms.deletePage');

                Route::post('status-trigger/{idOrSlug}', 'Cms\BaseController@statusTrigger')->name('admin.cms.deleteStatusTrigger');
            }
        );

        Route::get('/contact', 'ContactController@index')->name('admin.contact');
        Route::get('/contact/export', 'ContactController@export')->name('admin.contact.export');

		Route::get( '/media', 'MediaController@index' )->name( 'admin.media' );
		Route::get( '/media/export', 'MediaController@export' )->name( 'admin.media.export' );


		Route::group(
			['prefix' => 'hotel'],
			function(){
				Route::get('/', 'HotelController@index')->name('admin.hotel.index');

				Route::post('create', 'HotelController@create')->name('admin.hotel.create');

//				Route::get('update/{hotel}', 'HotelController@update')->name('admin.hotel.update');
				Route::patch('update/{hotel}', 'HotelController@patch')->name('admin.hotel.patch');

				Route::delete('delete/{hotel}', 'HotelController@delete')->name('admin.hotel.delete');

                Route::get('all-hotels-ajax', 'HotelController@getAllHotelsAjax')->name('admin.hotel.all.ajax');
			}
		);

		Route::group(
			['prefix' => 'email'],
			function(){
				Route::get('/global', 'EmailController@globalEmail')->name('admin.email.global');
				Route::post('/global', 'EmailController@globalEmailSave')->name('admin.email.global.save');

				Route::get('/edit', 'EmailController@editPage')->name('admin.email.edit-page');
				Route::post('/edit', 'EmailController@edit')->name('admin.email.edit');

				Route::get('/send', 'EmailController@send')->name('admin.email.send');
			}
		);


        Route::group(
            ['prefix' => 'meeting-management'],
            function(){

                Route::group(['prefix' => 'delegates'],
                    function() {
                        Route::get('/', 'MeetingManagement\DelegateController@index')->name('admin.meeting-management.delegates.index');

                        Route::get('/get-requests-ajax', 'MeetingManagement\DelegateController@getRequestsAjax')->name('admin.meeting-management.delegates.getDelegatesAjax');

                        Route::get( '/download', 'MeetingManagement\DelegateController@export' )->name('admin.meeting-management.delegates.download');
                    }
                );

                Route::group(['prefix' => 'days'],
                    function() {
                        Route::get('/', 'MeetingManagement\DaysController@index')->name('admin.meeting-management.days.index');

                        Route::post('/create', 'MeetingManagement\DaysController@store')->name('admin.meeting-management.days.store');

                        Route::get('/get-days-ajax', 'MeetingManagement\DaysController@getDaysAjax')->name('admin.meeting-management.days.getDaysAjax');

                        Route::patch('/{day}', 'MeetingManagement\DaysController@update')->name('admin.meeting-management.days.update');

                        Route::delete('/{day}', 'MeetingManagement\DaysController@delete')->name('admin.meeting-management.days.delete');
                    }
                );

                Route::group(['prefix' => 'schedule-timeslots'],
                    function() {
                        Route::get('/', 'MeetingManagement\ScheduleTimeslotsController@index')->name('admin.meeting-management.schedule-timeslots.index');

                        Route::post('/create', 'MeetingManagement\ScheduleTimeslotsController@store')->name('admin.meeting-management.schedule-timeslots.store');

                        Route::patch('/{scheduleTimeslot}', 'MeetingManagement\ScheduleTimeslotsController@update')->name('admin.meeting-management.schedule-timeslots.update');

                        Route::delete('/{scheduleTimeslot}', 'MeetingManagement\ScheduleTimeslotsController@delete')->name('admin.meeting-management.schedule-timeslots.delete');

                        Route::get('/get-schedule-timeslots-ajax', 'MeetingManagement\ScheduleTimeslotsController@getScheduleTimeslotsAjax')->name('admin.meeting-management.schedule-timeslots.getScheduleTimeslotsAjax');
                    }
                );

                Route::group(['prefix' => 'meeting-timeslots'],
                    function() {

                        Route::get('/', 'MeetingManagement\MeetingTimeslotsController@index')->name('admin.meeting-management.meeting-timeslots.index');

                        Route::get('/get-meeting-timeslots-ajax', 'MeetingManagement\MeetingTimeslotsController@getMeetingTimeslotsAjax')->name('admin.meeting-management.meeting-timeslots.getMeetingTimeslotsAjax');

                        Route::post('/create', 'MeetingManagement\MeetingTimeslotsController@store')->name('admin.meeting-management.meeting-timeslots.store');

                        Route::patch('/{meetingTimeslot}', 'MeetingManagement\MeetingTimeslotsController@update')->name('admin.meeting-management.meeting-timeslots.update');

                        Route::delete('/{meetingTimeslot}', 'MeetingManagement\MeetingTimeslotsController@delete')->name('admin.meeting-management.meeting-timeslots.delete');

                    }
                );

                Route::group(['prefix' => 'meeting-corporates'],
                    function() {

                        Route::get('/', 'MeetingManagement\CorporatesController@index')->name('admin.meeting-management.corporates.index');

                        Route::post('/create', 'MeetingManagement\CorporatesController@save')->name('admin.meeting-management.corporates.save');

                        Route::get('/get-corporates-ajax', 'MeetingManagement\CorporatesController@getCorporatesAjax')->name('admin.meeting-management.corporates.getCorporatesAjax');

                        Route::patch('/{corporate}', 'MeetingManagement\CorporatesController@update')->name('admin.meeting-management.corporates.update');

                        Route::delete('/{corporate}', 'MeetingManagement\CorporatesController@delete')->name('admin.meeting-management.corporates.delete');

                    }
                );

                Route::group(['prefix' => 'meeting-matrix'],
                    function() {

                        Route::get('/', 'MeetingManagement\MatrixController@index')->name('admin.meeting-management.matrix.index');

                        Route::post('/create', 'MeetingManagement\MatrixController@save')->name('admin.meeting-management.matrix.save');

                        Route::post('/agenda', 'MeetingManagement\MatrixController@getAgenda')->name('admin.meeting-management.matrix.agenda');

                        Route::post('/shuffle', 'MeetingManagement\MatrixController@reshuffle')->name('admin.meeting-management.matrix.shuffle');

                        Route::post('/change-status', 'MeetingManagement\MatrixController@changeSetStatus')->name('admin.meeting-management.matrix.setStatus');

                        Route::get('/capacity', 'MeetingManagement\MatrixCapacityController@index')->name('admin.meeting-management.capacity.index');

                        Route::post('/capacity/update', 'MeetingManagement\MatrixCapacityController@update')->name('admin.meeting-management.capacity.update');
                    }
                );

            }
        );
		Route::group(['prefix' => 'csv-export'],
			function() {

				Route::get('/corporates', 'CSV\MatrixExportController@exportCorporates')->name('admin.csv-export.corporates');
				Route::get('/investors', 'CSV\MatrixExportController@exportInvestors')->name('admin.csv-export.investors');

			}
		);
	}
);

Route::get('/', function(){
    return redirect('/latin-america/');
});

//FRONTEND
Route::group(
	[ 'prefix' => '{conference}' ],
	function ( $conference ) {

		Route::get( '/', 'Frontend\ConferenceController@index' )->name( 'frontend.index' );

		Route::get( 'login', 'Frontend\Auth\LoginController@showLoginForm' )->name( 'frontend.login' );
		Route::post( 'login', 'Frontend\Auth\LoginController@login' )->name( 'frontend.login.post' );
		Route::any( 'logout', 'Frontend\Auth\LoginController@logout' )->name( 'frontend.logout' );

		Route::get( 'register', 'Frontend\Auth\RegisterController@showRegistrationForm' )->name( 'frontend.register' );
		Route::post( 'register', 'Frontend\Auth\RegisterController@register' )->name( 'frontend.register.post' );
		Route::post( 'uic-check', 'Frontend\Auth\RegisterController@checkUIC' )->name( 'frontend.register.uic-check' );

		Route::get( 'dashboard', 'Frontend\Auth\ProfileController@index' )->name( 'frontend.dashboard' );
		Route::post( 'profile', 'Frontend\Auth\ProfileController@update' )->name( 'frontend.profile.post' );
		Route::post( 'profile-photo', 'Frontend\Auth\ProfileController@updatePhoto' )->name( 'frontend.profile-photo.post' );

		Route::get( 'password/reset', 'Frontend\Auth\ForgotPasswordController@showLinkRequestForm' )->name( 'frontend.password.request' );
		Route::post( 'password/email', 'Frontend\Auth\ForgotPasswordController@sendResetLinkEmail' )->name( 'frontend.password.email' );
		Route::get( 'password/reset/{token}', 'Frontend\Auth\ResetPasswordController@showResetForm' )->name( 'frontend.password.reset' );
		Route::post( 'password/reset', 'Frontend\Auth\ResetPasswordController@reset' )->name( 'frontend.password.post-reset' );

		Route::get( '/speakers', 'Frontend\SpeakerController@index' )->name( 'frontend.speakers' );
		Route::get( '/speakers/{speaker}', 'Frontend\SpeakerController@profile' )->name( 'frontend.speakers.profile' );

		Route::get( '/contact', 'Frontend\ContactController@index' )->name( 'frontend.contact' );
		Route::post( '/contact', 'Frontend\ContactController@store' )->name( 'frontend.post.contact' );

		Route::get( '/media/register', 'Frontend\MediaController@register' )->name( 'frontend.media.register' );
		Route::post( '/media/register', 'Frontend\MediaController@store' )->name( 'frontend.media.post.register' );

		Route::get( '/knowledge-hub', 'Frontend\KnowledgeHubController@index' )->name( 'frontend.knowledge-hub' );

		Route::get( '/hotel-finder', 'Frontend\HotelController@index' )->name( 'frontend.hotel-finder' );

		Route::get( '/gallery/{gallery}', 'Frontend\GalleryController@single' )->name( 'frontend.gallery-single' );

        //Hack: need to match front end path depth for vue api calls
        Route::get('mobileplanner', 'Frontend\Auth\ProfileController@appMobilePlanner')->name( 'mobileplanner' );
        Route::get('mobileprofile', 'Frontend\Auth\ProfileController@appMobileProfile')->name( 'mobileprofile' );
        Route::get('mobiledelegates', 'Frontend\Auth\ProfileController@appMobileDelegates');
        Route::get('mobilereports', 'Frontend\MobileController@getKhReports');
        Route::get('mobilegallery', 'Frontend\MobileController@getKhGallery');
        Route::get('mobilepress', 'Frontend\MobileController@getKhPress');
        Route::get('mobilelogout', 'Frontend\MobileController@mobileLogout');
        Route::get('mobilelogoutdone', function(){
            return "";
        });

		Route::get('/{customPage}', 'Frontend\Cms\BaseController@single')->name('frontend.custom-page');

		Route::group(
			['prefix' => 'api'],
			function(){
				Route::get( '/get-meetings', 'Frontend\Auth\ProfileController@getMeetingTimeSlots' )->name( 'frontend.profile.get-meetings' );
				Route::get( '/get-industries', 'Frontend\Auth\ProfileController@getDelegateIndustries' )->name( 'frontend.profile.get-industries' );
				Route::post( '/get-delegates', 'Frontend\Auth\ProfileController@getFilteredDelegates' )->name( 'frontend.profile.get-delegates' );
				Route::get('/get-asset/{asset}', 'Frontend\Cms\BaseController@getAsset')->name('frontend.api.get-asset');

                Route::post('/login', 'Frontend\Auth\LoginController@mobileLogin');
                Route::get( '/profile', 'Frontend\Auth\ProfileController@mobileIndex' );

				Route::group(
					['prefix' => 'profile'],
					function(){
						Route::get( '/planner-pdf', 'Frontend\Auth\ProfileController@plannerPdf' )
						     ->name( 'frontend.profile.planner-pdf' );
						Route::get( '/planner-email', 'Frontend\Auth\ProfileController@plannerEmail' )
						     ->name( 'frontend.profile.planner-email' );
					}
				);

				Route::group(
					['prefix' => 'request'],
					function(){
						Route::get( '/all', 'Frontend\MeetingRequestController@getAll' )
						     ->name( 'frontend.request.get-all' );
						Route::post( '/create', 'Frontend\MeetingRequestController@create' )
						     ->name( 'frontend.request.create' );
						Route::delete( '/cancel/{meeting_request}', 'Frontend\MeetingRequestController@cancel' )
						     ->name( 'frontend.request.cancel' );
						Route::patch( '/accept/{meeting_request}', 'Frontend\MeetingRequestController@accept' )
						     ->name( 'frontend.request.cancel' );
					}
				);

				Route::group(
					['prefix' => 'timeslot'],
					function(){
						Route::get( '/get-unavailable', 'Frontend\TimeslotController@getUnavailable' )
						     ->name( 'frontend.timeslot.get-unavailable' );
						Route::patch( '/update-unavailable', 'Frontend\TimeslotController@updateUnavailable' )
						     ->name( 'frontend.timeslot.update-unavailable' );
					}
				);

				Route::group(
					['prefix' => 'appointment'],
					function(){
						Route::post( '/create', 'Frontend\AppointmentController@create' )
						     ->name( 'frontend.appointment.create' );
						Route::delete( '/remove/{personal_appointment}', 'Frontend\AppointmentController@remove' )
						     ->name( 'frontend.appointment.remove' );
					}
				);
			}
		);

		Route::group(
		    ['prefix' => 'mobile'],
            function(){
		        Route::get('app', 'Frontend\MobileController@getApp');
                Route::get('speakers', 'Frontend\MobileController@getSpeakers');
                Route::get('tab/{tab}/{index}', 'Frontend\MobileController@getTabById');
                Route::get('tabjson/{tab}/{index}', 'Frontend\MobileController@getTabJsonById');
                Route::get('programmetab/{tab}/{index}', 'Frontend\MobileController@getProgrammeTabById');
                Route::get('image/{id}', 'Frontend\MobileController@getImage');
                Route::get('sponsor/popup/{id}', 'Frontend\MobileController@getSponsorImage');
                Route::get('sponsor/popup/{id}/exists', 'Frontend\MobileController@getSponsorImageExists');
                Route::get('slido/url', 'Frontend\MobileController@getSlidoUrl');
                Route::get('programme/now', 'Frontend\MobileController@getCurrentSession');
                Route::get('push/register', 'Frontend\MobileController@putPushToken');
                Route::post('question', 'Frontend\MobileController@speakerQuestion');
                Route::post('feedback', 'Frontend\MobileController@eventFeedback');
        });

	}
);

Route::get('/barcode/get/{barcode}.jpg', function($barcode){
    $generator = new Picqer\Barcode\BarcodeGeneratorJPG();
    //echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($_GET['c'], $generator::TYPE_INTERLEAVED_2_5)) . '">';
    //echo base64_encode($generator->getBarcode($_GET['c'], $generator::TYPE_INTERLEAVED_2_5));

    // send the right headers
    header("Content-Type: image/png");
    //header("Content-Length: ".filesize($barcode));

    echo $generator->getBarcode($barcode, $generator::TYPE_CODE_128);
});
